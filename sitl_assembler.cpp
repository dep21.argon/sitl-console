#include "sitl_assembler.h"
#include "about_info.h"
#include <QStringList>
#include <QObject>

void SitlAssembler::addCommandAssembler(SitlCommandAssembler *commandAssembler)
{
    sitlCommands.insert(commandAssembler->getCommandId(), commandAssembler);
}

bool SitlAssembler::isSitlCommand(quint64 sitlCommandId)
{
    QMap <quint64, SitlCommandAssembler *>::const_iterator i = sitlCommands.begin();
    while(i != sitlCommands.end())
    {
        if(i.key() == sitlCommandId)
        {
            return true;
        }
        ++i;
    }
    return false;
}

bool SitlAssembler::isSitlCommand(QString sitlCommandStr)
{
    QMap <quint64, SitlCommandAssembler *>::const_iterator i = sitlCommands.begin();
    while(i != sitlCommands.end())
    {
        if(i.value()->getAliases().contains(sitlCommandStr.toUpper()))
        {
            return true;
        }
        ++i;
    }
    return false;
}

SitlCommandAssembler * SitlAssembler::getSitlCommandAssembler(QString sitlCommandStr)
{
    QMap <quint64, SitlCommandAssembler *>::const_iterator i = sitlCommands.begin();
    while(i != sitlCommands.end())
    {
        if(i.value()->getAliases().contains(sitlCommandStr.toUpper()))
        {
            return i.value();
        }
        ++i;
    }
    return NULL;
}

SitlCommandAssembler * SitlAssembler::getSitlCommandAssembler(quint64 sitlCommandId)
{
    QMap <quint64, SitlCommandAssembler *>::const_iterator i = sitlCommands.begin();
    while(i != sitlCommands.end())
    {
        if(i.key() == sitlCommandId)
        {
            return i.value();
        }
        ++i;
    }
    return NULL;
}

QList <SitlCommandFamilyParams> SitlAssembler::assemble(QStringList sitlCommandArguments)
{
    QList <SitlCommandFamilyParams> paramsList;
    SitlCommandAssembler * assembler = getSitlCommandAssembler(sitlCommandArguments.at(0));
    if(assembler == NULL)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    return assembler->assemble(sitlCommandArguments);
}

QString SitlAssembler::disassemble(SitlCommandFamilyParams sitlParams)
{
    SitlCommandAssembler * disassembler = getSitlCommandAssembler(sitlParams.commandId);
    if(disassembler == NULL)
    {
        return "WRONG COMMAND                                     ";
    }
    return disassembler->disassemble(sitlParams);
}

SitlCommandAssembler::SitlCommandAssembler(quint64 commandId, QString commandStr)
{
    this->commandId = commandId;
    this->commandStr = commandStr;
    addCommandAlias(commandStr);
}

QString SitlCommandAssembler::manual()
{
    return QObject::trUtf8("Нет справки для данной команды");
}

void SitlCommandAssembler::addCommandAlias(QString alias)
{
    aliases.append(alias);
}

QList <QString> SitlCommandAssembler::getAliases()
{
    return aliases;
}

quint64 SitlCommandAssembler::getCommandId()
{
    return commandId;
}

bool SitlCommandAssembler::isMyCommand(quint8 commandId)
{
    return (this->commandId == commandId);
}

bool SitlCommandAssembler::isMyCommand(QString sitlCommandStr)
{
    return aliases.contains(sitlCommandStr.toUpper());
}

QString SitlCommandAssembler::answerCodeToString(AnswerCode answerCode)
{
    QString answerCodeStr;
    switch(answerCode)
    {
    case AC_ICTRL:
        answerCodeStr = "ICTRL";
        break;
    case AC_DONE:
        answerCodeStr = "DONE";
        break;
    case AC_ERDON:
        answerCodeStr = "ERDON";
        break;
    case AC_ABORT:
        answerCodeStr = "ABORT";
        break;
    case AC_ERABR:
        answerCodeStr = "ERABR";
        break;
    case AC_RETRY:
        answerCodeStr = "RETRY";
        break;
    case AC_ERRET:
        answerCodeStr = "ERRET";
        break;
    case AC_SETUP:
        answerCodeStr = "SETUP";
        break;
    case AC_ACTIV:
        answerCodeStr = "ACTIV";
        break;
    default:
        answerCodeStr = "ERUNK";
    }
    return answerCodeStr;
}

WordSize SitlCommandAssembler::stringToWordSize(QString wordSizeStr, bool &successFlag)
{
    successFlag = true;
    if((wordSizeStr == "-D8")
            || wordSizeStr == "-D08")
    {
        return WS_8BIT;
    }
    else if(wordSizeStr == "-D16")
    {
        return WS_16BIT;
    }
    else if(wordSizeStr == "-D32")
    {
        return WS_32BIT;
    }
    else if(wordSizeStr =="-D64")
    {
       return WS_64BIT;
    }
    successFlag = false;
    return WS_8BIT;
}

MemoryAddressSize SitlCommandAssembler::memoryAddressSizeFromBits(int bits, bool &successFlag)
{
    MemoryAddressSize memoryAddressSize;
    switch(bits)
    {
        case 16:
            successFlag = true;
            memoryAddressSize = MAS_16BIT;
            break;
        case 24:
            successFlag = true;
            memoryAddressSize = MAS_24BIT;
            break;
        case 32:
            successFlag = true;
            memoryAddressSize = MAS_32BIT;
            break;
        case 64:
            successFlag = true;
            memoryAddressSize = MAS_64BIT;
            break;
        default:
            successFlag = false;
            memoryAddressSize = MAS_16BIT;
    }
    return memoryAddressSize;
}


WordSize SitlCommandAssembler::wordSizeFromBits(int bits, bool &successFlag)
{
    WordSize wordSize;
    switch(bits)
    {
    case 8:
        successFlag = true;
        wordSize = WS_8BIT;
        break;
    case 16:
        successFlag = true;
        wordSize = WS_16BIT;
        break;
    case 32:
        successFlag = true;
        wordSize = WS_32BIT;
        break;
    case 64:
        successFlag = true;
        wordSize = WS_64BIT;
        break;
    default:
        successFlag = false;
        wordSize = WS_8BIT;
    }
    return wordSize;
}

bool SitlCommandAssembler::stringToUseAutoincrementAddress(QString useAutoincrementAddressString, bool &successFlag)
{
    successFlag = true;
    if(useAutoincrementAddressString == "-S0")
    {
        return false;
    }
    else if(useAutoincrementAddressString == "-S1")
    {
        return true;
    }
    else
    {
        successFlag = false;
        return false;
    }
}

quint64 SitlCommandAssembler::stringToReadCount(QString readCountStr, bool &successFlag)
{
    successFlag = true;
    quint64 readCount = readCountStr.mid(2, -1).toLongLong(&successFlag);
    if((readCount < 1) || (readCount > 256))
    {
        successFlag = false;
    }
    return readCount;
}

SitlCommandAssemblerMWR::SitlCommandAssemblerMWR()
    :SitlCommandAssembler(COMMAND_ID_SITL_MWR, "MWR")
{
    addCommandAlias("WR");
    addCommandAlias("MEMWR");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMWR::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() != 3)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_MWR;
    params.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    params.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString dataWordString = sitlCommandStringList.at(2);
    params.wordSize = wordSizeFromBits(dataWordString.size() * 4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.dataWord = (quint64)dataWordString.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerMWR::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
    sitlAnswer.replace(6, addrStr.size(), addrStr);
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    sitlAnswer.replace(40, 4, "LAST");
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMWR::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MWR - запись одиночных слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MWR {АДРЕС} {ДАННЫЕ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MWR выполняет запись одиночного слова данных в адресном пространстве паняти.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит запись. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{ДАННЫЕ} - данные, которые необходимо записать. Данные указываются в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности данных:\n"));
    man.append(QObject::trUtf8("\t8 бит   - {ДАННЫЕ} = 00...FF\n"));
    man.append(QObject::trUtf8("\t16 бит  - {ДАННЫЕ} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {ДАННЫЕ} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {ДАННЫЕ} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MWR 20125600 F0\n"));
    man.append(QObject::trUtf8("\tЗапись 8-битного слова F0h по 32-битному адресу 20125600h\n"));
    man.append(QObject::trUtf8("MWR 2012560112000000 0123456789ABCDEF\n"));
    man.append(QObject::trUtf8("\tЗапись 64-битного слова 0123456789ABCDEFh по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerMRD::SitlCommandAssemblerMRD()
    :SitlCommandAssembler(COMMAND_ID_SITL_MRD, "MRD")
{
    addCommandAlias("RD");
    addCommandAlias("MEMRD");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMRD::assemble(QStringList wordList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(wordList.size() != 3)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_MRD;
    params.commandType = CT_REQUEST;
    QString addressWordSting = wordList.at(1);
    bool ok;
    params.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString dataWordSizeString = wordList.at(2);
    params.wordSize = stringToWordSize(dataWordSizeString, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerMRD::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
    sitlAnswer.replace(6, addrStr.size(), addrStr);
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    sitlAnswer.replace(40, 4, "LAST");
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMRD::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MRD - чтение одиночных слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MRD {АДРЕС} -D{РАЗРЯДНОСТЬ ДАННЫХ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MRD выполняет чтение одиночного слова данных в адресном пространстве паняти.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти с которого происходит чтение. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{РАЗРЯДНОСТЬ ДАННЫХ} - определяет разрядность считываемых данных\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности данных:\n"));
    man.append(QObject::trUtf8("\t8 бит   - {РАЗРЯДНОСТЬ ДАННЫХ} = 8|08\n"));
    man.append(QObject::trUtf8("\t16 бит  - {РАЗРЯДНОСТЬ ДАННЫХ} = 16\n"));
    man.append(QObject::trUtf8("\t32 бита - {РАЗРЯДНОСТЬ ДАННЫХ} = 32\n"));
    man.append(QObject::trUtf8("\t64 бита - {РАЗРЯДНОСТЬ ДАННЫХ} = 64\n\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MRD 20125600 -D64\n"));
    man.append(QObject::trUtf8("\tЧтение 64-битного слова по 32-битному адресу 20125600h\n"));
    man.append(QObject::trUtf8("MRD 2012560112000000 -D8\n"));
    man.append(QObject::trUtf8("\tЧтение 8-битного слова по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerIDEN::SitlCommandAssemblerIDEN()
    :SitlCommandAssembler(COMMAND_ID_SITL_IDEN, "IDEN")
{
    addCommandAlias("ID");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerIDEN::assemble(QStringList sitlCommandString)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandString.size() != 1)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(SitlCommandIdenParamsRequest());
    return paramsList;
}

QString SitlCommandAssemblerIDEN::disassemble(SitlCommandFamilyParams sitlParams)
{
    if(sitlParams.commandId != COMMAND_ID_SITL_IDEN)
    {
        return "Invalid command id";
    }
    QString versionString = QString("UDP IO Client SW v") + QString(BUILD_VERSION) + QString().fill(' ', 50);
    versionString.resize(50);
    QString idenString;
    idenString += "IDEN: SITL-v1.0                                   \n";
    idenString += "SITL-assembler:                                   \n";
    idenString += versionString + "\n"; ;
    idenString += "For communication with 1986BE1T MCU               \n";
    idenString += "Via Ethernet IP network connection                \n";
    idenString += "SITL-processor:                                   \n";
    idenString += versionString + "\n";
    idenString += "For communication with 1986BE1T MCU               \n";
    idenString += "Via Ethernet IP network connection                \n";
    idenString += "SITL-disassembler:                                \n";
    idenString += versionString + "\n";
    idenString += "For communication with 1986BE1T MCU               \n";
    idenString += "Via Ethernet IP network connection                \n";
    idenString += "(C) Institute Argon. 2015.                        \n";
    idenString += "For OS Microsoft Windows version XP-7             \n";
    idenString += "Writen by Dmitry Saraev                           ";
    return idenString;
}

QString SitlCommandAssemblerIDEN::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("IDEN - сведения о поддерживаемой версии языка SITL и конфигурации обородования\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("IDEN\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Функции Команды IDEN - вывести в поток результатов сведения о поддерживаемой версии языка SITL (версия спецификации SITL) и данные о текцщей конфигурации оборудования (SITL-ассемблере, SITL-процессоре, SITL-дизассемблере)."));
    return man;
}

SitlCommandAssemblerLIST::SitlCommandAssemblerLIST()
    :SitlCommandAssembler(COMMAND_ID_SITL_LIST, "LIST")
{

}

QList <SitlCommandFamilyParams> SitlCommandAssemblerLIST::assemble(QStringList sitlCommandString)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandString.size() != 1)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(SitlCommandListParamsRequest());
    return paramsList;
}

QString SitlCommandAssemblerLIST::disassemble(SitlCommandFamilyParams sitlParams)
{
    if(sitlParams.commandId != COMMAND_ID_SITL_LIST)
    {
        return "Invalid command id";
    }
    QString listString;
    listString += "LIST:                                             \n";
    listString += "LIST                                              \n";
    listString += "IDEN                                              \n";
    listString += "MWR   ADDRESS-32       DATA-WORD-08               \n";
    listString += "MWR   ADDRESS-32       DATA-WORD-16               \n";
    listString += "MWR   ADDRESS-32       DATA-WORD-32               \n";
    listString += "MWR   ADDRESS-32       DATA-WORD-64               \n";
    listString += "MRD   ADDRESS-32       -D08                       \n";
    listString += "MRD   ADDRESS-32       -D16                       \n";
    listString += "MRD   ADDRESS-32       -D32                       \n";
    listString += "MRD   ADDRESS-32       -D64                       \n";
    listString += "MWX   ADDRESS-32       DATA-WORD-32 DATA-WORD32 > \n";
    listString += "MRX   ADDRESS-32       -D32                       \n";
    listString += "MWI   ADDRESS-32       DATA-WORD-32 DATA-WORD32 > \n";
    listString += "MRL   ADDRESS-32       -D32                       \n";
    listString += "MRM   ADDRESS-32       -D32                       \n";
    listString += "CLSET                                             \n";
    listString += "CLINE                                             \n";
    listString += "INTRQ IRQ[255:0]                                  ";
    return listString;
}

QString SitlCommandAssemblerLIST::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("LIST - список поддерживаемых SITL команд\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("LIST\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Функции Команды LIST - вывести в поток результатов список команд, поддерживаемых текущей конфигурацией оборудования."));
    return man;
}

SitlCommandAssemblerINT::SitlCommandAssemblerINT()
    :SitlCommandAssembler(COMMAND_ID_SITL_INT, "INT")
{

}

QList <SitlCommandFamilyParams> SitlCommandAssemblerINT::assemble(QStringList sitlCommandString)
{
    Q_UNUSED(sitlCommandString);
    QList <SitlCommandFamilyParams> paramsList;
    paramsList.append(SitlCommandSynerParamsRequest());
    return paramsList;
}

QString SitlCommandAssemblerINT::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    QString irqNumStr = QString("%1").arg(params.irqNumber, 2, 16, QChar('0'));
    sitlAnswer.replace(6, irqNumStr.size(), irqNumStr);
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

SitlCommandAssemblerSYNER::SitlCommandAssemblerSYNER()
    :SitlCommandAssembler(COMMAND_ID_SITL_SYNER, "SYNER")
{

}

QList <SitlCommandFamilyParams> SitlCommandAssemblerSYNER::assemble(QStringList sitlCommandString)
{
    Q_UNUSED(sitlCommandString);
    QList <SitlCommandFamilyParams> paramsList;
    paramsList.append(SitlCommandSynerParamsRequest());
    return paramsList;
}

QString SitlCommandAssemblerSYNER::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

SitlCommandAssemblerMWX::SitlCommandAssemblerMWX()
    :SitlCommandAssembler(COMMAND_ID_SITL_MWX, "MWX")
{
    addCommandAlias("WRX");
    addCommandAlias("MEMWX");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMWX::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() < 4)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams commonParams;
    commonParams.commandId = COMMAND_ID_SITL_MWX;
    commonParams.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    commonParams.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    commonParams.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString useAutoincrementStr = sitlCommandStringList.at(sitlCommandStringList.size() - 1);
    commonParams.useAddressAutoincrement = stringToUseAutoincrementAddress(useAutoincrementStr, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    int paramCounter;
    for(paramCounter = 2; paramCounter < (sitlCommandStringList.size() - 1); ++paramCounter)
    {
        SitlCommandFamilyParams params;
        params.commandId = commonParams.commandId;
        params.commandType = commonParams.commandType;
        params.memoryAddressSize = commonParams.memoryAddressSize;
        params.addressWord = commonParams.addressWord;
        params.haveWordInMessage = true;
        params.haveAddressInMessage = false;
        params.useAddressAutoincrement = commonParams.useAddressAutoincrement;
        QString dataWordString = sitlCommandStringList.at(paramCounter);
        params.wordSize = wordSizeFromBits(dataWordString.size() * 4, ok);
        if(ok == false)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        params.dataWord = (quint64)dataWordString.toULongLong(&ok, 16);
        if(ok == false)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        params.isMultipleTransaction = true;
        if(paramCounter == 2)
        {
            params.placeInTransaction = PIT_FIRST;
            commonParams.wordSize = params.wordSize;
        }
        else if(paramCounter == (sitlCommandStringList.size() - 2))
        {
            params.commandId = COMMAND_ID_SITL_SEQ;
            params.placeInTransaction = PIT_LAST;
        }
        else
        {
            params.commandId = COMMAND_ID_SITL_SEQ;
            params.placeInTransaction = PIT_INTERMEDIATE;
        }
        if(commonParams.useAddressAutoincrement)
        {
            commonParams.addressWord += commonParams.GetBytesWordSize();
        }
        if(commonParams.wordSize != params.wordSize)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        paramsList.append(params);
    }
    //only one word
    if(paramCounter == 3)
    {
        SitlCommandFamilyParams params;
        params.commandId = COMMAND_ID_SITL_SEQ;
        //params.commandId = commonParams.commandId;
        params.commandType = commonParams.commandType;
        params.haveWordInMessage = false;
        params.haveAddressInMessage = false;
        params.isMultipleTransaction = true;
        params.placeInTransaction = PIT_LAST;
        paramsList.append(params);
    }
    return paramsList;
}

QString SitlCommandAssemblerMWX::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    if(params.haveAddressInMessage)
    {
        QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(6, addrStr.size(), addrStr);
    }
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    if(params.placeInTransaction == PIT_LAST)
    {
        sitlAnswer.replace(40, 4, "LAST");
    }
    else
    {
        if(params.useAddressAutoincrement)
        {
            sitlAnswer.replace(40, 4, "NEXT");
        }
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMWX::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MWX - запись массива слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MWX {АДРЕС} {МАССИВ ДАННЫХ} -S{ПОРЯДОК ВЫБОРКИ ИПИ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MWX выполняет запись массива слов данных в адресном пространстве памяти.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит запись. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{МАССИВ ДАННЫХ} - данные, которые необходимо записать. Данные указываются в шестнадцатиричном формате, через пробелы. Разрядность данных в массиве должна быть одинаковой.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности данных:\n"));
    man.append(QObject::trUtf8("\t8 бит   - {ДАННЫЕ} = 00...FF\n"));
    man.append(QObject::trUtf8("\t16 бит  - {ДАННЫЕ} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {ДАННЫЕ} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {ДАННЫЕ} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{ПОРЯДОК ВЫБОРКИ ИПИ} - определяет изменение адреса в объеме многофазовой транзакции.\n"));
    man.append(QObject::trUtf8("Доступны следующие варианты порядка выборки ИПИ:\n"));
    man.append(QObject::trUtf8("\tОбращение по одному и тому же адресу в каждой фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 0\n"));
    man.append(QObject::trUtf8("\tУвеличение адресу в каждой следующей фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 1\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MWX 20125600 00 11 22 33 44 55 66 77 -S1\n"));
    man.append(QObject::trUtf8("\tЗапись массива из 8-битных слов данных начиная с 32-битного адреса 20125600h\n"));
    man.append(QObject::trUtf8("MWX 2012560112000000 0123 4567 89AB CDEF -S0\n"));
    man.append(QObject::trUtf8("\tЗапись 16-битных слов по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerMRX::SitlCommandAssemblerMRX()
    :SitlCommandAssembler(COMMAND_ID_SITL_MRX, "MRX")
{
    addCommandAlias("RDX");
    addCommandAlias("MEMRX");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMRX::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() != 5)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_MRX;
    params.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    params.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString dataWordSizeString = sitlCommandStringList.at(2);
    params.wordSize = stringToWordSize(dataWordSizeString, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString readCountString = "-" + sitlCommandStringList.at(3);
    params.wordsCount = stringToReadCount(readCountString, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString useAutoincrementStr = "-" + sitlCommandStringList.at(4);
    params.useAddressAutoincrement = stringToUseAutoincrementAddress(useAutoincrementStr, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerMRX::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    if(params.haveAddressInMessage)
    {
        QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(6, addrStr.size(), addrStr);
    }
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    if(params.placeInTransaction == PIT_LAST)
    {
        sitlAnswer.replace(40, 4, "LAST");
    }
    else
    {
        if(params.useAddressAutoincrement)
        {
            sitlAnswer.replace(40, 4, "NEXT");
        }
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMRX::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MRX - чтение массива слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MRX {АДРЕС} -D{РАЗРЯДНОСТЬ ДАННЫХ} R{КОЛИЧЕСТВО СЛОВ} S{ПОРЯДОК ВЫБОРКИ ИПИ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MRX выполняет чтение массива слов данных в адресном пространстве памяти.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит чтение. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{РАЗРЯДНОСТЬ ДАННЫХ} - определяет разрядность считываемых данных\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности данных:\n"));
    man.append(QObject::trUtf8("\t8 бит   - {РАЗРЯДНОСТЬ ДАННЫХ} = 8|08\n"));
    man.append(QObject::trUtf8("\t16 бит  - {РАЗРЯДНОСТЬ ДАННЫХ} = 16\n"));
    man.append(QObject::trUtf8("\t32 бита - {РАЗРЯДНОСТЬ ДАННЫХ} = 32\n"));
    man.append(QObject::trUtf8("\t64 бита - {РАЗРЯДНОСТЬ ДАННЫХ} = 64\n\n"));
    man.append(QObject::trUtf8("{КОЛИЧЕСТВО СЛОВ} - определяет количество считываемых слов данных\n"));
    man.append(QObject::trUtf8("Количество слов ограничено общим объемом считываемых данных в 256 байт, таким обзазом:\n"));
    man.append(QObject::trUtf8("\tпри считывании 8 битых слов   - {КОЛИЧЕСТВО СЛОВ} = 1..256\n"));
    man.append(QObject::trUtf8("\tпри считывании 16 битых слов  - {КОЛИЧЕСТВО СЛОВ} = 1..128\n"));
    man.append(QObject::trUtf8("\tпри считывании 32 битых слов  - {КОЛИЧЕСТВО СЛОВ} = 1..64\n"));
    man.append(QObject::trUtf8("\tпри считывании 64 битых слов  - {КОЛИЧЕСТВО СЛОВ} = 1..32\n"));
    man.append(QObject::trUtf8("{ПОРЯДОК ВЫБОРКИ ИПИ} - определяет изменение адреса в объеме многофазовой транзакции.\n"));
    man.append(QObject::trUtf8("Доступны следующие варианты порядка выборки ИПИ:\n"));
    man.append(QObject::trUtf8("\tОбращение по одному и тому же адресу в каждой фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 0\n"));
    man.append(QObject::trUtf8("\tУвеличение адресу в каждой следующей фазе данных          - {ПОРЯДОК ВЫБОРКИ ИПИ} = 1\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MRX 20125600 -D8 R8 S1\n"));
    man.append(QObject::trUtf8("\tСчитывание массива из 8 8-битных слов данных начиная с 32-битного адреса 20125600h\n"));
    man.append(QObject::trUtf8("MRX 2012560112000000 -D16 R40 S0\n"));
    man.append(QObject::trUtf8("\tЧтение 40 16-битных слов по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerMWI::SitlCommandAssemblerMWI()
    :SitlCommandAssembler(COMMAND_ID_SITL_MWI, "MWI")
{
    addCommandAlias("MEMWI");
    addCommandAlias("POSTW");
    addCommandAlias("PMW");
    addCommandAlias("PMWR");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMWI::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() < 4)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams commonParams;
    commonParams.commandId = COMMAND_ID_SITL_MWI;
    commonParams.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    commonParams.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    commonParams.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    QString useAutoincrementStr = sitlCommandStringList.at(sitlCommandStringList.size() - 1);
    commonParams.useAddressAutoincrement = stringToUseAutoincrementAddress(useAutoincrementStr, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    int paramCounter;
    for(paramCounter = 2; paramCounter < (sitlCommandStringList.size() - 1); ++paramCounter)
    {
        SitlCommandFamilyParams params;
        params.commandId = commonParams.commandId;
        params.commandType = commonParams.commandType;
        params.memoryAddressSize = commonParams.memoryAddressSize;
        params.addressWord = commonParams.addressWord;
        params.haveWordInMessage = true;
        params.haveAddressInMessage = false;
        params.useAddressAutoincrement = commonParams.useAddressAutoincrement;
        QString dataWordString = sitlCommandStringList.at(paramCounter);
        params.wordSize = wordSizeFromBits(dataWordString.size() * 4, ok);
        if(ok == false)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        params.dataWord = (quint64)dataWordString.toULongLong(&ok, 16);
        if(ok == false)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        params.isMultipleTransaction = true;
        if(paramCounter == 2)
        {
            params.placeInTransaction = PIT_FIRST;
            commonParams.wordSize = params.wordSize;
        }
        else if(paramCounter == (sitlCommandStringList.size() - 2))
        {
            params.commandId = COMMAND_ID_SITL_SEQ;
            params.placeInTransaction = PIT_LAST;
        }
        else
        {
            params.commandId = COMMAND_ID_SITL_SEQ;
            params.placeInTransaction = PIT_INTERMEDIATE;
        }
        if(commonParams.useAddressAutoincrement)
        {
            commonParams.addressWord += commonParams.GetBytesWordSize();
        }
        if(commonParams.wordSize != params.wordSize)
        {
            paramsList.clear();
            paramsList.append(SitlCommandSynerParamsRequest());
            return paramsList;
        }
        paramsList.append(params);
    }
    //only one word
    if(paramCounter == 3)
    {
        SitlCommandFamilyParams params;
        params.commandId = COMMAND_ID_SITL_SEQ;
        //params.commandId = commonParams.commandId;
        params.commandType = commonParams.commandType;
        params.haveWordInMessage = false;
        params.haveAddressInMessage = false;
        params.isMultipleTransaction = true;
        params.placeInTransaction = PIT_LAST;
        paramsList.append(params);
    }
    return paramsList;
}

QString SitlCommandAssemblerMWI::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    if(params.haveAddressInMessage)
    {
        QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(6, addrStr.size(), addrStr);
    }
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    if(params.placeInTransaction == PIT_LAST)
    {
        sitlAnswer.replace(40, 4, "LAST");
    }
    else
    {
        if(params.useAddressAutoincrement)
        {
            sitlAnswer.replace(40, 4, "NEXT");
        }
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMWI::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MWI - отложенная запись массива слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MWI {АДРЕС} {МАССИВ ДАННЫХ} -S{ПОРЯДОК ВЫБОРКИ ИПИ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MWI выполняет отложенную запись массива слов данных в адресном пространстве памяти.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит запись. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{МАССИВ ДАННЫХ} - данные, которые необходимо записать. Данные указываются в шестнадцатиричном формате, через пробелы. Разрядность данных в массиве должна быть одинаковой.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности данных:\n"));
    man.append(QObject::trUtf8("\t8 бит   - {ДАННЫЕ} = 00...FF\n"));
    man.append(QObject::trUtf8("\t16 бит  - {ДАННЫЕ} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {ДАННЫЕ} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {ДАННЫЕ} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{ПОРЯДОК ВЫБОРКИ ИПИ} - определяет изменение адреса в объеме многофазовой транзакции.\n"));
    man.append(QObject::trUtf8("Доступны следующие варианты порядка выборки ИПИ:\n"));
    man.append(QObject::trUtf8("\tОбращение по одному и тому же адресу в каждой фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 0\n"));
    man.append(QObject::trUtf8("\tУвеличение адресу в каждой следующей фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 1\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MWI 20125600 00 11 22 33 44 55 66 77 -S1\n"));
    man.append(QObject::trUtf8("\tОтложенная запись массива из 8-битных слов данных начиная с 32-битного адреса 20125600h\n"));
    man.append(QObject::trUtf8("MWI 2012560112000000 0123 4567 89AB CDEF -S0\n"));
    man.append(QObject::trUtf8("\tОтложенная запись 16-битных слов по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerMRL::SitlCommandAssemblerMRL()
    :SitlCommandAssembler(COMMAND_ID_SITL_MRL, "MRL")
{
    addCommandAlias("PMRL");
    addCommandAlias("PREFR");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMRL::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() != 3)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_MRL;
    params.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    params.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.wordSize = WS_32BIT;
    QString useAutoincrementStr = sitlCommandStringList.at(2);
    params.useAddressAutoincrement = stringToUseAutoincrementAddress(useAutoincrementStr, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerMRL::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    if(params.haveAddressInMessage)
    {
        QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(6, addrStr.size(), addrStr);
    }
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    if(params.placeInTransaction == PIT_LAST)
    {
        sitlAnswer.replace(40, 4, "LAST");
    }
    else
    {
        if(params.useAddressAutoincrement)
        {
            sitlAnswer.replace(40, 4, "NEXT");
        }
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMRL::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MRL - чтение с предвыборкой массива слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MRL {АДРЕС} -S{ПОРЯДОК ВЫБОРКИ ИПИ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MRL выполняет чтение массива слов данных в адресном пространстве памяти до старшего адреса текущей строки КЭШа.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит чтение. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{ПОРЯДОК ВЫБОРКИ ИПИ} - определяет изменение адреса в объеме многофазовой транзакции.\n"));
    man.append(QObject::trUtf8("Доступны следующие варианты порядка выборки ИПИ:\n"));
    man.append(QObject::trUtf8("\tОбращение по одному и тому же адресу в каждой фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 0\n"));
    man.append(QObject::trUtf8("\tУвеличение адресу в каждой следующей фазе данных          - {ПОРЯДОК ВЫБОРКИ ИПИ} = 1\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MRL 20125600 -S1\n"));
    man.append(QObject::trUtf8("\tСчитывание массива из 8 8-битных слов данных начиная с 32-битного адреса 20125600h\n"));
    man.append(QObject::trUtf8("MRL 2012560112000000 -D16 R40 S0\n"));
    man.append(QObject::trUtf8("\tЧтение 40 16-битных слов по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerMRM::SitlCommandAssemblerMRM()
    :SitlCommandAssembler(COMMAND_ID_SITL_MRM, "MRM")
{
    addCommandAlias("PMRM");
    addCommandAlias("PRMUL");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerMRM::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() != 3)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_MRM;
    params.commandType = CT_REQUEST;
    QString addressWordSting = sitlCommandStringList.at(1);
    bool ok;
    params.memoryAddressSize = memoryAddressSizeFromBits(addressWordSting.size()*4, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.addressWord = (quint64)addressWordSting.toULongLong(&ok, 16);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    params.wordSize = WS_32BIT;
    QString useAutoincrementStr = sitlCommandStringList.at(2);
    params.useAddressAutoincrement = stringToUseAutoincrementAddress(useAutoincrementStr, ok);
    if(ok == false)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerMRM::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    if(params.haveAddressInMessage)
    {
        QString addrStr = QString("%1").arg(params.addressWord, params.GetBytesMemoryAddressSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(6, addrStr.size(), addrStr);
    }
    if(params.haveWordInMessage)
    {
        QString dataStr = QString("%1").arg(params.dataWord, params.GetBytesWordSize() * 2, 16, QChar('0'));
        sitlAnswer.replace(23, dataStr.size(), dataStr);
    }
    if(params.placeInTransaction == PIT_LAST)
    {
        sitlAnswer.replace(40, 4, "LAST");
    }
    else
    {
        if(params.useAddressAutoincrement)
        {
            sitlAnswer.replace(40, 4, "NEXT");
        }
    }
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerMRM::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("MRM - чтение с множественной предвыборкой массива слов данных\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("MRM {АДРЕС} -S{ПОРЯДОК ВЫБОРКИ ИПИ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда MRM выполняет чтение массива слов данных в адресном пространстве памяти до старшего адреса строки КЭШа, следующей за строкой к которой относится стартовый адрес.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{АДРЕС} - адрес в памяти по которому происходит чтение. Адрес указывается в шестнадцатиричном формате.\n"));
    man.append(QObject::trUtf8("Доступны следующие разрядности адреса:\n"));
    man.append(QObject::trUtf8("\t16 бит  - {АДРЕС} = 0000...FFFF\n"));
    man.append(QObject::trUtf8("\t24 бита - {АДРЕС} = 000000...FFFFFF\n"));
    man.append(QObject::trUtf8("\t32 бита - {АДРЕС} = 00000000...FFFFFFFF\n"));
    man.append(QObject::trUtf8("\t64 бита - {АДРЕС} = 0000000000000000...FFFFFFFFFFFFFFFF\n\n"));
    man.append(QObject::trUtf8("{ПОРЯДОК ВЫБОРКИ ИПИ} - определяет изменение адреса в объеме многофазовой транзакции.\n"));
    man.append(QObject::trUtf8("Доступны следующие варианты порядка выборки ИПИ:\n"));
    man.append(QObject::trUtf8("\tОбращение по одному и тому же адресу в каждой фазе данных - {ПОРЯДОК ВЫБОРКИ ИПИ} = 0\n"));
    man.append(QObject::trUtf8("\tУвеличение адресу в каждой следующей фазе данных          - {ПОРЯДОК ВЫБОРКИ ИПИ} = 1\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("MRM 20125600 -S1\n"));
    man.append(QObject::trUtf8("\tСчитывание массива из 8 8-битных слов данных начиная с 32-битного адреса 20125600h\n"));
    man.append(QObject::trUtf8("MRM 2012560112000000 -D16 R40 S0\n"));
    man.append(QObject::trUtf8("\tЧтение 40 16-битных слов по 64-битному адресу 2012560112000000h"));
    return man;
}

SitlCommandAssemblerCLINE::SitlCommandAssemblerCLINE()
    :SitlCommandAssembler(COMMAND_ID_SITL_CLINE, "CLINE")
{
    addCommandAlias("LSIZE");
    addCommandAlias("CHLNS");
}

QList <SitlCommandFamilyParams> SitlCommandAssemblerCLINE::assemble(QStringList sitlCommandStringList)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandStringList.size() != 1)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_CLINE;
    params.commandType = CT_REQUEST;
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerCLINE::disassemble(SitlCommandFamilyParams params)
{
    QString sitlAnswer;
    sitlAnswer.fill(' ', 50);
    QString sitlCommand = commandStr;
    sitlAnswer.replace(0, sitlCommand.size(), sitlCommand);
    QString cacheSizeStr = QString::number(params.cacheSize, 16);
    if(params.cacheSize == 0x100)
    {
        cacheSizeStr = "0" + cacheSizeStr;
    }
    sitlAnswer.replace(6, cacheSizeStr.size(), cacheSizeStr);
    QString answerCodeStr = answerCodeToString(params.answerCode);
    sitlAnswer.replace(45, answerCodeStr.size(), answerCodeStr);
    sitlAnswer.resize(50);
    return sitlAnswer.toUpper();
}

QString SitlCommandAssemblerCLINE::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("CLINE - вывод текущего объема строки КЭШа SITL-процессора.\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("CLINE\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда CLINE предназначена для вывода текущего объема строки КЭШа SITL-процессора.\n\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("CLINE\n"));
    man.append(QObject::trUtf8("\tСчитывание текущего объема строки КЭШа SITL-процессора\n"));
    return man;
}

SitlCommandAssemblerCLSET::SitlCommandAssemblerCLSET()
    :SitlCommandAssembler(COMMAND_ID_SITL_CLSET, "CLSET")
{

}

QList <SitlCommandFamilyParams> SitlCommandAssemblerCLSET::assemble(QStringList sitlCommandString)
{
    QList <SitlCommandFamilyParams> paramsList;
    if(sitlCommandString.size() != 2)
    {
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    bool ok;
    quint16 cacheSize = sitlCommandString.at(1).toUInt(&ok);
    if(ok == false)
    {
        paramsList.clear();
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    if((cacheSize < 4) || (cacheSize > 8))
    {
        paramsList.clear();
        paramsList.append(SitlCommandSynerParamsRequest());
        return paramsList;
    }
    SitlCommandFamilyParams params;
    params.commandId = COMMAND_ID_SITL_CLSET;
    params.commandType = CT_REQUEST;
    params.cacheSize = cacheSize;
    paramsList.append(params);
    return paramsList;
}

QString SitlCommandAssemblerCLSET::disassemble(SitlCommandFamilyParams params)
{
    Q_UNUSED(params);
    return "CLSET WRONG COMMAND                               ";
}

QString SitlCommandAssemblerCLSET::manual()
{
    QString man;
    man.append(QObject::trUtf8("Название:\n"));
    man.append(QObject::trUtf8("CLSET - установка объема одной строки КЭШа SITL-процессора.\n\n"));
    man.append(QObject::trUtf8("Синтаксис:\n"));
    man.append(QObject::trUtf8("CLSET {ОБЪЕМ КЭШ}\n\n"));
    man.append(QObject::trUtf8("Описание:\n"));
    man.append(QObject::trUtf8("Команда CLSET выполняет установку объема одной строки КЭШа SITL-процессора.\n\n"));
    man.append(QObject::trUtf8("Параметры:\n"));
    man.append(QObject::trUtf8("{ОБЪЕМ КЭШ} - Новое значение объема строки КЭШа, степень числа два.\n"));
    man.append(QObject::trUtf8("Доступны следующие объемы КЭШа:\n"));
    man.append(QObject::trUtf8("\t16 байт  - {ОБЪЕМ КЭШ} = 4\n"));
    man.append(QObject::trUtf8("\t32 байта - {ОБЪЕМ КЭШ} = 5\n"));
    man.append(QObject::trUtf8("\t64 байта - {ОБЪЕМ КЭШ} = 6\n"));
    man.append(QObject::trUtf8("\t128 байт - {ОБЪЕМ КЭШ} = 7\n"));
    man.append(QObject::trUtf8("\t256 байт - {ОБЪЕМ КЭШ} = 8\n\n"));
    man.append(QObject::trUtf8("Примеры:\n"));
    man.append(QObject::trUtf8("CLSET 5\n"));
    man.append(QObject::trUtf8("\tУстановка значения объема КЭШа в 32 байта\n"));
    man.append(QObject::trUtf8("CLSET 8\n"));
    man.append(QObject::trUtf8("\tУстановка значения объема КЭШа в 256 байт"));
    return man;
}
