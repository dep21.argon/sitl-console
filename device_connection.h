#ifndef DEVICE_CONNECTION_H
#define DEVICE_CONNECTION_H

#include <QtCore>
#include <QByteArray>

enum ConnectionState{CS_CONNECTED, CS_DISCONNECTED};

class DeviceConnection : public QObject
{
Q_OBJECT
public:
    //explicit DeviceConnection();
    virtual void connectDevice() = 0;
    virtual void disconnectDevice() = 0;
    virtual int sendData(QByteArray rawData) = 0;
    virtual ConnectionState getConnectionState() = 0;

signals:
    void connectNotify();
    void disconnectNotify();
    void dataSended(QByteArray rawData);
    void dataReceived(QByteArray rawData);
};

#endif // DEVICE_CONNECTION_H

