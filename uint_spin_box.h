#ifndef UINT_SPIN_BOX_H
#define UINT_SPIN_BOX_H

#include <QObject>
#include <QAbstractSpinBox>
#include <limits>

class UIntSpinBox : public QAbstractSpinBox
{
    Q_OBJECT
public:
    explicit UIntSpinBox( QWidget* parent = 0, int base = 10 );

    virtual ~UIntSpinBox();

  public:
    quint64 value() const;

    quint64 maximum() const;

  public:
    void setValue( quint64 value );

    void setMaximum( quint64 max );
    void setBase( int base );

  protected: // QAbstractSpinBox API
    virtual QValidator::State validate( QString& input, int& pos ) const;
    virtual void stepBy( int steps );
    virtual void fixup( QString& input ) const;
    virtual StepEnabled stepEnabled () const;

  protected:
    void updateEditLine() const;

  protected:
    mutable quint64 mValue;

    quint64 mMaximum;
    int mBase;

    QString mPrefix;
};

inline UIntSpinBox::UIntSpinBox( QWidget *parent, int base )
  : QAbstractSpinBox( parent ),
    mValue( 0 ),
    mMaximum( std::numeric_limits<quint64>::max() ),
    mBase( 0 )
{
    setBase( base );
}

inline quint64 UIntSpinBox::value()   const { return mValue; }
inline quint64 UIntSpinBox::maximum() const { return mMaximum; }

inline void UIntSpinBox::setMaximum( quint64 maximum )
{
    if( mMaximum == maximum )
        return;

    mMaximum = maximum;

    if( mValue > mMaximum )
    {
        mValue = mMaximum;

        updateEditLine();
    }
}

inline void UIntSpinBox::setValue( quint64 value )
{
    if( value > mMaximum )
        value = mMaximum;

    if( mValue == value )
        return;

    mValue = value;

    updateEditLine();
}

inline void UIntSpinBox::setBase( int base )
{
    base = qBound( 2, base, 36 );

    if( mBase == base )
        return;

    mBase = base;
    mPrefix = QString::fromLatin1(
        (base == 16) ? "0x" :
        (base ==  8) ? "0o" :
        (base ==  2) ? "0b" :
        /* else */     0 );
}

inline UIntSpinBox::~UIntSpinBox() {}

#endif // UINT_SPIN_BOX_H
