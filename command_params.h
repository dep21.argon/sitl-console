#ifndef COMMAND_PARAMS_H
#define COMMAND_PARAMS_H

#include <QMetaType>
#include <QSharedPointer>

enum CommandType{CT_REQUEST, CT_ANSWER};
enum CommandStatus{STATUS_SUCCESS, STATUS_ERROR};

#define EC_DEVICE_ERROR 0x01
#define EC_UNKNOWN_ERROR 0xFF

class CommandParams
{
public:
    virtual ~CommandParams();
    quint64 commandId;
    CommandType commandType;
    CommandStatus commandStatus;
    quint64 errorCode;
    bool operator==(const CommandParams & params) const;
};

Q_DECLARE_METATYPE(CommandParams)
Q_DECLARE_METATYPE(CommandParams *)

#endif // COMMAND_PARAMS_H

