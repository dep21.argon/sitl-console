#ifndef CONSOLE_H
#define CONSOLE_H

#include <QtGui>

class Console : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit Console(QWidget *parent = 0, QString prompt = "# ");
    void output(QString);
    void unblock();
    void scrollDown();
    QStringList getCommands();
    QStringList getResults();
protected:
    void keyPressEvent(QKeyEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseDoubleClickEvent(QMouseEvent *);
    void contextMenuEvent(QContextMenuEvent *);
private:
    QString prompt;
    bool isLocked;
    QStringList *history;
    int historyPos;
    QStringList commands;
    QStringList results;

    void onEnter();
    void insertPrompt(bool insertNewBlock = true);
    void commandAdd(QString);
    void resultAdd(QString);
    void historyAdd(QString);
    void historyBack();
    void historyForward();
public slots:
    void addExternalCommand(QString command);
    void addExternalCommandAndNotify(QString command);
    void addExternalResult(QString result);
    void addExternalResultAndNotify(QString result);
    void clear();
signals:
    void onCommand(QString);
    void onResult(QString);
    void onChange(QString);
};

#endif // CONSOLE_H
