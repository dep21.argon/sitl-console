#-------------------------------------------------
#
# Project created by QtCreator 2014-12-09T10:41:59
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sitl_udp_client
TEMPLATE = app
LIBS += -lIphlpapi -lWs2_32

version = 0.1

ABOUT_HEADER = $$_PRO_FILE_PWD_/about_info.h
about_info.target = about_info
about_info.commands += cd $$_PRO_FILE_PWD_ &
about_info.commands += $$_PRO_FILE_PWD_/set_version_info.bat
about_info.depends = FORCE

#system($$_PRO_FILE_PWD_/set_version_info.bat)

PRE_TARGETDEPS += about_info
QMAKE_EXTRA_TARGETS += about_info

after_comp.target = after_comp
after_comp.commands = echo hello world
#del $$_PRO_FILE_PWD_/about_info.h
after_comp.depends = FORCE

win32:RC_FILE = $$_PRO_FILE_PWD_/res/sitl_udp_client.rc

POST_TARGETDEPS += after_comp
QMAKE_EXTRA_TARGETS += after_comp

SOURCES += main.cpp\
        mainwindow.cpp \
    console.cpp \
    command_builder.cpp \
    command_codec.cpp \
    command_params.cpp \
    udp_device_connection.cpp \
    sitl_command_family_params.cpp \
    sitl_assembler.cpp \
    sitl_command_family_codec.cpp \
    sitoip_command_family_params.cpp \
    sitoip_command_family_codec.cpp \
    settings_command_family_params.cpp \
    settings_command_family_codec.cpp \
    arp_funcs.cpp \
    about.cpp \
    icmp_funcs.cpp \
    mac_address.cpp \
    check_sitoip_connection.cpp \
    codec_data_param_filter.cpp \
    codec_data_param_timeout_filter.cpp \
    settings_form.cpp \
    sitl_client_settings_class.cpp \
    console_engine.cpp \
    sitoip_console_proc.cpp \
    help_console_proc.cpp \
    console_proc.cpp \
    sitl_command_global_codec.cpp \
    sitl_ram_accessor.cpp \
    arp_record.cpp \
    uint_spin_box.cpp

HEADERS  += mainwindow.h \
    command_builder.h \
    console.h \
    command_codec.h \
    command_params.h \
    device_connection.h \
    udp_device_connection.h \
    sitl_command_family_params.h \
    sitl_command_family_codec.h \
    sitl_assembler.h \
    sitoip_command_family_codec.h \
    sitoip_command_family_params.h \
    settings_command_family_params.h \
    settings_command_family_codec.h \
    arp_funcs.h \
    about.h \
    icmp_funcs.h \
    check_sitoip_connection.h \
    mac_address.h \
    codec_data_param_filter.h \
    codec_data_param_timeout_filter.h \
    settings_form.h \
    sitl_client_settings_class.h \
    console_engine.h \
    console_proc.h \
    sitoip_console_proc.h \
    help_console_proc.h \
    sitl_command_global_codec.h \
    sitl_ram_accessor.h \
    arp_record.h \
    uint_spin_box.h

FORMS    += mainwindow.ui \
    about.ui \
    settings_form.ui

RESOURCES += \
    resources.qrc
