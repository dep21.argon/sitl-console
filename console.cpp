#include "console.h"

Console::Console(QWidget *parent, QString prompt) :
    QPlainTextEdit(parent)
{
    this->prompt = prompt;
    setUndoRedoEnabled(false);
    history = new QStringList;
    historyPos = 0;
    insertPrompt(false);
    isLocked = false;
}

void Console::keyPressEvent(QKeyEvent *event)
{
    if(isReadOnly())
    {
        return;
    }
    if(isLocked)
	return;
    if(event->key() == Qt::Key_Backspace
       && event->modifiers() == Qt::NoModifier
       && textCursor().positionInBlock() > prompt.length())
    {
        QPlainTextEdit::keyPressEvent(event);
    }
    if(event->key() == Qt::Key_Delete
       && event->modifiers() == Qt::NoModifier)
    {
        QPlainTextEdit::keyPressEvent(event);
    }
    else if((event->key() == Qt::Key_Right
        && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::KeypadModifier))
            || ((event->key() == Qt::Key_Left)
                && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::KeypadModifier)
                && (textCursor().positionInBlock() > prompt.length())))
    {
        QPlainTextEdit::keyPressEvent(event);
    }
    else if(
       event->key() >= 0x20 && event->key() <= 0x7e
       //event->text() > 0
       && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::ShiftModifier || event->modifiers() == Qt::KeypadModifier))
    {
            QPlainTextEdit::keyPressEvent(event);
    }
//    QString keyText = event->text();
//    if(keyText != "")
//    {
//        QPlainTextEdit::keyPressEvent(event);
//    }
    if((event->key() == Qt::Key_Return && event->modifiers() == Qt::NoModifier)
            || (event->key() == Qt::Key_Enter))
	onEnter();
    if(event->key() == Qt::Key_Up && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::KeypadModifier))
	historyBack();
    if(event->key() == Qt::Key_Down && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::KeypadModifier))
	historyForward();
    QString cmd = textCursor().block().text().mid(prompt.length());
    emit onChange(cmd);
}

void Console::mousePressEvent(QMouseEvent *)
{
    setFocus();
}

void Console::mouseDoubleClickEvent(QMouseEvent *){}

void Console::contextMenuEvent(QContextMenuEvent *){}

void Console::onEnter()
{
    if(textCursor().positionInBlock() == prompt.length())
    {
	insertPrompt();
	return;
    }
    QString cmd = textCursor().block().text().mid(prompt.length());
    commandAdd(cmd);
    historyAdd(cmd);
    insertPrompt();
    emit onCommand(cmd);
}

void Console::output(QString s)
{
    QTextCursor cursor = textCursor();
    moveCursor(QTextCursor::StartOfBlock);
    textCursor().insertBlock();
    moveCursor(QTextCursor::PreviousBlock);
    textCursor().insertText(s);
    setTextCursor(cursor);
    resultAdd(s);
    emit onResult(s);
}

void Console::addExternalResult(QString result)
{
    QTextCursor cursor = textCursor();
    moveCursor(QTextCursor::StartOfBlock);
    textCursor().insertBlock();
    moveCursor(QTextCursor::PreviousBlock);
    textCursor().insertText(result);
    setTextCursor(cursor);
    resultAdd(result);
}

void Console::addExternalResultAndNotify(QString result)
{
    addExternalResult(result);
    emit onResult(result);
}

void Console::addExternalCommand(QString command)
{
    QTextCursor cursor = textCursor();
    moveCursor(QTextCursor::StartOfBlock);
    textCursor().insertBlock();
    moveCursor(QTextCursor::PreviousBlock);
    textCursor().insertText(prompt);
    textCursor().insertText(command);
    commandAdd(command);
    historyAdd(command);
    setTextCursor(cursor);
}

void Console::addExternalCommandAndNotify(QString command)
{
    addExternalCommand(command);
    emit onCommand(command);
}

void Console::unblock()
{
    insertPrompt();
    isLocked = false;
}

void Console::insertPrompt(bool insertNewBlock)
{
    if(insertNewBlock)
    {
        moveCursor(QTextCursor::End);
        textCursor().insertBlock();
    }
    textCursor().insertText(prompt);
    scrollDown();
}

void Console::scrollDown()
{
    QScrollBar *vbar = verticalScrollBar();
    vbar->setValue(vbar->maximum());
}

void Console::historyAdd(QString cmd)
{
    history->append(cmd);
    historyPos = history->length();
}

void Console::historyBack()
{
    if(!historyPos)
	return;
    QTextCursor cursor = textCursor();
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    cursor.removeSelectedText();
    cursor.insertText(prompt + history->at(historyPos-1));
    setTextCursor(cursor);
    historyPos--;
}

void Console::historyForward()
{
    if(historyPos == history->length())
	return;
    QTextCursor cursor = textCursor();
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    cursor.removeSelectedText();
    if(historyPos == history->length() - 1)
	cursor.insertText(prompt);
    else
	cursor.insertText(prompt + history->at(historyPos + 1));
    setTextCursor(cursor);
    historyPos++;
}

void Console::commandAdd(QString cmd)
{
    commands.append(cmd);
}

void Console::resultAdd(QString res)
{
    results.append(res);
}

QStringList Console::getCommands()
{
    return commands;
}

QStringList Console::getResults()
{
    return results;
}

void Console::clear()
{
    commands.clear();
    results.clear();
    QPlainTextEdit::clear();
    insertPrompt(false);
}
