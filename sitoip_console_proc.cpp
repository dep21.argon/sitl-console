#include "sitoip_console_proc.h"

SitoipConsoleProc::SitoipConsoleProc()
{
    setProcessMode(PM_IMMEDIATE_PROCESSING);
    sitoipFilter.setNeedDecode();
    sitoipFilter.setNeedEncode();
    setMaxSitlCommandsInSitoipPacket(1);
    QObject::connect(&sitoipFilter, SIGNAL(encodedData(QByteArray)), SIGNAL(sitoipRawCommandOutput(QByteArray)));
    QObject::connect(&sitoipFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(showSitoipPacket(QSharedPointer <CommandParams>)));
    QObject::connect(&sitoipFilter, SIGNAL(decodedParams(QSharedPointer<CommandParams>)), SLOT(handleInputPacket(QSharedPointer<CommandParams>)));
}

void SitoipConsoleProc::startProc(QStringList commandArguments)
{
    if(commandArguments.at(0).toUpper() == "SITOIP")
    {
        if(commandArguments.size() < 2)
        {
            emit returnControl();
            return;
        }
        if(commandArguments.at(1).toUpper() == "REPEAT-ANSWER")
        {
            SitoipCommandFamilyParams * sitoipRepeatRequestParams = new SitoipCommandFamilyParams(COMMAND_ID_SITOIP_REPEAT_REQUEST, CT_REQUEST);
            QList <QSharedPointer <CommandParams> > sitoipParams;
            sitoipParams.append(QSharedPointer <CommandParams>(sitoipRepeatRequestParams));
            sendSitoipPackets(sitoipParams);
            emit returnControl();
            return;
        }
    }
    QList <SitlCommandFamilyParams> sitlCommandValueList = assembler->assemble(commandArguments);
    QList <QSharedPointer <CommandParams> > sitlCommandPtrList;
    for(int i = 0; i < sitlCommandValueList.size(); ++i)
    {
        SitlCommandFamilyParams * sitlCommand = new SitlCommandFamilyParams;
        *sitlCommand = sitlCommandValueList.at(i);
        sitlCommandPtrList.append(QSharedPointer <CommandParams>(sitlCommand));
    }
    if(getProcessMode() == PM_IMMEDIATE_PROCESSING)
    {
        QList <QSharedPointer <CommandParams> > sitoipParams = createSitoipPackets(sitlCommandPtrList);
        sendSitoipPackets(sitoipParams);
        sitoipParams.clear();
        sitlCommandPtrList.clear();
    }
    else if(getProcessMode() == PM_POST_PROCESSING)
    {
        sitlCommands.append(sitlCommandPtrList);
    }
    emit returnControl();
}

void SitoipConsoleProc::process()
{
    QList <QSharedPointer <CommandParams> > sitoipParams = createSitoipPackets(sitlCommands);
    sendSitoipPackets(sitoipParams);
    sitoipParams.clear();
    sitlCommands.clear();
//    while(!sitlCommands.isEmpty())
//    {
//        delete sitlCommands.takeFirst();
//    }
}

QList <QSharedPointer <CommandParams> > SitoipConsoleProc::createSitoipPackets(QList<QSharedPointer <CommandParams> > sitlCommandList)
{
    QList <QSharedPointer <CommandParams> > sitoipList;
    uint sitlCommandCounter = 0;
    while(sitlCommandCounter < sitlCommandList.size())
    {
        SitoipCommandFamilyParams * sitoipParams = new SitoipCommandFamilyParams;
        sitoipParams->commandType = CT_REQUEST;
        sitoipParams->commandId = COMMAND_ID_SITOIP_REQUEST;
        sitoipParams->sitlCommandsList.clear();
        int sitlCommandInCurrentSitoip = qMin((uint)sitlCommandList.size(), getMaxSitlCommandsInSitoipPacket());
        QList <QSharedPointer <CommandParams> > currentSitlCommands = sitlCommandList.mid(sitlCommandCounter, sitlCommandInCurrentSitoip);
        sitlCommandCounter += sitlCommandInCurrentSitoip;
        sitoipParams->sitlCommandsList.append(currentSitlCommands);
        sitoipList.append(QSharedPointer <CommandParams>(sitoipParams));
    }
    return sitoipList;
}

void SitoipConsoleProc::sendSitlRequestList(QList<QSharedPointer<CommandParams> > sitlRequestList)
{
    QList <QSharedPointer <CommandParams> > sitoipList = createSitoipPackets(sitlRequestList);
    sendSitoipPackets(sitoipList);
}

void SitoipConsoleProc::sendSitoipPackets(QList<QSharedPointer <CommandParams> > sitoipParams)
{
    for(int i = 0; i < sitoipParams.size(); ++i)
    {
        sitoipFilter.filterParams(sitoipParams.at(i));
    }
}

void SitoipConsoleProc::setProcessMode(ProcessMode mode)
{
    processMode = mode;
}

ProcessMode SitoipConsoleProc::getProcessMode()
{
    return processMode;
}

void SitoipConsoleProc::setEndian(QDataStream::ByteOrder endian)
{
    this->endian = endian;
    sitoipFilter.setEndian(endian);
}

void SitoipConsoleProc::setMaxSitlCommandsInSitoipPacket(uint sitlCommands)
{
    maxSitlCommandsInSitoipPacket = sitlCommands;
}

uint SitoipConsoleProc::getMaxSitlCommandsInSitoipPacket()
{
    return maxSitlCommandsInSitoipPacket;
}

void SitoipConsoleProc::addAssembler(SitlAssembler *assembler)
{
    this->assembler = assembler;
}

void SitoipConsoleProc::addCodec(SitoipCommandFamilyCodec *sitoipCodec)
{
    sitoipFilter.addCodec(sitoipCodec);
}

void SitoipConsoleProc::sitoipRawCommandInpup(QByteArray sitoipRawCommand)
{
    sitoipFilter.filterData(sitoipRawCommand);
}

void SitoipConsoleProc::showSitoipPacket(QSharedPointer <CommandParams> params)
{
    SitoipCommandFamilyParams * sitoipParams = (SitoipCommandFamilyParams*) params.data();
    for(int i = 0; i < sitoipParams->sitlCommandsList.size(); ++i)
    {
        SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) sitoipParams->sitlCommandsList.at(i).data();
        QString disasembledSitlString = assembler->disassemble(*sitlParams);
        emit output(disasembledSitlString);
    }
}

void SitoipConsoleProc::handleInputPacket(QSharedPointer<CommandParams> params)
{
    SitoipCommandFamilyParams * sitoipParams = (SitoipCommandFamilyParams*) params.data();
    emit receivedSitlAnswerList(sitoipParams->sitlCommandsList);
}
