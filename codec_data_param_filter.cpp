#include "codec_data_param_filter.h"

CodecDataParamFilter::CodecDataParamFilter(QObject *parent)
    :QObject(parent)
{
    endian = QDataStream::BigEndian;
    encode = false;
    decode = false;
    commandBuilder.setEndian(endian);
}

void CodecDataParamFilter::addCodec(CommandCodec *codec)
{
    commandBuilder.registerCodec(codec);
}

void CodecDataParamFilter::setEndian(QDataStream::ByteOrder endian)
{
    this->endian = endian;
    commandBuilder.setEndian(endian);
}

void CodecDataParamFilter::setNeedEncode(bool encode)
{
    this->encode = encode;
}

bool CodecDataParamFilter::getNeedEncode()
{
    return encode;
}

void CodecDataParamFilter::setNeedDecode(bool decode)
{
    this->decode = decode;
}

bool CodecDataParamFilter::getNeedDecode()
{
    return decode;
}

void CodecDataParamFilter::filterData(QByteArray rawData)
{
    if(commandBuilder.getCodecFromData(rawData) != NULL)
    {
        emit validData(rawData);
        if(decode)
        {
            CodecError codecError;
            QSharedPointer <CommandParams> params = commandBuilder.decode(rawData, &codecError);
            if(codecError.errType == CE_NO_ERROR)
            {
                emit decodedParams(params);
            }
        }
    }
}

void CodecDataParamFilter::filterParams(QSharedPointer <CommandParams> params)
{
    if(commandBuilder.getCodecFromParams(params) != NULL)
    {
        emit validParams(params);
        if(encode)
        {
            CodecError codecError;
            QByteArray rawData = commandBuilder.encode(params, &codecError);
            if(codecError.errType == CE_NO_ERROR)
            {
                emit encodedData(rawData);
            }
        }
    }
}
