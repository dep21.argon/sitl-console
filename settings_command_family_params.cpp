#include "settings_command_family_params.h"
#include <cstring>

SettingsCommandFamilyParams::SettingsCommandFamilyParams()
{

}

SettingsCommandFamilyParams::SettingsCommandFamilyParams(quint64 commandId, CommandType commandType)
{
    this->commandId = commandId;
    this->commandType = commandType;
}

SettingsCommandFamilyParams::~SettingsCommandFamilyParams()
{

}

bool SettingsCommandFamilyParams::operator ==(const SettingsCommandFamilyParams &params) const
{
    if((this->commandId != params.commandId)
            || this->commandType != params.commandType)
    {
        return false;
    }
    if(this->commandType == CT_REQUEST)
    {
        switch(this->commandId)
        {
            case COMMAND_ID_SETTINGS_SET_SOCKET:
                return ((this->deviceIpAddress == params.deviceIpAddress) &&
                        (this->deviceIpPort == params.deviceIpPort));
                break;
            case COMMAND_ID_SETTINGS_SET_MAC:
                return (memcmp(this->deviceMacAddress, params.deviceMacAddress, 6) == 0);
                break;
            case COMMAND_ID_SETTINGS_SET_GATEWAY_IP:
                return (this->gatewayIpAddress == params.gatewayIpAddress);
                break;
            case COMMAND_ID_SETTINGS_SET_NETMASK:
                return (this->netmask == params.netmask);
                break;
            case COMMAND_ID_SETTINGS_GET_ALL:
                return true;
                break;
        }
    }
    else if(this->commandType == CT_ANSWER)
    {
        switch(this->commandId)
        {
            case COMMAND_ID_SETTINGS_SET_SOCKET:
                return (this->commandStatus == params.commandStatus);
                break;
            case COMMAND_ID_SETTINGS_SET_MAC:
                return (this->commandStatus == params.commandStatus);
                break;
            case COMMAND_ID_SETTINGS_SET_GATEWAY_IP:
                return (this->commandStatus == params.commandStatus);
                break;
            case COMMAND_ID_SETTINGS_SET_NETMASK:
                return (this->commandStatus == params.commandStatus);
                break;
            case COMMAND_ID_SETTINGS_GET_ALL:
                return ((this->deviceIpAddress == params.deviceIpAddress)
                        && (this->deviceIpPort == params.deviceIpPort)
                        && (memcmp(this->deviceMacAddress, params.deviceMacAddress, 6) == 0)
                        && (this->gatewayIpAddress == params.gatewayIpAddress)
                        && (this->netmask == params.netmask)
                        && (memcmp(this->gatewayMacAddress, params.gatewayMacAddress, 6) == 0));
                break;
        }
    }
    return false;
}
