#include "check_sitoip_connection.h"

SitoipEmptyRequest::SitoipEmptyRequest()
{
    sitoipEmptyAnswerFilter.addCodec(&sitoipEmptyCodec);
    sitoipEmptyAnswerFilter.setEndian(QDataStream::LittleEndian);
    sitoipEmptyAnswerFilter.setTimeout(2000);
    QObject::connect(this, SIGNAL(checkData(QByteArray)), &sitoipEmptyAnswerFilter, SLOT(filterData(QByteArray)));
    QObject::connect(&sitoipEmptyAnswerFilter, SIGNAL(validData(QByteArray)), SLOT(slotSitoipAnswerReady(QByteArray)));
    QObject::connect(&sitoipEmptyAnswerFilter, SIGNAL(timeout()), SLOT(slotSitoipAnswerTimeout()));
}

void SitoipEmptyRequest::stop()
{
    sitoipEmptyAnswerFilter.stopWait();
}

void SitoipEmptyRequest::sendRequest(uint timeoutMsec, QDataStream::ByteOrder endian)
{
    sitoipEmptyAnswerFilter.setTimeout(timeoutMsec);
    sitoipEmptyAnswerFilter.setEndian(endian);
    SitoipCommandFamilyParams * emptyParams = new SitoipCommandFamilyParams(COMMAND_ID_SITOIP_EMPTY, CT_REQUEST);
    CodecError codecError;
    QByteArray rawEmptyRequest = sitoipEmptyCodec.encode(QSharedPointer <CommandParams>(emptyParams), endian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        return;
    }
    sitoipEmptyAnswerFilter.startWait();
    sendRequestTime = QTime::currentTime();
    emit sendData(rawEmptyRequest);
    //deviceConnection->sendData(rawEmptyRequest);
}

void SitoipEmptyRequest::slotSitoipAnswerReady(QByteArray rawData)
{
    Q_UNUSED(rawData);
    receiveAnswerTime = QTime::currentTime();
    emit signalAnswer(SITOIP_SUCCESS, (uint)sendRequestTime.msecsTo(receiveAnswerTime));
}

void SitoipEmptyRequest::slotSitoipAnswerTimeout()
{
    emit signalAnswer(SITOIP_TIMEOUT, (uint)0);
}

SitoipCheckConnection::SitoipCheckConnection()
{
    sitoipEmptyRequest = new SitoipEmptyRequest();
    QObject::connect(sitoipEmptyRequest, SIGNAL(sendData(QByteArray)), this, SIGNAL(sendData(QByteArray)));
    QObject::connect(this, SIGNAL(checkData(QByteArray)), sitoipEmptyRequest, SIGNAL(checkData(QByteArray)));
    QObject::connect(sitoipEmptyRequest, SIGNAL(signalAnswer(SitoipAnswer,uint)), SLOT(slotSitoipAnswer(SitoipAnswer,uint)));
    QObject::connect(&waitBetwenSendRequests, SIGNAL(timeout()), this, SLOT(slotSendSitoipEmpytRequest()));
}

void SitoipCheckConnection::setCheckConnectionParams(uint timeoutMsec, uint timeBetweenRequestsMsec, uint requestsForStatistics)
{
    this->timeoutMsec = timeoutMsec;
    this->timeBetweenRequestsMsec = timeBetweenRequestsMsec;
    this->requestsForStatistics = requestsForStatistics;
}

void SitoipCheckConnection::sendSitoipEmptyRequestSequence()
{
    stopRequestNow = false;
    sendTotal = 0;
    answersInfo.clear();
    slotSendSitoipEmpytRequest();
}

void SitoipCheckConnection::stopSitoipEmptyRequestSequence()
{
    sitoipEmptyRequest->stop();
    stopRequestNow = true;
    waitBetwenSendRequests.stop();
}

void SitoipCheckConnection::slotSitoipAnswer(SitoipAnswer answer, uint timeToAnswerMsec)
{
    if(answersInfo.size() >= requestsForStatistics)
    {
        answersInfo.dequeue();
    }
    SitoipEmptyAnswerInfo answerInfo;
    answerInfo.answer = answer;
    answerInfo.timeToAnswer = timeToAnswerMsec;
    answersInfo.enqueue(answerInfo);
    emit signalSitoipAnswer(answer, timeToAnswerMsec, sendTotal);
    emit signalSitoipConnectionStatistics(answersInfo.size(), successSend(), errorSend(), minSendTime(), maxSendTime(), avgSendTime());
    if(stopRequestNow)
    {
    }
    else
    {
        waitBetwenSendRequests.setSingleShot(true);
        waitBetwenSendRequests.start(timeBetweenRequestsMsec);
    }
}

void SitoipCheckConnection::slotSendSitoipEmpytRequest()
{
    ++sendTotal;
    sitoipEmptyRequest->sendRequest(timeoutMsec, QDataStream::LittleEndian);
}

uint SitoipCheckConnection::successSend()
{
    uint successSend = 0;
    for(int i = 0; i < answersInfo.size(); ++i)
    {
        if(answersInfo.at(i).answer == SITOIP_SUCCESS)
        {
            ++successSend;
        }
    }
    return successSend;
}

uint SitoipCheckConnection::errorSend()
{
    uint errorSend = 0;
    for(int i = 0; i < answersInfo.size(); ++i)
    {
        if(answersInfo.at(i).answer != SITOIP_SUCCESS)
        {
            ++errorSend;
        }
    }
    return errorSend;
}

uint SitoipCheckConnection::minSendTime()
{
    int minSendTime = -1;
    for(int i = 0; i < answersInfo.size(); ++i)
    {
        if(answersInfo.at(i).answer == SITOIP_SUCCESS)
        {
            if(minSendTime == -1)
            {
                minSendTime = answersInfo.at(i).timeToAnswer;
            }
            else if(answersInfo.at(i).timeToAnswer < minSendTime)
            {
                minSendTime = answersInfo.at(i).timeToAnswer;
            }
        }
    }
    return (uint)minSendTime;
}

uint SitoipCheckConnection::maxSendTime()
{
    int maxSendTime = -1;
    for(int i = 1; i < answersInfo.size(); ++i)
    {
        if(answersInfo.at(i).answer == SITOIP_SUCCESS)
        {
            if(maxSendTime == -1)
            {
                maxSendTime = answersInfo.at(i).timeToAnswer;
            }
            else if(answersInfo.at(i).timeToAnswer > maxSendTime)
            {
                maxSendTime = answersInfo.at(i).timeToAnswer;
            }
        }
    }
    return (uint)maxSendTime;
}

uint SitoipCheckConnection::avgSendTime()
{
    uint totalSendTime = 0;
    uint totalSends = 0;
    for(int i = 0; i < answersInfo.size(); ++i)
    {
        if(answersInfo.at(i).answer == SITOIP_SUCCESS)
        {
            totalSendTime += answersInfo.at(i).timeToAnswer;
            ++totalSends;
        }
    }
    if(totalSends == 0)
    {
        return (uint)-1;
    }
    return totalSendTime / totalSends;
}
