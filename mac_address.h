#ifndef MAC_ADDRESS_H
#define MAC_ADDRESS_H
#include <QtCore>

class MacAddress
{
public:
    MacAddress();
    MacAddress(QString macString);
    MacAddress(QByteArray rawMac);
    void setMacFromString(QString macString);
    void setMacFromByteArray(QByteArray rawMac);
    QString toString();
    QByteArray getMacByteArray();
    static const uint MAC_SIZE = 6;
private:
    QByteArray macAddress;
};

#endif // MAC_ADDRESS_H

