#include "sitl_command_family_codec.h"
#include <QDebug>

SitlCommandFamilyCodec::SitlCommandFamilyCodec(quint64 codecId, quint64 commandId, quint8 commandMask, CommandType commandType)
    :CommandCodec(codecId)
{
    m_commandId = commandId;
    m_commandMask = commandMask;
    m_commandType = commandType;
}

bool SitlCommandFamilyCodec::isParamsMyCommand(QSharedPointer <CommandParams> params)
{
    return ((params->commandId == m_commandId)
            && (params->commandType == m_commandType));
}

bool SitlCommandFamilyCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    /*
    quint8 header;
    dataStream >> header;
    if(m_commandType == CT_REQUEST)
    {
        if(header != COMMAND_ID_SITL_REQUEST)
        {
            return false;
        }
    }
    else if (m_commandType == CT_ANSWER)
    {
        if(header != COMMAND_ID_SITL_ANSWER)
        {
            return false;
        }
    }
    quint8 sitlWordsCount;
    dataStream >> sitlWordsCount;
    */
    if(m_commandType != CT_ANSWER)
    {
        return false;
    }
    quint8 sitlHeader;
    dataStream >> sitlHeader;
    if((quint8)(sitlHeader & m_commandMask) == (quint8)m_commandId)
    {
        return true;
    }
    return false;
}

QSharedPointer <CommandParams> SitlCommandFamilyCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    Q_UNUSED(data);
    Q_UNUSED(endian);
    codecError->errType = CE_CANT_DECODE;
    return QSharedPointer <CommandParams>(NULL);
}

QByteArray SitlCommandFamilyCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    Q_UNUSED(param);
    Q_UNUSED(endian);
    QByteArray nullArray;
    codecError->errType = CE_CANT_ENCODE;
    return nullArray;
}

AnswerCode SitlCommandFamilyCodec::answerCodeFromSitlAnswerCode(quint8 sitlAnswerCode, bool &success)
{
    success = true;
    AnswerCode answerCode = AC_DONE;
    switch(sitlAnswerCode)
    {
    case SITL_ANSWER_CODE_ICTRL:
        answerCode = AC_ICTRL;
        break;
    case SITL_ANSWER_CODE_DONE_WITH_DATA:
        answerCode = AC_DONE;
        break;
    case SITL_ANSWER_CODE_ERDONE_WITH_DATA:
        answerCode = AC_ERDON;
        break;
    case SITL_ANSWER_CODE_ABORT_NO_DATA:
        answerCode = AC_ABORT;
        break;
    case SITL_ANSWER_CODE_ABORT_WITH_DATA:
        answerCode = AC_ABORT;
        break;
    case SITL_ANSWER_CODE_ERABR_WITH_DATA:
        answerCode = AC_ERABR;
        break;
    case SITL_ANSWER_CODE_RETRY_NO_DATA:
        answerCode = AC_RETRY;
        break;
    case SITL_ANSWER_CODE_RETRY_WITH_DATA:
        answerCode = AC_RETRY;
        break;
    case SITL_ANSWER_CODE_ERRET_WITH_DATA:
        answerCode = AC_ERRET;
        break;
    case SITL_ANSWER_CODE_SETUP_NO_STATUS:
        answerCode = AC_SETUP;
        break;
    case SITL_ANSWER_CODE_SETUP_WITH_STATUS:
        answerCode = AC_SETUP;
        break;
    case SITL_ANSWER_CODE_ACTIV:
        answerCode = AC_ACTIV;
        break;
    default:
        success = false;
    }
    return answerCode;
}

quint8 SitlCommandFamilyCodec::sitlAnswerCodeFromAnswerCodeAndData(AnswerCode answerCode, bool dataInMessage, bool &success)
{
    success = true;
    quint8 sitlAnswerCode = SITL_ANSWER_CODE_DONE_WITH_DATA;
    if((answerCode == AC_DONE) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_DONE_WITH_DATA;
    }
    else if((answerCode == AC_ERDON) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_ERDONE_WITH_DATA;
    }
    else if((answerCode == AC_ABORT) && (dataInMessage == false))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_ABORT_NO_DATA;
    }
    else if((answerCode == AC_ABORT) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_ABORT_WITH_DATA;
    }
    else if((answerCode == AC_ERABR) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_ERABR_WITH_DATA;
    }
    else if((answerCode == AC_RETRY) && (dataInMessage == false))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_RETRY_NO_DATA;
    }
    else if((answerCode == AC_RETRY) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_RETRY_WITH_DATA;
    }
    else if((answerCode == AC_ERRET) && (dataInMessage == true))
    {
        sitlAnswerCode = SITL_ANSWER_CODE_ERRET_WITH_DATA;
    }
    else
    {
        success = false;
    }
    return sitlAnswerCode;
}

bool SitlCommandFamilyCodec::haveWordInMessageFromSitlAnswerCode(quint8 sitlAnswerCode, bool &success)
{
    success = true;
    bool haveWordInMessage = false;
    switch(sitlAnswerCode)
    {
    case SITL_ANSWER_CODE_DONE_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_ERDONE_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_ABORT_NO_DATA:
        haveWordInMessage = false;
        break;
    case SITL_ANSWER_CODE_ABORT_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_ERABR_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_RETRY_NO_DATA:
        haveWordInMessage = false;
        break;
    case SITL_ANSWER_CODE_RETRY_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_ERRET_WITH_DATA:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_SETUP_NO_STATUS:
        haveWordInMessage = false;
        break;
    case SITL_ANSWER_CODE_SETUP_WITH_STATUS:
        haveWordInMessage = true;
        break;
    case SITL_ANSWER_CODE_ACTIV:
        haveWordInMessage = false;
        break;
    default:
        success = false;
    }
    return haveWordInMessage;
}

bool SitlCommandFamilyCodec::haveWordInMessageFromSitlHeader(quint8 header, bool &success)
{
    success = true;
    switch(header & SITL_SEQ_HAVE_DATA_MASK)
    {
    case SITL_SEQ_HAVE_DATA_TRUE:
        return true;
        break;
    case SITL_SEQ_HAVE_DATA_FALSE:
        return false;
        break;
    }
    success = false;
    return true;
}

PlaceInTransaction SitlCommandFamilyCodec::getHeaderPIT(quint8 header)
{
    switch(header & SITL_SEQ_PIT_MASK)
    {
    case SITL_SEQ_PIT_INTERMEDIATE:
        return PIT_INTERMEDIATE;
        break;
    case SITL_SEQ_PIT_LAST:
        return PIT_LAST;
    }
    return  PIT_LAST;
}

void SitlCommandFamilyCodec::setHeaderHaveWord(quint8 &header, bool haveWord)
{
    switch(haveWord)
    {
    case true:
        header |= SITL_SEQ_HAVE_DATA_TRUE;
        break;
    case false:
        header |= SITL_SEQ_HAVE_DATA_FALSE;
        break;
    }
}

void SitlCommandFamilyCodec::setHeaderPIT(quint8 &header, PlaceInTransaction pit)
{
    switch(pit)
    {
    case PIT_INTERMEDIATE:
        header |= SITL_SEQ_PIT_INTERMEDIATE;
        break;
    case PIT_LAST:
        header |= SITL_SEQ_PIT_LAST;
        break;
    }
}

void SitlCommandFamilyCodec::setHeaderWordSize(quint8 &header, WordSize wordSize)
{
    switch(wordSize)
    {
        case WS_8BIT:
            header |= SITL_WORD_SIZE_8BIT;
            break;
        case WS_16BIT:
            header |= SITL_WORD_SIZE_16BIT;
            break;
        case WS_32BIT:
            header |= SITL_WORD_SIZE_32BIT;
            break;
        case WS_64BIT:
            header |= SITL_WORD_SIZE_64BIT;
            break;
    }
}

void SitlCommandFamilyCodec::setHeaderAddressSize(quint8 &header, MemoryAddressSize memoryAddressSize)
{
    switch(memoryAddressSize)
    {
        case MAS_16BIT:
            header |= SITL_ADDRESS_SIZE_16BIT;
            break;
        case MAS_24BIT:
            header |= SITL_ADDRESS_SIZE_24BIT;
            break;
        case MAS_32BIT:
            header |= SITL_ADDRESS_SIZE_32BIT;
            break;
        case MAS_64BIT:
            header |= SITL_ADDRESS_SIZE_64BIT;
            break;
    }
}

WordSize SitlCommandFamilyCodec::getFromHeaderWordSize(quint8 header)
{
    WordSize wordSize;
    quint8 sitlWordSize = header & SITL_WORD_SIZE_MASK;
    switch(sitlWordSize)
    {
        case SITL_WORD_SIZE_8BIT:
            wordSize = WS_8BIT;
            break;
        case SITL_WORD_SIZE_16BIT:
            wordSize = WS_16BIT;
            break;
        case SITL_WORD_SIZE_32BIT:
            wordSize = WS_32BIT;
            break;
        case SITL_WORD_SIZE_64BIT:
            wordSize = WS_64BIT;
            break;
        default:
            wordSize = WS_8BIT;
            qDebug() << "Invalid word size";
            break;
    }
    return wordSize;
}

MemoryAddressSize SitlCommandFamilyCodec::getFromHeaderAddressSize(quint8 header)
{
    MemoryAddressSize memoryAddressSize;
    quint8 addressSize = header & SITL_ADDRESS_SIZE_MASK;
    switch(addressSize)
    {
        case SITL_ADDRESS_SIZE_16BIT:
            memoryAddressSize = MAS_16BIT;
            break;
        case SITL_ADDRESS_SIZE_24BIT:
            memoryAddressSize = MAS_24BIT;
            break;
        case SITL_ADDRESS_SIZE_32BIT:
            memoryAddressSize = MAS_32BIT;
            break;
        case SITL_ADDRESS_SIZE_64BIT:
            memoryAddressSize = MAS_64BIT;
            break;
    }
    return memoryAddressSize;
}

bool SitlCommandFamilyCodec::getFromHeaderUseAutoincrementAddress(quint8 header)
{
    if((header & 0x20) == 0x20)
    {
        return true;
    }
    return false;
}

void SitlCommandFamilyCodec::setHeaderUseAutoincrementAddress(quint8 &header, bool useAutoincrementAddress)
{
    if(useAutoincrementAddress)
    {
        header |= 0x20;
    }
    else
    {
        header &= 0xDF;
    }
}

quint8 SitlCommandFamilyCodec::calculateOffset(quint8 maxWordSizeBits, quint8 wordSizeBits, quint64 address)
{
    quint64 significBits;// = LOG2(wordSizeBits/8) = address >> ((wordSizeBits / 8) - 1);
    switch(wordSizeBits)
    {
        case 8:
            significBits = address >> 0;
            break;
        case 16:
            significBits = address >> 1;
            break;
        case 32:
            significBits = address >> 2;
            break;
        case 64:
            significBits = address >> 3;
            break;
        default:
            significBits = address >> 0;
    }
    int module = (maxWordSizeBits / wordSizeBits);
    int offset = (significBits % module);
    return wordSizeBits * offset;
}

SitlCommandMwrRequestCodec::SitlCommandMwrRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWR_REQUEST, COMMAND_ID_SITL_MWR, SITL_COMMAND_MASK_MWR, CT_REQUEST)
{

}

QByteArray SitlCommandMwrRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    //dataStream << (quint8)COMMAND_ID_SITL_REQUEST;
    //dataStream << (quint8)0x01;
    quint8 header = (quint8)m_commandId;
    setHeaderAddressSize(header, sitlParams->memoryAddressSize);
    setHeaderWordSize(header, sitlParams->wordSize);
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8) 0x00;
    quint64 mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    mcwd = sitlParams->dataWord << offset;
    dataStream << mcwd;
    return data;
}

SitlCommandMrdRequestCodec::SitlCommandMrdRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRD_REQUEST, COMMAND_ID_SITL_MRD, SITL_COMMAND_MASK_MRD, CT_REQUEST)
{

}

QByteArray SitlCommandMrdRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    //dataStream << (quint8)COMMAND_ID_SITL_REQUEST;
    //dataStream << (quint8)0x01;
    quint8 header = (quint8)m_commandId;
    setHeaderAddressSize(header, sitlParams->memoryAddressSize);
    setHeaderWordSize(header, sitlParams->wordSize);
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8) 0x00;
    quint64 mcwd = 0x00;
    dataStream << mcwd;
    return data;
}

SitlCommandMwrAnswerCodec::SitlCommandMwrAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWR_ANSWER, COMMAND_ID_SITL_MWR, SITL_COMMAND_MASK_MWR, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMwrAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    //dataStream >> header;
    //quint8 sitlWordsCount;
    //dataStream >> sitlWordsCount;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MWR;
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = getFromHeaderWordSize(header);
    dataStream >> sitlParams->addressWord;
    quint8 sitlAnswer;
    bool ok;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMrdAnswerCodec::SitlCommandMrdAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRD_ANSWER, COMMAND_ID_SITL_MRD, SITL_COMMAND_MASK_MRD, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMrdAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    //dataStream >> header;
    //quint8 sitlWordsCount;
    //dataStream >> sitlWordsCount;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MRD;
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = getFromHeaderWordSize(header);
    dataStream >> sitlParams->addressWord;
    quint8 sitlAnswer;
    bool ok;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandListRequestCodec::SitlCommandListRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_LIST_REQUEST, COMMAND_ID_SITL_LIST, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandListRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    QByteArray sitlRawData;
    if(!isParamsMyCommand(param))
    {
        codecError->errType = CE_CANT_ENCODE;
        return sitlRawData;
    }
    codecError->errType = CE_NO_ERROR;
    QDataStream sitlDataStream(&sitlRawData, QIODevice::WriteOnly);
    sitlDataStream.setByteOrder(endian);
    quint8 mcwoc = (quint8)m_commandId;
    quint64 mcwa = 0;
    quint8 mcwdc = 0;
    quint64 mcwd = 0;
    sitlDataStream << mcwoc << mcwa << mcwdc << mcwd;
    return sitlRawData;
}

SitlCommandListAnswerCodec::SitlCommandListAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_LIST_ANSWER, COMMAND_ID_SITL_LIST, SITL_COMMAND_MASK_ALL_HEADER, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandListAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SitlCommandListParamsAnswer * sitlParams = new SitlCommandListParamsAnswer();
    if(!isDataMyCommand(data, endian))
    {
        codecError->errType = CE_CANT_DECODE;
    }
    else
    {
        codecError->errType = CE_NO_ERROR;
    }
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandIdenRequestCodec::SitlCommandIdenRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_IDEN_REQUEST, COMMAND_ID_SITL_IDEN, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandIdenRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    QByteArray sitlRawData;
    if(!isParamsMyCommand(param))
    {
        codecError->errType = CE_CANT_ENCODE;
        return sitlRawData;
    }
    codecError->errType = CE_NO_ERROR;
    QDataStream sitlDataStream(&sitlRawData, QIODevice::WriteOnly);
    sitlDataStream.setByteOrder(endian);
    quint8 mcwoc = (quint8)m_commandId;
    quint64 mcwa = 0;
    quint8 mcwdc = 0;
    quint64 mcwd = 0;
    sitlDataStream << mcwoc << mcwa << mcwdc << mcwd;
    return sitlRawData;
}

SitlCommandIdenAnswerCodec::SitlCommandIdenAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_IDEN_ANSWER, COMMAND_ID_SITL_IDEN, SITL_COMMAND_MASK_ALL_HEADER, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandIdenAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    if(isDataMyCommand(data, endian))
    {
        codecError->errType = CE_NO_ERROR;
    }
    else
    {
        codecError->errType = CE_CANT_DECODE;
    }
    SitlCommandIdenParamsAnswer * sitlParams = new SitlCommandIdenParamsAnswer();
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandSynerRequestCodec::SitlCommandSynerRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_SYNER_REQUEST, COMMAND_ID_SITL_SYNER, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandSynerRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    QByteArray sitlRawData;
    if(!isParamsMyCommand(param))
    {
        codecError->errType = CE_CANT_ENCODE;
        return sitlRawData;
    }
    codecError->errType = CE_NO_ERROR;
    QDataStream sitlDataStream(&sitlRawData, QIODevice::WriteOnly);
    sitlDataStream.setByteOrder(endian);
    quint8 mcwoc = (quint8)m_commandId;
    quint64 mcwa = 0;
    quint8 mcwdc = 0;
    quint64 mcwd = 0;
    sitlDataStream << mcwoc << mcwa << mcwdc << mcwd;
    return sitlRawData;
}

SitlCommandSynerAnswerCodec::SitlCommandSynerAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_SYNER_ANSWER, COMMAND_ID_SITL_SYNER, SITL_COMMAND_MASK_ALL_HEADER, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandSynerAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    if(isDataMyCommand(data, endian))
    {
        codecError->errType = CE_NO_ERROR;
    }
    else
    {
        codecError->errType = CE_CANT_DECODE;
    }
    SitlCommandSynerParamsAnswer * sitlParams = new SitlCommandSynerParamsAnswer();
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandIntAnswerCodec::SitlCommandIntAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_INT_ANSWER, COMMAND_ID_SITL_INT, SITL_COMMAND_MASK_ALL_HEADER, CT_ANSWER)
{
    setStatusIdSize(WS_64BIT);
}

void SitlCommandIntAnswerCodec::setStatusIdSize(WordSize size)
{
    statusIdSize = size;
}

QSharedPointer <CommandParams> SitlCommandIntAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = m_commandId;
    sitlParams->wordSize = statusIdSize;
    dataStream >> sitlParams->addressWord;
    sitlParams->irqNumber = (quint8)sitlParams->addressWord;
    quint8 sitlAnswer;
    bool ok;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    sitlParams->dataWord = mcwd;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMwxRequestCodec::SitlCommandMwxRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWX_REQUEST, COMMAND_ID_SITL_MWX, SITL_COMMAND_MASK_MWX, CT_REQUEST)
{

}

QByteArray SitlCommandMwxRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header = (quint8) COMMAND_ID_SITL_MWX;
    sitlParams->isMultipleTransaction = true;
    if(sitlParams->placeInTransaction == PIT_FIRST)
    {
        setHeaderUseAutoincrementAddress(header, sitlParams->useAddressAutoincrement);
        setHeaderAddressSize(header, sitlParams->memoryAddressSize);
        setHeaderWordSize(header, sitlParams->wordSize);
    }
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8) 0x00;
    quint64 mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    mcwd = sitlParams->dataWord << offset;
    dataStream << mcwd;
    return data;
}

SitlCommandMwxAnswerCodec::SitlCommandMwxAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWX_ANSWER, COMMAND_ID_SITL_MWX, SITL_COMMAND_MASK_MWX, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMwxAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MWX;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = PIT_FIRST;
    sitlParams->useAddressAutoincrement = getFromHeaderUseAutoincrementAddress(header);
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = getFromHeaderWordSize(header);
    dataStream >> sitlParams->addressWord;
    sitlParams->haveAddressInMessage = true;
    quint8 sitlAnswer;
    bool ok;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    sitlParams->haveWordInMessage = true;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMrxRequestCodec::SitlCommandMrxRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRX_REQUEST, COMMAND_ID_SITL_MRX, SITL_COMMAND_MASK_MRX, CT_REQUEST)
{

}

QByteArray SitlCommandMrxRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header = (quint8)COMMAND_ID_SITL_MRX;
    setHeaderUseAutoincrementAddress(header, sitlParams->useAddressAutoincrement);
    setHeaderAddressSize(header, sitlParams->memoryAddressSize);
    setHeaderWordSize(header, sitlParams->wordSize);
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8)(sitlParams->wordsCount - 1);
    quint64 mcwd = 0x00;
    dataStream << mcwd;
    return data;
}

SitlCommandMrxAnswerCodec::SitlCommandMrxAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRX_ANSWER, COMMAND_ID_SITL_MRX, SITL_COMMAND_MASK_MRX, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMrxAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MRX;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = PIT_FIRST;
    sitlParams->useAddressAutoincrement = getFromHeaderUseAutoincrementAddress(header);
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = getFromHeaderWordSize(header);
    dataStream >> sitlParams->addressWord;
    sitlParams->haveAddressInMessage = true;
    quint8 sitlAnswer;
    dataStream >> sitlAnswer;
    bool ok;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    sitlParams->haveWordInMessage = true;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMwiRequestCodec::SitlCommandMwiRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWI_REQUEST, COMMAND_ID_SITL_MWI, SITL_COMMAND_MASK_MWI, CT_REQUEST)
{

}

QByteArray SitlCommandMwiRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header = (quint8) COMMAND_ID_SITL_MWI;
    sitlParams->isMultipleTransaction = true;
    if(sitlParams->placeInTransaction == PIT_FIRST)
    {
        setHeaderUseAutoincrementAddress(header, sitlParams->useAddressAutoincrement);
        setHeaderAddressSize(header, sitlParams->memoryAddressSize);
        setHeaderWordSize(header, sitlParams->wordSize);
    }
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8) 0x00;
    quint64 mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    mcwd = sitlParams->dataWord << offset;
    dataStream << mcwd;
    return data;
}

SitlCommandMwiAnswerCodec::SitlCommandMwiAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MWI_ANSWER, COMMAND_ID_SITL_MWI, SITL_COMMAND_MASK_MWI, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMwiAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MWI;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = PIT_FIRST;
    sitlParams->useAddressAutoincrement = getFromHeaderUseAutoincrementAddress(header);
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = getFromHeaderWordSize(header);
    dataStream >> sitlParams->addressWord;
    sitlParams->haveAddressInMessage = true;
    quint8 sitlAnswer;
    bool ok;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    sitlParams->haveWordInMessage = true;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMrlRequestCodec::SitlCommandMrlRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRL_REQUEST, COMMAND_ID_SITL_MRL, SITL_COMMAND_MASK_MRL, CT_REQUEST)
{

}

QByteArray SitlCommandMrlRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header = (quint8)COMMAND_ID_SITL_MRL;
    setHeaderUseAutoincrementAddress(header, sitlParams->useAddressAutoincrement);
    setHeaderAddressSize(header, sitlParams->memoryAddressSize);
    //setHeaderWordSize(header, sitlParams->wordSize);
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8)0x00;
    quint64 mcwd = 0x00;
    dataStream << mcwd;
    return data;
}

SitlCommandMrlAnswerCodec::SitlCommandMrlAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRL_ANSWER, COMMAND_ID_SITL_MRL, SITL_COMMAND_MASK_MRL, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMrlAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MRL;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = PIT_FIRST;
    sitlParams->useAddressAutoincrement = getFromHeaderUseAutoincrementAddress(header);
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = WS_32BIT;
    dataStream >> sitlParams->addressWord;
    sitlParams->haveAddressInMessage = true;
    quint8 sitlAnswer;
    dataStream >> sitlAnswer;
    bool ok;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    sitlParams->haveWordInMessage = true;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandMrmRequestCodec::SitlCommandMrmRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRM_REQUEST, COMMAND_ID_SITL_MRM, SITL_COMMAND_MASK_MRM, CT_REQUEST)
{
}

QByteArray SitlCommandMrmRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header = (quint8)COMMAND_ID_SITL_MRM;
    setHeaderUseAutoincrementAddress(header, sitlParams->useAddressAutoincrement);
    setHeaderAddressSize(header, sitlParams->memoryAddressSize);
    //setHeaderWordSize(header, sitlParams->wordSize);
    dataStream << header;
    quint64 mcwa = sitlParams->addressWord;
    dataStream << mcwa;
    dataStream << (quint8)0x00;
    quint64 mcwd = 0x00;
    dataStream << mcwd;
    return data;
}

SitlCommandMrmAnswerCodec::SitlCommandMrmAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_MRM_ANSWER, COMMAND_ID_SITL_MRM, SITL_COMMAND_MASK_MRM, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandMrmAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_MRM;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = PIT_FIRST;
    sitlParams->useAddressAutoincrement = getFromHeaderUseAutoincrementAddress(header);
    sitlParams->memoryAddressSize = getFromHeaderAddressSize(header);
    sitlParams->wordSize = WS_32BIT;
    dataStream >> sitlParams->addressWord;
    sitlParams->haveAddressInMessage = true;
    quint8 sitlAnswer;
    dataStream >> sitlAnswer;
    bool ok;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    sitlParams->haveWordInMessage = true;
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandClineRequestCodec::SitlCommandClineRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_CLINE_REQUEST, COMMAND_ID_SITL_CLINE, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandClineRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    QByteArray sitlRawData;
    if(!isParamsMyCommand(param))
    {
        codecError->errType = CE_CANT_ENCODE;
        return sitlRawData;
    }
    codecError->errType = CE_NO_ERROR;
    QDataStream sitlDataStream(&sitlRawData, QIODevice::WriteOnly);
    sitlDataStream.setByteOrder(endian);
    quint8 mcwoc = (quint8)m_commandId;
    quint64 mcwa = 0;
    quint8 mcwdc = 0;
    quint64 mcwd = 0;
    sitlDataStream << mcwoc << mcwa << mcwdc << mcwd;
    return sitlRawData;
}

SitlCommandClineAnswerCodec::SitlCommandClineAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_CLINE_ANSWER, COMMAND_ID_SITL_CLINE, SITL_COMMAND_MASK_ALL_HEADER, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SitlCommandClineAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = COMMAND_ID_SITL_CLINE;
    quint64 mcwa;
    dataStream >> mcwa;
    sitlParams->cacheSize = (quint16)mcwa;
    quint8 sitlAnswer;
    dataStream >> sitlAnswer;
    bool ok;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    return QSharedPointer <CommandParams>(sitlParams);
}

SitlCommandClsetRequestCodec::SitlCommandClsetRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_CLSET_REQUEST, COMMAND_ID_SITL_CLSET, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandClsetRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header;
    header = (quint8)m_commandId;
    dataStream << header;
    quint64 mcwa = 0x00;
    dataStream << mcwa;
    quint8 mcwdc = 0x00;
    dataStream << mcwdc;
    quint64 mcwd = (quint8) sitlParams->cacheSize;
    dataStream << mcwd;
    return data;
}

SitlCommandSeqRequestCodec::SitlCommandSeqRequestCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_SEQ_REQUEST, COMMAND_ID_SITL_SEQ, SITL_COMMAND_MASK_ALL_HEADER, CT_REQUEST)
{

}

QByteArray SitlCommandSeqRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    quint8 header;
    header = SITL_COMMAND_ID_SEQ;
    setHeaderHaveWord(header, sitlParams->haveWordInMessage);
    setHeaderPIT(header, sitlParams->placeInTransaction);
    dataStream << header;
    quint64 mcwa = 0x00;
    dataStream << mcwa;
    dataStream << (quint8) 0x00;
    quint64 mcwd = 0x00;
    if(sitlParams->haveWordInMessage)
    {
        int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
        mcwd = sitlParams->dataWord << offset;
    }
    dataStream << mcwd;
    return data;
}

SitlCommandSeqAnswerCodec::SitlCommandSeqAnswerCodec()
    :SitlCommandFamilyCodec(CODEC_ID_SITL_SEQ_ANSWER, COMMAND_ID_SITL_SEQ, SITL_COMMAND_MASK_SEQ, CT_ANSWER)
{
    toDefault();
}

bool SitlCommandSeqAnswerCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    if(m_commandType != CT_ANSWER)
    {
        return false;
    }
    quint8 sitlHeader;
    dataStream >> sitlHeader;
    switch(sitlHeader)
    {
    case 0x51:
    case 0x52:
    case 0x53:
        return true;
    }
    return false;
}

void SitlCommandSeqAnswerCodec::toDefault()
{
    m_ws_commandId = COMMAND_ID_SITL_SEQ;
    m_ws_mas = MAS_64BIT;
    m_ws_wordSize = WS_64BIT;
    m_ws_addressWord = 0x00;
    m_ws_useAddressAutoincrement = false;
}

void SitlCommandSeqAnswerCodec::setCommandId(quint64 commandId)
{
    m_ws_commandId = commandId;
}

void SitlCommandSeqAnswerCodec::setMemoryAddressSize(MemoryAddressSize size)
{
    m_ws_mas = size;
}

void SitlCommandSeqAnswerCodec::setAddressWord(quint64 addressWord)
{
    m_ws_addressWord = addressWord;
}

void SitlCommandSeqAnswerCodec::setAddressAutoincrement(bool aai)
{
    m_ws_useAddressAutoincrement = aai;
}

void SitlCommandSeqAnswerCodec::setWordSize(WordSize wordSize)
{
    m_ws_wordSize = wordSize;
}

QSharedPointer <CommandParams> SitlCommandSeqAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitlCommandFamilyParams * sitlParams = new SitlCommandFamilyParams();
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    quint8 header;
    dataStream >> header;
    sitlParams->commandType = m_commandType;
    sitlParams->commandId = m_ws_commandId;
    sitlParams->useAddressAutoincrement = m_ws_useAddressAutoincrement;
    sitlParams->isMultipleTransaction = true;
    sitlParams->placeInTransaction = getHeaderPIT(header);
    bool ok;
    sitlParams->haveWordInMessage = haveWordInMessageFromSitlHeader(header, ok);
    //sitlParams->haveAddressInMessage = haveWordInMessageFromSitlHeader(header, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    sitlParams->memoryAddressSize = m_ws_mas;
    sitlParams->wordSize = m_ws_wordSize;//getFromHeaderWordSize(header);
    quint64 mcwa;
    dataStream >> mcwa;
    if(mcwa == 0)
    {
        sitlParams->haveAddressInMessage = false;
        if(m_ws_useAddressAutoincrement)
        {
            m_ws_addressWord += sitlParams->GetBytesWordSize();
        }
        sitlParams->addressWord = m_ws_addressWord;
    }
    else
    {
        sitlParams->haveAddressInMessage = true;
        sitlParams->addressWord = mcwa;
    }
    quint8 sitlAnswer;
    dataStream >> sitlAnswer;
    sitlParams->answerCode = answerCodeFromSitlAnswerCode(sitlAnswer, ok);
    if(ok == false)
    {
        codecError->errType = CE_CANT_DECODE;
    }
    quint64 mcwd;
    dataStream >> mcwd;
    int offset = calculateOffset(sizeof(mcwd) * 8, sitlParams->GetBytesWordSize() * 8, sitlParams->addressWord);
    sitlParams->dataWord = mcwd >> offset;
    return QSharedPointer <CommandParams>(sitlParams);
}
