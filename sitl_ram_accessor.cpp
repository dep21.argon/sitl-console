#include "sitl_ram_accessor.h"
#include <QtCore>

SitlRamAccessor::SitlRamAccessor(MemoryAddressSize addressSize, WordSize wordSize,  QObject *parent) : QObject(parent)
{
    setAddressSize(addressSize);
    setMinimunChunkSize(wordSize);
}

void SitlRamAccessor::setMinimunChunkSize(WordSize wordSize)
{
    m_minimunChunkSize = wordSize;
}

void SitlRamAccessor::setAddressSize(MemoryAddressSize memoryAddressSize)
{
    m_memoryAddressSize = memoryAddressSize;
}

QByteArray SitlRamAccessor::readMemory(quint64 address, quint64 sizeBytes, int timeout)
{
    //Рассчитываем кол-во посылаемых комманд чтения
    uint countOfFirstPacketsForAlignment[4] = {0, 0, 0, 0};
    uint countOfPacketsForEachSize[4] = {0, 0, 0, 0}; //for 8, 16, 32, 64 bits
    //quint64 bytesCount = sizeBytes;
    uint offsetForAlignment = address % wordSizeToBytes(WS_64BIT);
    quint64 bytesCount = sizeBytes - offsetForAlignment;
    for(int wsInt = WS_32BIT ; wsInt >= WS_8BIT; --wsInt)
    {
        countOfFirstPacketsForAlignment[wsInt] = offsetForAlignment / wordSizeToBytes((WordSize)wsInt);
        offsetForAlignment = offsetForAlignment % wordSizeToBytes((WordSize)wsInt);
    }
    for(int wsInt = WS_64BIT ; wsInt >= m_minimunChunkSize; --wsInt)
    {
        //WordSize ws = static_cast<WordSize>(wsInt);
        countOfPacketsForEachSize[wsInt] = bytesCount / wordSizeToBytes((WordSize)wsInt);
        bytesCount = bytesCount % wordSizeToBytes((WordSize)wsInt);
    }
    if(bytesCount > 0)
    {
        countOfPacketsForEachSize[m_minimunChunkSize] += 1;
    }
    QSet <quint64> unreadedBytesSet;
    for(quint64 i = 0; i < sizeBytes; ++i)
    {
        unreadedBytesSet.insert(address + i);
    }
    //Создаем место для хранения пришедших данных
    QByteArray receivedBytes;
    receivedBytes.resize(sizeBytes);
    receivedBytes.fill(0xFF);
    //Создаем комманды для чтения из памяти
    QList <QSharedPointer <CommandParams> > sendedCommands;
    quint64 currentAddress = address;
    for(int wsInt = WS_64BIT; wsInt >= WS_8BIT; --wsInt)
    {
        if(countOfFirstPacketsForAlignment[wsInt] > 0)
        {
            SitlCommandFamilyParams * mrxCmd =  new SitlCommandFamilyParams;
            mrxCmd->commandId = COMMAND_ID_SITL_MRX;
            mrxCmd->commandType = CT_REQUEST;
            mrxCmd->memoryAddressSize = m_memoryAddressSize;
            mrxCmd->addressWord = currentAddress;
            mrxCmd->wordSize = (WordSize)wsInt;
            mrxCmd->wordsCount = countOfFirstPacketsForAlignment[wsInt];
            mrxCmd->useAddressAutoincrement = true;
            sendedCommands.append(QSharedPointer <CommandParams>(mrxCmd));
            currentAddress += countOfFirstPacketsForAlignment[wsInt] * wordSizeToBytes((WordSize)wsInt);
        }
    }
    for(int wsInt = WS_64BIT; wsInt  >= WS_8BIT; --wsInt)
    {
        if(countOfPacketsForEachSize[wsInt] > 0)
        {
            SitlCommandFamilyParams * mrxCmd =  new SitlCommandFamilyParams;
            mrxCmd->commandId = COMMAND_ID_SITL_MRX;
            mrxCmd->commandType = CT_REQUEST;
            mrxCmd->memoryAddressSize = m_memoryAddressSize;
            mrxCmd->addressWord = currentAddress;
            mrxCmd->wordSize = (WordSize)wsInt;
            mrxCmd->wordsCount = countOfPacketsForEachSize[wsInt];
            mrxCmd->useAddressAutoincrement = true;
            sendedCommands.append(QSharedPointer <CommandParams>(mrxCmd));
            currentAddress += countOfPacketsForEachSize[wsInt] * wordSizeToBytes((WordSize)wsInt);
        }
    }
    QEventLoop sendEventLoop;
    QTimer sendTimer;
    sendTimer.setSingleShot(true);
    QTime sendTime;
    int leftTime = timeout;
    QObject::connect(this, SIGNAL(receivedSitlPacket()), &sendEventLoop, SLOT(quit()));
    QObject::connect(&sendTimer, SIGNAL(timeout()), &sendEventLoop, SLOT(quit()));
    m_receivedSitlAnswerList.clear();
    emit sendSitlRequestList(sendedCommands);
    do
    {
        sendTimer.start(leftTime);
        sendTime.start();
        sendEventLoop.exec();
        leftTime -= sendTime.elapsed();
        sendTimer.stop();
        if(m_receivedSitlAnswerList.size() > 0)
        {
            for(int i = 0; i < m_receivedSitlAnswerList.size(); ++i)
            {
                QSharedPointer <CommandParams> receivedCommand = m_receivedSitlAnswerList.at(i);
                if((receivedCommand->commandId == COMMAND_ID_SITL_MRX)
                        && (receivedCommand->commandType == CT_ANSWER))
                {
                    SitlCommandFamilyParams * mrxAnswer = (SitlCommandFamilyParams *)receivedCommand.data();
                    if(mrxAnswer->haveWordInMessage)
                    {
                        quint64 readedAddress = mrxAnswer->addressWord;
                        quint64 readedWord = mrxAnswer->dataWord;
                        uint readedWordSize = wordSizeToBytes(mrxAnswer->wordSize);
                        for(uint j = 0; j < readedWordSize; ++j)
                        {
                            quint64 bytePosition = readedAddress + j;
                            if(unreadedBytesSet.contains(bytePosition))
                            {
                                uint offset = bytePosition - address;
                                quint8 byteValue = (readedWord >> (j * 8)) & 0xFF;
                                receivedBytes[offset] = byteValue;
                                unreadedBytesSet.remove(bytePosition);
                            }
                        }
                    }
                }
            }
            m_receivedSitlAnswerList.clear();
        }
    } while((!unreadedBytesSet.isEmpty()) && (leftTime >= 0));
    if(!unreadedBytesSet.isEmpty())
    {
        QByteArray emptyData;
        return emptyData;
    }
    return receivedBytes;
}

void SitlRamAccessor::receiveSitlAnswerList(QList<QSharedPointer<CommandParams> > sitlAnswerList)
{
    m_receivedSitlAnswerList += sitlAnswerList;
    emit receivedSitlPacket();
}



uint SitlRamAccessor::wordSizeToBytes(WordSize wordSize)
{
    uint bytes;
    switch (wordSize)
    {
    case WS_8BIT:
        bytes = 1;
        break;
    case WS_16BIT:
        bytes =  2;
        break;
    case WS_32BIT:
        bytes = 4;
        break;
    case WS_64BIT:
        bytes = 8;
        break;
    default:
        qDebug() << "Invalid word size";
        bytes = 1;
    }
    return bytes;
}

uint SitlRamAccessor::memoryAddressSizeToBytes(MemoryAddressSize addressSize)
{
    uint bytes;
    switch(addressSize)
    {
    case MAS_16BIT:
        bytes = 2;
        break;
    case MAS_24BIT:
        bytes = 3;
        break;
    case MAS_32BIT:
        bytes = 4;
        break;
    case MAS_64BIT:
        bytes = 8;
        break;
    default:
        qDebug() << "Invalid address size";
        bytes = 4;
        break;
    }
    return bytes;
}

WordSize SitlRamAccessor::BytesToWordSize(uint bytes)
{
    switch(bytes)
    {
    case 1:
        return WS_8BIT;
        break;
    case 2:
        return WS_16BIT;
        break;
    case 4:
        return WS_32BIT;
        break;
    case 8:
        return WS_64BIT;
        break;
    default:
        qDebug() << "IMPOSSIBLE CONVERSION SitlRamAccessor::BytesToWordSize";
        return WS_8BIT;
        break;
    }
}

MemoryAddressSize SitlRamAccessor::BytesToMemoryAddressSize(uint bytes)
{
    switch(bytes)
    {
    case 2:
        return MAS_16BIT;
        break;
    case 3:
        return MAS_24BIT;
        break;
    case 4:
        return MAS_32BIT;
        break;
    case 8:
        return MAS_64BIT;
        break;
    default:
        qDebug() << "IMPOSSIBLE CONVERSION SitlRamAccessor::BytesToMemoryAddressSize";
        return MAS_32BIT;
        break;
    }
}

