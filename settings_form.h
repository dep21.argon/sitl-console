#ifndef SETTINGS_FORM_H
#define SETTINGS_FORM_H

#include <QDialog>
#include <sitl_client_settings_class.h>

namespace Ui {
class SettingsForm;
}

class SettingsForm : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsForm(SitlClientSettingsClass currentSettings, QWidget *parent = 0);
    void setSettings(SitlClientSettingsClass currentSettigns);
    void showSettings();
    void saveSettings();
    ~SettingsForm();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_checkRequestsCountSpinBox_valueChanged(int arg1);

signals:
    void settingsUpdated(SitlClientSettingsClass newSettings);

private:
    Ui::SettingsForm *ui;
    SitlClientSettingsClass settings;
};

#endif // SETTINGS_FORM_H
