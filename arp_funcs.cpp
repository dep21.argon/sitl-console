#include "arp_funcs.h"
#include <winsock2.h>
#include <iphlpapi.h>
#include <Windows.h>
#include <windows.h>
#include <qt_windows.h>
#include <iostream>

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

QByteArray getMacAddrFromIP(QHostAddress address, bool *successFlag)
{
    QByteArray resultMacAddr;
    resultMacAddr = QByteArray::fromHex("00 00 00 00 00 00");

    DWORD dwDestAddr = inet_addr(address.toString().toAscii()); //qFromBigEndian(address.toIPv4Address();
    DWORD dwSourceAddr = 0;
    MIB_IPFORWARDROW bestRoute;
    if(GetBestRoute(dwDestAddr, dwSourceAddr, &bestRoute) == NO_ERROR)
    {
        quint32 destIpAddr;//qFromBigEndian(address.toIPv4Address());
        if(bestRoute.dwForwardType == MIB_IPROUTE_TYPE_INDIRECT)
        {
            destIpAddr = bestRoute.dwForwardNextHop;
        }
        else if(bestRoute.dwForwardType == MIB_IPROUTE_TYPE_DIRECT)
        {
            destIpAddr = inet_addr(address.toString().toAscii());
        }
        else
        {
            *successFlag = false;
            return resultMacAddr;
        }
        DWORD retVal;
        quint32 srcIpAddr = INADDR_ANY;
        ULONG macAddr[2];
        //memset(&macAddr, 0xff, sizeof(macAddr));
        ULONG physAddrLen = 6;
        retVal = SendARP(destIpAddr, srcIpAddr, macAddr, &physAddrLen);
        if(retVal == NO_ERROR)
        {
            *successFlag = true;
            for(int i = 0; i < 6; ++i)
            {
                resultMacAddr[i] = ((quint8*)macAddr)[i];
            }
            return resultMacAddr;
        }
    }
    *successFlag = false;
    return resultMacAddr;
}
