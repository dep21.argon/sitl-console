#include "mac_address.h"

MacAddress::MacAddress()
{
    macAddress = QByteArray::fromHex("00:00:00:00:00:00");
}

MacAddress::MacAddress(QString macString)
{
    setMacFromString(macString);
    //MacAddress = QByteArray::fromHex(macString);
}

MacAddress::MacAddress(QByteArray rawMac)
{
    setMacFromByteArray(rawMac);
}

void MacAddress::setMacFromString(QString macString)
{
    macAddress = QByteArray::fromHex(macString.toAscii());
}

void MacAddress::setMacFromByteArray(QByteArray rawMac)
{
    macAddress = rawMac.left(MAC_SIZE);
}

QString MacAddress::toString()
{
    return macAddress.toHex().toUpper()
            .insert(10, ":")
            .insert(8, ":")
            .insert(6, ":")
            .insert(4, ":")
            .insert(2, ":");
}

QByteArray MacAddress::getMacByteArray()
{
    return macAddress;
}
