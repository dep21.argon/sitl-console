#ifndef COMMAND_CODEC_H
#define COMMAND_CODEC_H

#include <QByteArray>
#include <QDataStream>
#include <QSharedPointer>
#include "command_params.h"

enum CodecErrorType{CE_NO_ERROR, CE_CANT_DECODE, CE_CANT_ENCODE, CE_CODEC_NOT_FOUND};

class CodecError
{
public:
    CodecError();
    CodecErrorType errType;
    quint64 errCode;
};

class CommandCodec
{
public:
    CommandCodec(quint64 codecID);
    virtual ~CommandCodec();
    quint64 getCodecId();
    virtual QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError) = 0;
    virtual QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError) = 0;
    virtual bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian) = 0;
    virtual bool isParamsMyCommand(QSharedPointer <CommandParams> params) = 0;
private:
    quint64 m_codecId;
};

#endif // COMMAND_CODEC_H

