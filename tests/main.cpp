#include "test_sitl_assembler.h"
#include "test_sitl_codec.h"
#include "sitl_test_data.h"

int main()
{
    SitlTestData testData;

    sitlAssemblerTest test1(testData);
    QTest::qExec(&test1);

    SitlCommandFamilyCodecTest test2(testData);
    QTest::qExec(&test2);
}
