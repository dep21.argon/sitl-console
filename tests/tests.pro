#-------------------------------------------------
#
# Project created by QtCreator 2015-02-05T15:53:23
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_sitlassemblertest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    test_sitl_assembler.cpp \
    sitl_test_data.cpp \
    ../command_codec.cpp \
    ../command_params.cpp \
    ../sitl_assembler.cpp \
    ../sitl_command_family_params.cpp \
    main.cpp \
    ../sitl_command_family_codec.cpp \
    ../command_builder.cpp \
    test_sitl_codec.cpp \
    ../sitl_command_global_codec.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    test_sitl_assembler.h \
    sitl_test_data.h \
    ../command_codec.h \
    ../command_params.h \
    ../sitl_assembler.h \
    ../sitl_command_family_params.h \
    test_sitl_codec.h \
    ../sitl_command_family_codec.h \
    ../command_builder.h \
    ../sitl_command_global_codec.h
