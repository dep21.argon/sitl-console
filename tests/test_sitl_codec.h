#ifndef TEST_SITL_CODEC_H
#define TEST_SITL_CODEC_H

#include <QTest>
#include "sitl_test_data.h"
#include "../sitl_command_family_params.h"
#include "../sitl_command_family_codec.h"
#include "../sitl_command_global_codec.h"
#include "../command_builder.h"
#include "../sitoip_command_family_params.h"
#include "../sitoip_command_family_codec.h"

class SitlCommandFamilyCodecTest : public QObject
{
    Q_OBJECT
public:
    SitlCommandFamilyCodecTest(SitlTestData testData);
    SitlTestData testData;
    SitlCommandGlobalCodec sitlCodec;
    //CommandBuilder commandBuilder;
private slots:
    void encode_data();
    void encode();
    void decode_data();
    void decode();
};


#endif // TEST_SITL_CODEC_H

