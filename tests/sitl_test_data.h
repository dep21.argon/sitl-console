#ifndef SITL_TEST_DATA_H
#define SITL_TEST_DATA_H

#include <QString>
#include <QByteArray>
#include <QList>

#include "../sitl_command_family_params.h"

class SitlTestItem
{
public:
    QString commandString;
    QList <SitlCommandFamilyParams> commandsList;
    QList <QByteArray> encodedCommandsList;
    bool encode;
    bool decode;
};

class SitlTestData
{
public:
    SitlTestData();
    void addTests();
    QList <SitlTestItem> testItems;
};

#endif // SITL_TEST_DATA_H

