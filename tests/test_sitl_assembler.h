#ifndef TEST_SITL_ASSEMBLER_H
#define TEST_SITL_ASSEMBLER_H

#include <QtTest>
#include "sitl_test_data.h"
#include "../sitl_command_family_params.h"
#include "../sitl_assembler.h"


class sitlAssemblerTest : public QObject
{
    Q_OBJECT

public:
    sitlAssemblerTest(SitlTestData testData);
    SitlTestData testData;
    SitlAssembler assembler;

private Q_SLOTS:
    void assemble_data();
    void assemble();
    void disassemble_data();
    void disassemble();
};

#endif // TEST_SITL_ASSEMBLER_H

