#include "test_sitl_codec.h"
#include <QDebug>

SitlCommandFamilyCodecTest::SitlCommandFamilyCodecTest(SitlTestData testData)
{
    this->testData = testData;

    CommandCodec * codec01 = (CommandCodec *) new SitlCommandMwrRequestCodec();
    CommandCodec * codec02 = (CommandCodec *) new SitlCommandMwrAnswerCodec();
    CommandCodec * codec03 = (CommandCodec *) new SitlCommandMrdRequestCodec();
    CommandCodec * codec04 = (CommandCodec *) new SitlCommandMrdAnswerCodec();
    CommandCodec * codec05 = (CommandCodec *) new SitlCommandListRequestCodec();
    CommandCodec * codec06 = (CommandCodec *) new SitlCommandListAnswerCodec();
    CommandCodec * codec07 = (CommandCodec *) new SitlCommandIdenRequestCodec();
    CommandCodec * codec08 = (CommandCodec *) new SitlCommandIdenAnswerCodec();
    CommandCodec * codec09 = (CommandCodec *) new SitlCommandSynerRequestCodec();
    CommandCodec * codec10 = (CommandCodec *) new SitlCommandSynerAnswerCodec();
    CommandCodec * codec11 = (CommandCodec *) new SitlCommandMwxRequestCodec();
    CommandCodec * codec12 = (CommandCodec *) new SitlCommandMwxAnswerCodec();
    CommandCodec * codec13 = (CommandCodec *) new SitlCommandMrxRequestCodec();
    CommandCodec * codec14 = (CommandCodec *) new SitlCommandMrxAnswerCodec();
    CommandCodec * codec15 = (CommandCodec *) new SitlCommandMwiRequestCodec();
    CommandCodec * codec16 = (CommandCodec *) new SitlCommandMwiAnswerCodec();
    CommandCodec * codec17 = (CommandCodec *) new SitlCommandMrlRequestCodec();
    CommandCodec * codec18 = (CommandCodec *) new SitlCommandMrlAnswerCodec();
    CommandCodec * codec19 = (CommandCodec *) new SitlCommandMrmRequestCodec();
    CommandCodec * codec20 = (CommandCodec *) new SitlCommandMrmAnswerCodec();
    CommandCodec * codec21 = (CommandCodec *) new SitlCommandSeqRequestCodec();
    CommandCodec * codec22 = (CommandCodec *) new SitlCommandSeqAnswerCodec();
    CommandCodec * codec23 = (CommandCodec *) new SitlCommandIntAnswerCodec();
    CommandCodec * codec24 = (CommandCodec *) new SitlCommandClineRequestCodec();
    CommandCodec * codec25 = (CommandCodec *) new SitlCommandClineAnswerCodec();
    CommandCodec * codec26 = (CommandCodec *) new SitlCommandClsetRequestCodec();
    sitlCodec.addCodec(codec01);
    sitlCodec.addCodec(codec02);
    sitlCodec.addCodec(codec03);
    sitlCodec.addCodec(codec04);
    sitlCodec.addCodec(codec05);
    sitlCodec.addCodec(codec06);
    sitlCodec.addCodec(codec07);
    sitlCodec.addCodec(codec08);
    sitlCodec.addCodec(codec09);
    sitlCodec.addCodec(codec10);
    sitlCodec.addCodec(codec11);
    sitlCodec.addCodec(codec12);
    sitlCodec.addCodec(codec13);
    sitlCodec.addCodec(codec14);
    sitlCodec.addCodec(codec15);
    sitlCodec.addCodec(codec16);
    sitlCodec.addCodec(codec17);
    sitlCodec.addCodec(codec18);
    sitlCodec.addCodec(codec19);
    sitlCodec.addCodec(codec20);
    sitlCodec.addCodec(codec21);
    sitlCodec.addCodec(codec22);
    sitlCodec.addCodec(codec23);
    sitlCodec.addCodec(codec24);
    sitlCodec.addCodec(codec25);
    sitlCodec.addCodec(codec26);
    //SitoipCommandFamilyCodec * sitoipCodec = SitoipCommandFamilyCodec();
    //commandBuilder.registerCodec(sitoipCodec);
}

void SitlCommandFamilyCodecTest::encode_data()
{
    QTest::addColumn<SitlCommandFamilyParams>("sitlParams");
    QTest::addColumn<QByteArray>("sitlRawData");
    for(int i = 0; i < testData.testItems.size(); ++i)
    {
        SitlTestItem testItem = testData.testItems.at(i);
        if(testItem.encode == true)
        {
            for(int j = 0; j < testItem.commandsList.size(); ++j)
            {
                QTest::newRow(testItem.commandString.toLocal8Bit().data()) << testItem.commandsList.at(j) << testItem.encodedCommandsList.at(j);
            }
        }
    }
}

void SitlCommandFamilyCodecTest::encode()
{
    QFETCH(SitlCommandFamilyParams, sitlParams);
    QFETCH(QByteArray, sitlRawData);

    CodecError codecError;
    QByteArray rawData;
    SitlCommandFamilyParams * ptrSitlParams = new SitlCommandFamilyParams();
    *ptrSitlParams = sitlParams;
    QSharedPointer <CommandParams> spSitlParams(ptrSitlParams);
    rawData = sitlCodec.encode(spSitlParams, QDataStream::BigEndian, &codecError);

    QCOMPARE(codecError.errType, CE_NO_ERROR);
    QCOMPARE(rawData, sitlRawData);
}

void SitlCommandFamilyCodecTest::decode_data()
{
    QTest::addColumn<SitlCommandFamilyParams>("sitlParams");
    QTest::addColumn<QByteArray>("sitlRawData");
    for(int i = 0; i < testData.testItems.size(); ++i)
    {
        SitlTestItem testItem = testData.testItems.at(i);
        if(testItem.decode == true)
        {
            for(int j = 0; j < testItem.commandsList.size(); ++j)
            {
                QTest::newRow(testItem.commandString.toLocal8Bit().data()) << testItem.commandsList.at(j) << testItem.encodedCommandsList.at(j);
            }
        }
    }
}

void SitlCommandFamilyCodecTest::decode()
{
    QFETCH(SitlCommandFamilyParams, sitlParams);
    QFETCH(QByteArray, sitlRawData);

    CodecError codecError;
    QSharedPointer <CommandParams> spSitlParams = sitlCodec.decode(sitlRawData, QDataStream::BigEndian, &codecError);


    QCOMPARE(codecError.errType, CE_NO_ERROR);
    SitlCommandFamilyParams paramsToSitl = *(SitlCommandFamilyParams *)spSitlParams.data();
    QCOMPARE(sitlParams, paramsToSitl);
}
