#include "test_sitl_assembler.h"
#include <QString>
#include <QDebug>


sitlAssemblerTest::sitlAssemblerTest(SitlTestData testData)
{
    this->testData = testData;
    SitlCommandAssemblerMWR * sitlMwr = new SitlCommandAssemblerMWR();
    SitlCommandAssemblerMRD * sitlMrd = new SitlCommandAssemblerMRD();
    SitlCommandAssemblerINT * sitlInt = new SitlCommandAssemblerINT();
    SitlCommandAssemblerLIST * sitlList = new SitlCommandAssemblerLIST();
    SitlCommandAssemblerIDEN * sitlIden = new SitlCommandAssemblerIDEN();
    SitlCommandAssemblerSYNER * sitlSyner = new SitlCommandAssemblerSYNER();
    SitlCommandAssemblerMWX * sitlMwx = new SitlCommandAssemblerMWX();
    SitlCommandAssemblerMRX * sitlMrx = new SitlCommandAssemblerMRX();
    SitlCommandAssemblerMWI * sitlMwi = new SitlCommandAssemblerMWI();
    SitlCommandAssemblerMRL * sitlMrl = new SitlCommandAssemblerMRL();
    SitlCommandAssemblerMRM * sitlMrm = new SitlCommandAssemblerMRM();
    SitlCommandAssemblerCLINE * sitlCline = new SitlCommandAssemblerCLINE();
    SitlCommandAssemblerCLSET * sitlClset = new SitlCommandAssemblerCLSET();
    assembler.addCommandAssembler(sitlMwr);
    assembler.addCommandAssembler(sitlMrd);
    assembler.addCommandAssembler(sitlInt);
    assembler.addCommandAssembler(sitlList);
    assembler.addCommandAssembler(sitlIden);
    assembler.addCommandAssembler(sitlSyner);
    assembler.addCommandAssembler(sitlMwx);
    assembler.addCommandAssembler(sitlMrx);
    assembler.addCommandAssembler(sitlMwi);
    assembler.addCommandAssembler(sitlMrl);
    assembler.addCommandAssembler(sitlMrm);
    assembler.addCommandAssembler(sitlCline);
    assembler.addCommandAssembler(sitlClset);
}

Q_DECLARE_METATYPE(QList <SitlCommandFamilyParams>)

void sitlAssemblerTest::assemble_data()
{
    QTest::addColumn<QString>("sitlString");
    QTest::addColumn<QList <SitlCommandFamilyParams> >("sitlParamsList");
    for(int i = 0; i < testData.testItems.size(); ++i)
    {
        SitlTestItem testItem = testData.testItems.at(i);
        if(testItem.encode == true)
        {
            QTest::newRow(testItem.commandString.toLocal8Bit().data()) << testItem.commandString << testItem.commandsList;
        }
    }
}

void sitlAssemblerTest::assemble()
{
    QFETCH(QString, sitlString);
    QFETCH(QList <SitlCommandFamilyParams>, sitlParamsList);

    QStringList sitlArguments = sitlString.split(" ",QString::SkipEmptyParts);

    QList <SitlCommandFamilyParams> paramsList = assembler.assemble(sitlArguments);
    QCOMPARE(paramsList.size(), sitlParamsList.size());
    for(int i = 0; i < sitlParamsList.size(); ++i)
    {
        QCOMPARE(paramsList.at(i), sitlParamsList.at(i));
    }
    //QVERIFY2(true, "Failure");
}

void sitlAssemblerTest::disassemble_data()
{
    QTest::addColumn<QString>("sitlString");
    QTest::addColumn<SitlCommandFamilyParams>("sitlParams");
    for(int i = 0; i < testData.testItems.size(); ++i)
    {
        SitlTestItem testItem = testData.testItems.at(i);
        if(testItem.decode == true)
        {
            QTest::newRow(testItem.commandString.toLocal8Bit().data()) << testItem.commandString << testItem.commandsList.at(0);
        }
    }
}

void sitlAssemblerTest::disassemble()
{
    QFETCH(QString, sitlString);
    QFETCH(SitlCommandFamilyParams, sitlParams);

    QString result = assembler.disassemble(sitlParams);
    QCOMPARE(result, sitlString);
    //QVERIFY2(true, "Failure");
}

//QTEST_APPLESS_MAIN(sitlAssemblerTest)

//#include "tst_sitlassemblertest.moc"
