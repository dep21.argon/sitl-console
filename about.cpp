#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->textBrowser->clear();
    ui->textBrowser->append(trUtf8("Клиент для микроконтроллера Milandr с поддержкой языка SITL"));
    ui->textBrowser->append(trUtf8("Версия: ") + BUILD_VERSION);
    //ui->textBrowser->append(trUtf8("Ревизия: ") + BUILD_ID);
    ui->textBrowser->append(trUtf8("Собрано: ") + BUILD_DATE + " " + BUILD_TIME);
    ui->textBrowser->append(trUtf8("Разработчик: Сараев Д. А."));
    ui->textBrowser->append(trUtf8("© 2015 ОАО \"НИИ \"Аргон\""));
    ui->textBrowser->append("<a href=\"http://argon.ru\">http://argon.ru</a>");
}

About::~About()
{
    delete ui;
}
