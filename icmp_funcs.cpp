#include "icmp_funcs.h"

//win32
#include <winsock2.h>
#include <iphlpapi.h>
#include <icmpapi.h>

#include <QFutureWatcher>

void win32Ping(QHostAddress ipAddr, uint timeoutMsec, PingStatus * status, uint * roundTripTimeMsec)
{
    *roundTripTimeMsec = 0;
    *status = PING_ERROR;
    HANDLE hIcmpFile;
    unsigned long ipaddr = INADDR_NONE;
    DWORD dwRetVal = 0;
    char SendData[32] = "PING FROM ARGON SITL CLIENT 0.1";
    /*
    {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
        0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
        0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F
    };
    */
    LPVOID ReplyBuffer = NULL;
    DWORD ReplySize = 0;

    ipaddr = inet_addr(ipAddr.toString().toAscii());
    if (ipaddr == INADDR_NONE)
    {
        *status = PING_ERROR;
        return;
    }

    hIcmpFile = IcmpCreateFile();
    if (hIcmpFile == INVALID_HANDLE_VALUE)
    {
        *status = PING_ERROR;
        return;
    }

    ReplySize = sizeof(ICMP_ECHO_REPLY) + sizeof(SendData);
    ReplyBuffer = (VOID*) malloc(ReplySize);
    if (ReplyBuffer == NULL)
    {
        *status = PING_ERROR;
        return;
    }

    dwRetVal = IcmpSendEcho(hIcmpFile, ipaddr, SendData, sizeof(SendData),
        NULL, ReplyBuffer, ReplySize, timeoutMsec);
    if (dwRetVal != 0) {
        PICMP_ECHO_REPLY pEchoReply = (PICMP_ECHO_REPLY)ReplyBuffer;

        switch(pEchoReply->Status)
        {
            case IP_SUCCESS:
                *status = PING_SUCCESS;
                break;
            case IP_REQ_TIMED_OUT:
                *status = PING_TIMEOUT;
                break;
            default:
                *status = PING_ERROR;
        }
        *roundTripTimeMsec = pEchoReply->RoundTripTime;
    }
    else {
        *status = PING_ERROR;
    }
}

IcmpRequest::IcmpRequest()
{
    QObject::connect(&watcher, SIGNAL(finished()), this, SLOT(slotPingReady()));
}

void IcmpRequest::sendPing(QHostAddress ipAddr, uint timeoutMsec)
{
    future = QtConcurrent::run(win32Ping, ipAddr, timeoutMsec, &pingStatus, &realTimeout);
    watcher.setFuture(future);
}

void IcmpRequest::slotPingReady()
{
    emit signalPingStatus(pingStatus, realTimeout);
}

IcmpRequestSequence::IcmpRequestSequence()
{
    QObject::connect(&icmpRequest, SIGNAL(signalPingStatus(PingStatus,uint)), SLOT(slotPingStatus(PingStatus,uint)));
}

void IcmpRequestSequence::sendPingSequence(QHostAddress ipAddr, uint timeoutMsec, uint timesToSend)
{
    stopIcmpRequestNow = false;
    sendTotal = 0;
    successSend = 0;
    errorSend = 0;
    minSendTime = 0;
    maxSendTime = 0;
    avgSendTime = 0;
    totalSendTime = 0;
    this->ipAddr = ipAddr;
    this->timeoutMsec = timeoutMsec;
    this->needSendTotal = timesToSend;
    icmpRequest.sendPing(ipAddr, timeoutMsec);
}

void IcmpRequestSequence::stopPingSequence()
{
    stopIcmpRequestNow = true;
}

void IcmpRequestSequence::slotPingStatus(PingStatus status, uint timeToAnswerMsec)
{
    ++sendTotal;
    if(status == PING_SUCCESS)
    {
        ++successSend;
        if(sendTotal == 1)
        {
            minSendTime = timeToAnswerMsec;
            maxSendTime = timeToAnswerMsec;
            avgSendTime = timeToAnswerMsec;
            totalSendTime = timeToAnswerMsec;
        }
        else
        {
            minSendTime = qMin(minSendTime, timeToAnswerMsec);
            maxSendTime = qMax(maxSendTime, timeToAnswerMsec);
            totalSendTime += timeToAnswerMsec;
            avgSendTime = totalSendTime / successSend;
        }
    }
    else
    {
        ++errorSend;
    }
    emit signalPingStatusSequence(status, timeToAnswerMsec, sendTotal);
    if(stopIcmpRequestNow || ((sendTotal == needSendTotal) && (needSendTotal != 0)))
    {
        emit signalPingStatistics(sendTotal, successSend, errorSend, minSendTime, maxSendTime, avgSendTime);
    }
    else
    {
        icmpRequest.sendPing(ipAddr, timeoutMsec);
    }
}
