#include "sitl_client_settings_class.h"
#include <QSettings>

SitlClientSettingsClass::SitlClientSettingsClass()
{
    needShowNetworkDataInLog = false;
    pingRequestCount = 10;
    pingRequestTimeout = 3000;
    checkConnection = true;
    checkInterval = 2000;
    checkTimeout = 2000;
    checkRequests = 4;
    checkMaxFailRequestsToBeStable = 0;
    disconnectFromDeviceIfCheckFaild = false;
    getDeviceSettingsOnConnect = true;
    sitlIntStatusSize = WS_64BIT;
    maxSitlWordsInSitoipPacket = 76;
    arpTableAddress = 0x200006BC;
    arpTableMaxRecords = 20;
}

void SitlClientSettingsClass::loadSettingsFromFile(QString fileName)
{
    QSettings fileSettings(fileName, QSettings::IniFormat);
    needShowNetworkDataInLog = fileSettings.value("log/showNetworkDataInLog",needShowNetworkDataInLog).toBool();
    pingRequestCount = fileSettings.value("ping/requestCont",pingRequestCount).toInt();
    pingRequestTimeout = fileSettings.value("ping/requestTimeout",pingRequestTimeout).toInt();
    checkConnection = fileSettings.value("connection/needCheck",checkConnection).toBool();
    checkInterval = fileSettings.value("connection/checkInterval",checkInterval).toInt();
    checkTimeout = fileSettings.value("connection/checkTimeout",checkTimeout).toInt();
    checkRequests = fileSettings.value("connection/checkRequests",checkRequests).toInt();
    checkMaxFailRequestsToBeStable = fileSettings.value("connection/checkMaxFailRequestsToBeStable",checkMaxFailRequestsToBeStable).toInt();
    disconnectFromDeviceIfCheckFaild = fileSettings.value("connection/disconnectFromDeviceIfCheckFaild",disconnectFromDeviceIfCheckFaild).toBool();
    getDeviceSettingsOnConnect = fileSettings.value("connection/getDeviceSettingsOnConnect",getDeviceSettingsOnConnect).toBool();
    uint sitlIntStatSize = fileSettings.value("sitl/statSize", 64).toInt();

    switch(sitlIntStatSize)
    {
        case 8:
           sitlIntStatusSize = WS_8BIT;
            break;
        case 16:
           sitlIntStatusSize = WS_16BIT;
            break;
        case 32:
           sitlIntStatusSize = WS_32BIT;
            break;
        case 64:
           sitlIntStatusSize = WS_64BIT;
            break;
        default:
           sitlIntStatusSize = WS_64BIT;
            break;
    }
    maxSitlWordsInSitoipPacket = fileSettings.value("sitl/maxSitlWordsInSitoipPacket", maxSitlWordsInSitoipPacket).toInt();
    arpTableAddress = fileSettings.value("arpTable/address", arpTableAddress).toULongLong();
    arpTableMaxRecords = fileSettings.value("arpTable/maxRecords", arpTableMaxRecords).toUInt();
}

void SitlClientSettingsClass::saveSettingsToFile(QString fileName)
{
    QSettings fileSettings(fileName, QSettings::IniFormat);
    fileSettings.setValue("log/showNetworkDataInLog",needShowNetworkDataInLog);
    fileSettings.setValue("ping/requestCont",pingRequestCount);
    fileSettings.setValue("ping/requestTimeout",pingRequestTimeout);
    fileSettings.setValue("connection/needCheck",checkConnection);
    fileSettings.setValue("connection/checkInterval",checkInterval);
    fileSettings.setValue("connection/checkTimeout",checkTimeout);
    fileSettings.setValue("connection/checkRequests",checkRequests);
    fileSettings.setValue("connection/checkMaxFailRequestsToBeStable",checkMaxFailRequestsToBeStable);
    fileSettings.setValue("connection/disconnectFromDeviceIfCheckFaild",disconnectFromDeviceIfCheckFaild);
    fileSettings.setValue("connection/getDeviceSettingsOnConnect",getDeviceSettingsOnConnect);
    uint sitlIntStatSize = 64;
    switch(sitlIntStatusSize)
    {
        case WS_8BIT:
            sitlIntStatSize = 8;
            break;
        case WS_16BIT:
            sitlIntStatSize = 16;
            break;
        case WS_32BIT:
            sitlIntStatSize = 32;
            break;
        case WS_64BIT:
            sitlIntStatSize = 64;
            break;
        default:
            sitlIntStatSize = 64;
            break;
    }
    fileSettings.setValue("sitl/statSize", sitlIntStatSize);
    fileSettings.setValue("sitl/maxSitlWordsInSitoipPacket", maxSitlWordsInSitoipPacket);
    fileSettings.setValue("arpTable/address", arpTableAddress);
    fileSettings.setValue("arpTable/maxRecords", arpTableMaxRecords);
}
