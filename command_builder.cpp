#include "command_builder.h"

CommandBuilder::CommandBuilder(QObject *parent): QObject(parent)
{
    setEndian(QDataStream::BigEndian);
}

void CommandBuilder::setEndian(QDataStream::ByteOrder endian)
{
    this->endian = endian;
}

QDataStream::ByteOrder CommandBuilder::getEndian()
{
    return this->endian;
}

void CommandBuilder::registerCodec(CommandCodec * codec)
{
    quint64 codecId = codec->getCodecId();
    m_codecs.insert(codecId, codec);
}

void CommandBuilder::unregisterCodec(quint64 codecId)
{
    m_codecs.remove(codecId);
}

CommandCodec * CommandBuilder::getCodecFromData(QByteArray data)
{
    return getCodecFromData(data, getEndian());
}

CommandCodec * CommandBuilder::getCodecFromData(QByteArray data, QDataStream::ByteOrder endian)
{
    QMap <quint64, CommandCodec*>::const_iterator i = m_codecs.begin();
    while(i != m_codecs.end())
    {
        if(i.value()->isDataMyCommand(data, endian))
        {
            return i.value();
        }
        ++i;
    }
    return NULL;
}

CommandCodec * CommandBuilder::getCodecFromParams(QSharedPointer <CommandParams> param)
{
    QMap <quint64, CommandCodec*>::const_iterator i = m_codecs.begin();
    while(i != m_codecs.end())
    {
        if(i.value()->isParamsMyCommand(param))
        {
            return i.value();
        }
        ++i;
    }
    return NULL;
}

QByteArray CommandBuilder::encode(QSharedPointer <CommandParams> params, CodecError * codecError)
{
    return encode(params, getEndian(), codecError);
}

QByteArray CommandBuilder::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    QByteArray data;
    CommandCodec * codec = getCodecFromParams(param);
    if(codec == NULL)
    {
        codecError->errType = CE_CODEC_NOT_FOUND;
        return data;
    }
    return codec->encode(param, endian, codecError);
}

QSharedPointer <CommandParams> CommandBuilder::decode(QByteArray rawData, CodecError * codecError)
{
    return decode(rawData, getEndian(), codecError);
}

QSharedPointer <CommandParams> CommandBuilder::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    CommandCodec * codec = getCodecFromData(data, endian);
    if(codec == NULL)
    {
        codecError->errType = CE_CODEC_NOT_FOUND;
        return QSharedPointer <CommandParams>(NULL);
    }
    return codec->decode(data, endian, codecError);
}

void CommandBuilder::encodeSlot(QSharedPointer <CommandParams> params)
{
    QByteArray rawData;
    CodecError codecError;
    rawData = encode(params, &codecError);
    if(codecError.errType == CE_NO_ERROR)
    {
        emit encodeDoneSignal(rawData);
    }
    else
    {
        emit encodeErrorSignal();
    }
}

void CommandBuilder::decodeSlot(QByteArray rawData)
{
    QSharedPointer <CommandParams> params;
    CodecError codecError;
    params = decode(rawData, &codecError);
    if(codecError.errType == CE_NO_ERROR)
    {
        emit decodeDoneSignal(params);
    }
    else
    {
        emit decodeErrorSignal();
    }
}
