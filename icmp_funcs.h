#ifndef ICMP_FUNCS_H
#define ICMP_FUNCS_H

#include <QtCore>
#include <QHostAddress>
#include <QFutureWatcher>

enum PingStatus{PING_SUCCESS, PING_ERROR, PING_TIMEOUT};

PingStatus ping(QHostAddress, int timeout);

class IcmpRequest: public QObject
{
Q_OBJECT
public:
    IcmpRequest();
    void sendPing(QHostAddress ipAddr, uint timeoutMsec);
private:
    QFuture <void> future;
    QFutureWatcher <void> watcher;
    PingStatus pingStatus;
    uint realTimeout;
signals:
    void signalPingStatus(PingStatus status, uint timeToAnswerMsec);
public slots:
    void slotPingReady();
};

class IcmpRequestSequence: public QObject
{
Q_OBJECT
public:
    IcmpRequestSequence();
    void sendPingSequence(QHostAddress ipAddr, uint timeoutMsec, uint timesToSend);
    void stopPingSequence();
signals:
    void signalPingStatusSequence(PingStatus status, uint timeToAnswer, uint numberInSequence);
    void signalPingStatistics(uint pingTotalRequests, uint successRequests, uint failRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec);
public slots:
    void slotPingStatus(PingStatus status, uint timeToAnswerMsec);
private:
    QHostAddress ipAddr;
    uint timeoutMsec;
    uint sendTotal;
    uint needSendTotal;
    uint successSend;
    uint errorSend;
    uint minSendTime;
    uint maxSendTime;
    uint avgSendTime;
    uint totalSendTime;
    IcmpRequest icmpRequest;
    bool stopIcmpRequestNow;
};

void win32Ping(QHostAddress ipAddr, uint timeoutMsec, PingStatus * status, uint * roundTripTimeMsec);
extern void win32Ping(QHostAddress ipAddr, uint timeoutMsec, PingStatus * status, uint * roundTripTimeMsec);


#endif // ICMP_FUNCS_H

