#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "arp_funcs.h"
#include "about.h"
#include "icmp_funcs.h"
#include "mac_address.h"
#include "settings_form.h"
#include <windows.h>

#define SETTINGS_FILE "sitl_udp_client.ini"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString Octet = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";
    ui->deviceIpAddressLineEdit->setValidator(new QRegExpValidator(QRegExp("^" + Octet + "\\." + Octet + "\\." + Octet + "\\." + Octet + "$"), ui->deviceIpAddressLineEdit));
    ui->logTextEdit->clear();
    sitlAssembler = new SitlAssembler();
    SitlCommandAssemblerMWR * sitlCommandAssemblerMWR = new SitlCommandAssemblerMWR;
    SitlCommandAssemblerMRD * sitlCommandAssemblerMRD = new SitlCommandAssemblerMRD;
    SitlCommandAssemblerINT * sitlCommandAssemblerINT = new SitlCommandAssemblerINT;
    SitlCommandAssemblerSYNER * sitlCommandAssemblerSYNER = new SitlCommandAssemblerSYNER;
    SitlCommandAssemblerIDEN * sitlCommandAssemblerIDEN = new SitlCommandAssemblerIDEN;
    SitlCommandAssemblerLIST * sitlCommandAssemblerLIST = new SitlCommandAssemblerLIST;
    SitlCommandAssemblerMWX * sitlCommandAssemblerMWX = new SitlCommandAssemblerMWX;
    SitlCommandAssemblerMRX * sitlCommandAssemblerMRX = new SitlCommandAssemblerMRX;
    SitlCommandAssemblerMWI * sitlCommandAssemblerMWI = new SitlCommandAssemblerMWI;
    SitlCommandAssemblerMRL * sitlCommandAssemblerMRL = new SitlCommandAssemblerMRL;
    SitlCommandAssemblerMRM * sitlCommandAssemblerMRM = new SitlCommandAssemblerMRM;
    SitlCommandAssemblerCLINE * sitlCommandAssemblerCLINE = new SitlCommandAssemblerCLINE;
    SitlCommandAssemblerCLSET * sitlCommandAssemblerCLSET = new SitlCommandAssemblerCLSET;
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMWR);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMRD);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerINT);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerSYNER);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerIDEN);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerLIST);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMWX);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMRX);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMWI);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMRL);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerMRM);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerCLINE);
    sitlAssembler->addCommandAssembler(sitlCommandAssemblerCLSET);
    isDeviceConnected = false;

    commandBuilder = new CommandBuilder();
    sitoIpCommandBuilder = new CommandBuilder();
    settingsCommandBuilder = new CommandBuilder();

    sitlCommandGlobalCodec = new SitlCommandGlobalCodec();
    sitlCommandMwrRequestCodec = new SitlCommandMwrRequestCodec();
    sitlCommandMwrAnswerCodec = new SitlCommandMwrAnswerCodec();
    sitlCommandMrdRequestCodec = new SitlCommandMrdRequestCodec();
    sitlCommandMrdAnswerCodec = new SitlCommandMrdAnswerCodec();
    sitlCommandListRequestCodec = new SitlCommandListRequestCodec();
    sitlCommandListAnswerCodec = new SitlCommandListAnswerCodec();
    sitlCommandIdenRequestCodec = new SitlCommandIdenRequestCodec();
    sitlCommandIdenAnswerCodec = new SitlCommandIdenAnswerCodec();
    sitlCommandSynerRequestCodec = new SitlCommandSynerRequestCodec();
    sitlCommandSynerAnswerCodec = new SitlCommandSynerAnswerCodec();
    sitlCommandIntAnswerCodec = new SitlCommandIntAnswerCodec();
    sitlCommandMwxAnswerCodec = new SitlCommandMwxAnswerCodec();
    sitlCommandMwxRequestCodec = new SitlCommandMwxRequestCodec();
    sitlCommandMrxRequestCodec = new SitlCommandMrxRequestCodec();
    sitlCommandMrxAnswerCodec = new SitlCommandMrxAnswerCodec();
    sitlCommandMwiRequestCodec = new SitlCommandMwiRequestCodec();
    sitlCommandMwiAnswerCodec = new SitlCommandMwiAnswerCodec();
    sitlCommandMrlRequestCodec = new SitlCommandMrlRequestCodec();
    sitlCommandMrlAnswerCodec = new SitlCommandMrlAnswerCodec();
    sitlCommandMrmRequestCodec = new SitlCommandMrmRequestCodec();
    sitlCommandMrmAnswerCodec = new SitlCommandMrmAnswerCodec();
    sitlCommandClineRequestCodec = new SitlCommandClineRequestCodec();
    sitlCommandClineAnswerCodec = new SitlCommandClineAnswerCodec();
    sitlCommandClsetRequestCodec = new SitlCommandClsetRequestCodec();
    sitlCommandSeqAnswerCodec = new SitlCommandSeqAnswerCodec();
    sitlCommandSeqRequestCodec = new SitlCommandSeqRequestCodec();
    sitlCommandGlobalCodec->addCodec(sitlCommandMwrRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMwrAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrdRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrdAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandListRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandListAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandIdenRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandIdenAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandSynerRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandSynerAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandIntAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMwxAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMwxRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrxAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrxRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMwiAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMwiRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrlAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrlRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrmAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandMrmRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandClineAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandClineRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandClsetRequestCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandSeqAnswerCodec);
    sitlCommandGlobalCodec->addCodec(sitlCommandSeqRequestCodec);
    SitoipCommandFamilyCodec * sitoipCodec = new SitoipCommandFamilyCodec();
    sitoipCodec->addCodec(sitlCommandGlobalCodec);
    sitoIpCommandBuilder->registerCodec(sitoipCodec);

    setupSitlTab();
    setupSeparateSitlTab();
    setupArpTableTab();

    consoleEngine = new ConsoleEngine;
    QObject::connect(console, SIGNAL(onCommand(QString)), consoleEngine, SLOT(inputData(QString)));
    QObject::connect(commandConsole, SIGNAL(onCommand(QString)), consoleEngine, SLOT(inputData(QString)));
    QObject::connect(consoleEngine, SIGNAL(output(QString)), console, SLOT(addExternalResult(QString)));
    QObject::connect(consoleEngine, SIGNAL(output(QString)), resultConsole, SLOT(addExternalResult(QString)));

    settingsSocketChanged = false;
    settingsMacChanged = true;
    settingsNetmaskChanged = true;
    settingsGathewayChanged = false;

    SettingsCommandSetSocketRequestCodec * settingsCodec1 = new SettingsCommandSetSocketRequestCodec();
    SettingsCommandSetSocketAnswerCodec * settingsCodec2 = new SettingsCommandSetSocketAnswerCodec();
    SettingsCommandSetMacRequestCodec * settingsCodec3 = new SettingsCommandSetMacRequestCodec();
    SettingsCommandSetMacAnswerCodec * settingsCodec4 = new SettingsCommandSetMacAnswerCodec();
    SettingsCommandSetNetmaskRequestCodec * settingsCodec5 = new SettingsCommandSetNetmaskRequestCodec();
    SettingsCommandSetNetmaskAnswerCodec * settingsCodec6 = new SettingsCommandSetNetmaskAnswerCodec();
    SettingsCommandSetGatewayIpRequestCodec * settingsCodec7 = new SettingsCommandSetGatewayIpRequestCodec();
    SettingsCommandSetGatewayIpAnswerCodec * settingsCodec8 = new SettingsCommandSetGatewayIpAnswerCodec();
    SettingsCommandGetAllRequestCodec * settingsCodec9 = new SettingsCommandGetAllRequestCodec();
    SettingsCommandGetAllAnswerCodec * settingsCodec10 = new SettingsCommandGetAllAnswerCodec();
    settingsCommandBuilder->registerCodec(settingsCodec1);
    settingsCommandBuilder->registerCodec(settingsCodec2);
    settingsCommandBuilder->registerCodec(settingsCodec3);
    settingsCommandBuilder->registerCodec(settingsCodec4);
    settingsCommandBuilder->registerCodec(settingsCodec5);
    settingsCommandBuilder->registerCodec(settingsCodec6);
    settingsCommandBuilder->registerCodec(settingsCodec7);
    settingsCommandBuilder->registerCodec(settingsCodec8);
    settingsCommandBuilder->registerCodec(settingsCodec9);
    settingsCommandBuilder->registerCodec(settingsCodec10);
    udpConnection = new UdpDeviceConnection(getDeviceIpAddress(), getDeviceIpPort(), 4000);
    connect(this, SIGNAL(changeDeviceConnectionState(bool)), SLOT(changeDeviceConnectionStateSlot(bool)));
    //connect(console, SIGNAL(onCommand(QString)), SLOT(sendSitlCommandSlot(QString)));
    //connect(commandConsole, SIGNAL(onCommand(QString)), SLOT(sendSitlCommandSlot(QString)));
    connect(this, SIGNAL(sendData(QByteArray)), SLOT(sendDataSlot(QByteArray)));

    QObject::connect(udpConnection, SIGNAL(dataSended(QByteArray)), SLOT(sendedDataSlot(QByteArray)));


    sitoipConsoleProc = new SitoipConsoleProc();
    sitoipConsoleProc->addAssembler(sitlAssembler);
    sitoipConsoleProc->addCodec(sitoipCodec);
    sitoipConsoleProc->setEndian(QDataStream::LittleEndian);
    QString baseSitoipCommand = "SITOIP";
    consoleEngine->addProc(baseSitoipCommand, sitoipConsoleProc);
    helpConsoleProc = new HelpConsoleProc();
    consoleEngine->addProc("help", helpConsoleProc);
    //helpConsoleProc->addHelpTo("HELP", "usage: help command");
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MRD");
    helpConsoleProc->addHelpTo("MRD", sitlCommandAssemblerMRD->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "RD");
    helpConsoleProc->addHelpTo("RD", sitlCommandAssemblerMRD->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MEMRD");
    helpConsoleProc->addHelpTo("MEMRD", sitlCommandAssemblerMRD->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MWR");
    helpConsoleProc->addHelpTo("MWR", sitlCommandAssemblerMWR->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "WR");
    helpConsoleProc->addHelpTo("WR", sitlCommandAssemblerMWR->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MEMWR");
    helpConsoleProc->addHelpTo("MEMWR", sitlCommandAssemblerMWR->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "LIST");
    helpConsoleProc->addHelpTo("LIST", sitlCommandAssemblerLIST->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "IDEN");
    helpConsoleProc->addHelpTo("IDEN", sitlCommandAssemblerIDEN->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "ID");
    helpConsoleProc->addHelpTo("ID", sitlCommandAssemblerIDEN->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MWX");
    helpConsoleProc->addHelpTo("MWX", sitlCommandAssemblerMWX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "WRX");
    helpConsoleProc->addHelpTo("WRX", sitlCommandAssemblerMWX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MEMWX");
    helpConsoleProc->addHelpTo("MEMWX", sitlCommandAssemblerMWX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MRX");
    helpConsoleProc->addHelpTo("MRX", sitlCommandAssemblerMRX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "RDX");
    helpConsoleProc->addHelpTo("RDX", sitlCommandAssemblerMRX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MEMRX");
    helpConsoleProc->addHelpTo("MEMRX", sitlCommandAssemblerMRX->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MWI");
    helpConsoleProc->addHelpTo("MWI", sitlCommandAssemblerMWI->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MEMWI");
    helpConsoleProc->addHelpTo("MEMWI", sitlCommandAssemblerMWI->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "POSTW");
    helpConsoleProc->addHelpTo("POSTW", sitlCommandAssemblerMWI->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PMW");
    helpConsoleProc->addHelpTo("PMW", sitlCommandAssemblerMWI->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PMWR");
    helpConsoleProc->addHelpTo("PMWR", sitlCommandAssemblerMWI->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MRL");
    helpConsoleProc->addHelpTo("MRL", sitlCommandAssemblerMRL->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PMRL");
    helpConsoleProc->addHelpTo("PMRL", sitlCommandAssemblerMRL->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PREFR");
    helpConsoleProc->addHelpTo("PREFR", sitlCommandAssemblerMRL->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "MRM");
    helpConsoleProc->addHelpTo("MRM", sitlCommandAssemblerMRM->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PMRM");
    helpConsoleProc->addHelpTo("PMRM", sitlCommandAssemblerMRM->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "PRMUL");
    helpConsoleProc->addHelpTo("PRMUL", sitlCommandAssemblerMRM->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "CLINE");
    helpConsoleProc->addHelpTo("CLINE", sitlCommandAssemblerCLINE->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "LSIZE");
    helpConsoleProc->addHelpTo("LSIZE", sitlCommandAssemblerCLINE->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "CHLNS");
    helpConsoleProc->addHelpTo("CHLNS", sitlCommandAssemblerCLINE->manual());
    consoleEngine->addAliasToCommand(baseSitoipCommand, "CLSET");
    helpConsoleProc->addHelpTo("CLSET", sitlCommandAssemblerCLSET->manual());

    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), sitoipConsoleProc, SLOT(sitoipRawCommandInpup(QByteArray)));
    QObject::connect(sitoipConsoleProc, SIGNAL(sitoipRawCommandOutput(QByteArray)), udpConnection, SLOT(sendData(QByteArray)));

    sitlRamAccessor = new SitlRamAccessor();
    QObject::connect(sitoipConsoleProc, SIGNAL(receivedSitlAnswerList(QList<QSharedPointer<CommandParams> >)), sitlRamAccessor, SLOT(receiveSitlAnswerList(QList<QSharedPointer<CommandParams> >)));
    QObject::connect(sitlRamAccessor, SIGNAL(sendSitlRequestList(QList<QSharedPointer<CommandParams> >)), sitoipConsoleProc, SLOT(sendSitlRequestList(QList<QSharedPointer<CommandParams> >)));

    setSocketAnswerFilter.addCodec(settingsCodec2);
    setSocketAnswerFilter.setEndian(QDataStream::LittleEndian);
    setSocketAnswerFilter.setNeedDecode(true);
    setSocketAnswerFilter.setTimeout(10000);
    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), &setSocketAnswerFilter, SLOT(filterData(QByteArray)));
    //QObject::connect(udpConnection, SIGNAL(disconnectNotify()), &setSocketAnswerFilter, SLOT(stopWait()));
    QObject::connect(&setSocketAnswerFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(slotSetSocketAnswer(QSharedPointer <CommandParams>)));
    QObject::connect(&setSocketAnswerFilter, SIGNAL(timeout()), SLOT(slotSetSocketTimeout()));

    setMacAnswerFilter.addCodec(settingsCodec4);
    setMacAnswerFilter.setEndian(QDataStream::LittleEndian);
    setMacAnswerFilter.setNeedDecode(true);
    setMacAnswerFilter.setTimeout(10000);
    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), &setMacAnswerFilter, SLOT(filterData(QByteArray)));
    QObject::connect(udpConnection, SIGNAL(disconnectNotify()), &setMacAnswerFilter, SLOT(stopWait()));
    QObject::connect(&setMacAnswerFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(slotSetMacAnswer(QSharedPointer <CommandParams>)));
    QObject::connect(&setMacAnswerFilter, SIGNAL(timeout()), SLOT(slotSetMacTimeout()));

    setNetmaskAnswerFilter.addCodec(settingsCodec6);
    setNetmaskAnswerFilter.setEndian(QDataStream::LittleEndian);
    setNetmaskAnswerFilter.setNeedDecode(true);
    setNetmaskAnswerFilter.setTimeout(10000);
    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), &setNetmaskAnswerFilter, SLOT(filterData(QByteArray)));
    QObject::connect(udpConnection, SIGNAL(disconnectNotify()), &setNetmaskAnswerFilter, SLOT(stopWait()));
    QObject::connect(&setNetmaskAnswerFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(slotSetNetmaskAnswer(QSharedPointer <CommandParams>)));
    QObject::connect(&setNetmaskAnswerFilter, SIGNAL(timeout()), SLOT(slotSetNetmaskTimeout()));

    setGatewayAnswerFilter.addCodec(settingsCodec8);
    setGatewayAnswerFilter.setEndian(QDataStream::LittleEndian);
    setGatewayAnswerFilter.setNeedDecode(true);
    setGatewayAnswerFilter.setTimeout(10000);
    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), &setGatewayAnswerFilter, SLOT(filterData(QByteArray)));
    QObject::connect(udpConnection, SIGNAL(disconnectNotify()), &setGatewayAnswerFilter, SLOT(stopWait()));
    QObject::connect(&setGatewayAnswerFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(slotSetGatewayAnswer(QSharedPointer <CommandParams>)));
    QObject::connect(&setGatewayAnswerFilter, SIGNAL(timeout()), SLOT(slotSetGatewayTimeout()));

    getAllAnswerFilter.addCodec(settingsCodec10);
    getAllAnswerFilter.setEndian(QDataStream::LittleEndian);
    getAllAnswerFilter.setNeedDecode(true);
    getAllAnswerFilter.setTimeout(10000);
    QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), &getAllAnswerFilter, SLOT(filterData(QByteArray)));
    QObject::connect(udpConnection, SIGNAL(disconnectNotify()), &getAllAnswerFilter, SLOT(stopWait()));
    //QObject::connect(udpConnection, SIGNAL(connectNotify()), &getAllAnswerFilter, SLOT(startWait()));
    QObject::connect(&getAllAnswerFilter, SIGNAL(decodedParams(QSharedPointer <CommandParams>)), SLOT(slotGetAllAnswer(QSharedPointer <CommandParams>)));
    QObject::connect(&getAllAnswerFilter, SIGNAL(timeout()), SLOT(slotGetAllTimeout()));


    QObject::connect(&icmpRequestSequence, SIGNAL(signalPingStatusSequence(PingStatus,uint,uint)), SLOT(slotGetIcmpAnswer(PingStatus,uint, uint)));
    QObject::connect(&icmpRequestSequence, SIGNAL(signalPingStatistics(uint,uint,uint,uint,uint,uint)), SLOT(slotGetIcmpStatistics(uint,uint,uint,uint,uint,uint)));
    pingButtonStarted = false;

    sitoipCheck = new SitoipCheckConnection();

    QPalette connectionStateLabelPalette;
    ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
    connectionStateLabelPalette.setColor(ui->connectionStateLabel->backgroundRole(),
                                        palette().color(QPalette::Background));
    ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
    setVisibleInfoAboutConnection(false);

    settingsForm = new SettingsForm(settings, this);
    QObject::connect(settingsForm, SIGNAL(settingsUpdated(SitlClientSettingsClass)), SLOT(slotSettingsUpdated(SitlClientSettingsClass)));
    settings.loadSettingsFromFile(SETTINGS_FILE);
}

void MainWindow::setupSitlTab()
{
    console = new Console(this);

    QFont font;
    font.setFamily("Lucida Console");
    font.setPointSize(10);
    console->setFont(font);

    QVBoxLayout * vPanel = new QVBoxLayout;
    vPanel->addWidget(console);
    vPanel->setMargin(0);
    vPanel->setSpacing(0);

    QPushButton * clearSitlConsoleButton =  new QPushButton(trUtf8("Очистить"));
    QPushButton * openSitFileButton =  new QPushButton(trUtf8("Открыть поток комманд"));
    QPushButton * saveSitlLogButton = new QPushButton(trUtf8("Сохранить журнал SITL"));
    //QPushButton * saveSitFileButton =  new QPushButton(trUtf8("Сохранить поток комманд"));
    //QPushButton * saveSrlFileButton =  new QPushButton(trUtf8("Сохранить результаты"));

    QHBoxLayout * hPanel = new QHBoxLayout;
    hPanel->addWidget(clearSitlConsoleButton);
    hPanel->addWidget(openSitFileButton);
    hPanel->addWidget(saveSitlLogButton);
    //hPanel->addWidget(saveSitFileButton);
    //hPanel->addWidget(saveSrlFileButton);

    ui->sitlTab->setLayout(vPanel);
    vPanel->addLayout(hPanel);

    QObject::connect(openSitFileButton, SIGNAL(clicked()), SLOT(slotOpenSitFile()));
    //QObject::connect(saveSitFileButton, SIGNAL(clicked()), SLOT(slotSaveSitFile()));
    //QObject::connect(saveSrlFileButton, SIGNAL(clicked()), SLOT(slotSaveSrlFile()));
    QObject::connect(clearSitlConsoleButton, SIGNAL(clicked()), console, SLOT(clear()));
    QObject::connect(saveSitlLogButton, SIGNAL(clicked()), SLOT(slotSaveSitlLog()));
    //console->setFocus();
}

void MainWindow::setupSeparateSitlTab()
{
    resultConsole = new Console(this, "");
    resultConsole->setReadOnly(true);
    commandConsole = new Console(this);

    QFont font;
    font.setFamily("Lucida Console");
    font.setPointSize(10);
    resultConsole->setFont(font);
    commandConsole->setFont(font);

    QVBoxLayout * mainPanel = new QVBoxLayout;
    QVBoxLayout * resultPanel = new QVBoxLayout;
    QVBoxLayout * commandPanel = new QVBoxLayout;

    QLabel * resultLabel =  new QLabel;
    resultLabel->setText(trUtf8("Поток результатов"));
    resultPanel->addWidget(resultLabel);
    resultPanel->addWidget(resultConsole);
    QHBoxLayout * resultButtonPanel = new QHBoxLayout;
    QPushButton * clearResultButton = new QPushButton;
    clearResultButton->setText(trUtf8("Очистить"));
    QPushButton * saveResultsButton = new QPushButton;
    saveResultsButton->setText(trUtf8("Сохранить"));
    resultButtonPanel->addWidget(clearResultButton);
    resultButtonPanel->addWidget(saveResultsButton);
    resultPanel->addLayout(resultButtonPanel);
    QObject::connect(clearResultButton, SIGNAL(clicked()), resultConsole, SLOT(clear()));
    QObject::connect(saveResultsButton, SIGNAL(clicked()), SLOT(slotSaveSrlFile()));

    QLabel * commandLabel =  new QLabel;
    commandLabel->setText(trUtf8("Поток команд"));
    commandPanel->addWidget(commandLabel);
    commandPanel->addWidget(commandConsole);
    QHBoxLayout * commandButtonPanel = new QHBoxLayout;
    QPushButton * clearCommandButton = new QPushButton;
    clearCommandButton->setText(trUtf8("Очистить"));
    QPushButton * openCommandButton = new QPushButton;
    openCommandButton->setText(trUtf8("Открыть"));
    QPushButton * saveCommandButton = new QPushButton;
    saveCommandButton->setText(trUtf8("Сохранить"));
    commandButtonPanel->addWidget(clearCommandButton);
    commandButtonPanel->addWidget(openCommandButton);
    commandButtonPanel->addWidget(saveCommandButton);
    commandPanel->addLayout(commandButtonPanel);
    QObject::connect(clearCommandButton, SIGNAL(clicked()), commandConsole, SLOT(clear()));
    QObject::connect(openCommandButton, SIGNAL(clicked()), SLOT(slotOpenSitFile()));
    QObject::connect(saveCommandButton, SIGNAL(clicked()), SLOT(slotSaveSeparateSitFile()));
    QObject::connect(console, SIGNAL(onCommand(QString)), commandConsole, SLOT(addExternalCommand(QString)));
    QObject::connect(commandConsole, SIGNAL(onCommand(QString)), console, SLOT(addExternalCommand(QString)));

    mainPanel->addLayout(resultPanel);
    mainPanel->addLayout(commandPanel);
    mainPanel->setMargin(0);
    mainPanel->setSpacing(0);
    ui->separateSitlTab->setLayout(mainPanel);
}

void MainWindow::setupArpTableTab()
{
    arpModel = new QStandardItemModel(0, 3, this);
    arpModel->setHorizontalHeaderItem(0, new QStandardItem(QString("IP")));
    arpModel->setHorizontalHeaderItem(1, new QStandardItem(QString("MAC")));
    arpModel->setHorizontalHeaderItem(2, new QStandardItem(QString("TTL")));
    //arpModel->setHorizontalHeaderItem(3, new QStandardItem(QString("Flag")));
    ui->arpTableView->setModel(arpModel);
    clearArpTable();
    return;
}

void MainWindow::addLogEvent(QString event)
{
    ui->logTextEdit->append(QTime::currentTime().toString("hh:mm:ss.zzz ") + event);
}

void MainWindow::setDeviceSocket(QHostAddress ipAddress, quint16 ipPort)
{
    ui->deviceIpAddressLineEdit->setText(ipAddress.toString());
    ui->deviceIpPortSpinBox->setValue(ipPort);
    udpConnection->setSocket(ipAddress, ipPort);
}

void MainWindow::sendDataSlot(QByteArray rawData)
{
    udpConnection->setSocket(getDeviceIpAddress(), getDeviceIpPort());
    udpConnection->sendData(rawData);
}

void MainWindow::sendedDataSlot(QByteArray rawData)
{
    if(settings.needShowNetworkDataInLog)
    {
        addLogEvent(trUtf8("Отправка данных: ") + rawData.toHex());
    }
}

void MainWindow::sendSitlCommandSlot(QString sitlString)
{
    QStringList sitlArguments = sitlString.toUpper().split(" ", QString::SkipEmptyParts);
    QList <SitlCommandFamilyParams> paramsList = sitlAssembler->assemble(sitlArguments);
    SitoipCommandFamilyParams * sitoipParams = new SitoipCommandFamilyParams;
    sitoipParams->commandType = CT_REQUEST;
    sitoipParams->commandId = COMMAND_ID_SITOIP_REQUEST;
    for(int i = 0; i < paramsList.size(); ++i)
    {
        SitlCommandFamilyParams * sitlParams;
        *sitlParams = paramsList.at(i);
        QSharedPointer <CommandParams> params(sitlParams);
        sitoipParams->sitlCommandsList.append(params);
    }
    CodecError codecError;
    QByteArray sitoipRawData = sitoIpCommandBuilder->encode(QSharedPointer <CommandParams>(sitoipParams), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        console->output(trUtf8("SYNER"));
        //console->unblock();
        return;
    }
    //label!//emit sendData(sitoipRawData);
    connect(&sitlTimeoutTimer, SIGNAL(timeout()), this, SLOT(sitlAnswerTimeout()));
    sitlTimeoutTimer.setSingleShot(true);
    sitlTimeoutTimer.start(4000);
}

void MainWindow::sitlAnswerTimeout()
{
    disconnect(&sitlTimeoutTimer, SIGNAL(timeout()), this, SLOT(sitlAnswerTimeout()));
    //console->output("TOUT");
    //console->unblock();
}


void MainWindow::receiveDataSlot(QByteArray rawData)
{
    if(settings.needShowNetworkDataInLog)
    {
        addLogEvent(trUtf8("Прием данных:    ") + rawData.toHex());
    }
}

void MainWindow::receiveSitoIpCommand(QByteArray rawData)
{
    CodecError codecError;
    QSharedPointer <CommandParams> params = sitoIpCommandBuilder->decode(rawData, QDataStream::LittleEndian, &codecError);
    SitoipCommandFamilyParams * sitoipParams = (SitoipCommandFamilyParams *)params.data();
    if(codecError.errType == CE_CODEC_NOT_FOUND)
    {
        return;
    }
    if(codecError.errType != CE_NO_ERROR)
    {
        console->output(trUtf8("SYNER: неправильный формат принятых данных"));
        disconnect(&sitlTimeoutTimer, SIGNAL(timeout()), this, SLOT(sitlAnswerTimeout()));
        //console->unblock();
        sitlTimeoutTimer.stop();
        return;
    }
    else
    {
        if(sitoipParams->commandId == COMMAND_ID_SITOIP_EMPTY)
        {
            return;
        }
        for(int i = 0; i < sitoipParams->sitlCommandsList.size(); ++i)
        {
            QString disassembleString = sitlAssembler->disassemble(*(SitlCommandFamilyParams*)sitoipParams->sitlCommandsList.at(i).data());
            console->output(disassembleString);
            resultConsole->output(disassembleString);
        }
        //console->unblock();
        sitlTimeoutTimer.stop();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

QHostAddress MainWindow::getDeviceIpAddress()
{
    return QHostAddress(ui->deviceIpAddressLineEdit->text());
}

quint16 MainWindow::getDeviceIpPort()
{
    return ui->deviceIpPortSpinBox->value();
}

void MainWindow::changeDeviceConnectionStateSlot(bool isConnected)
{
    if(!isConnected)
    {
        ui->connectionButton->setText(trUtf8("Подключиться"));
        addLogEvent(trUtf8("Отключен от ") + getDeviceIpAddress().toString() + ":"+ QString::number(getDeviceIpPort()));
        setVisibleInfoAboutConnection(false);
    }
    else
    {
        ui->connectionButton->setText(trUtf8("Отключиться"));
        addLogEvent(trUtf8("Подключен к ") + getDeviceIpAddress().toString() + ":"+ QString::number(getDeviceIpPort()));
        updateConnectionStateStatSlot(CSS_EMPTY);
        setVisibleInfoAboutConnection(true);
        clearDeviceSettingsTab();
        clearArpTable();
    }
    isDeviceConnected = isConnected;
    ui->sitlTab->setEnabled(isConnected);
    ui->separateSitlTab->setEnabled(isConnected);
    ui->deviceSettingsTab->setEnabled(isConnected);
    ui->arpTableTab->setEnabled(isConnected);
    ui->action_9->setEnabled(isConnected);
    ui->deviceIpAddressLineEdit->setEnabled(!isConnected);
    ui->deviceIpPortSpinBox->setEnabled(!isConnected);
    if(isConnected)
    {
        currentSettings = settings;
        sitoipConsoleProc->setMaxSitlCommandsInSitoipPacket(currentSettings.maxSitlWordsInSitoipPacket);
        sitlCommandIntAnswerCodec->setStatusIdSize(currentSettings.sitlIntStatusSize);
        if(currentSettings.checkConnection)
        {
            sitoipCheck->setCheckConnectionParams(currentSettings.checkTimeout, currentSettings.checkInterval, currentSettings.checkRequests);
            QObject::connect(udpConnection, SIGNAL(connectNotify()), sitoipCheck, SLOT(sendSitoipEmptyRequestSequence()));
            QObject::connect(udpConnection, SIGNAL(disconnectNotify()), sitoipCheck, SLOT(stopSitoipEmptyRequestSequence()));
            QObject::connect(sitoipCheck, SIGNAL(sendData(QByteArray)), udpConnection, SLOT(sendData(QByteArray)));
            QObject::connect(udpConnection, SIGNAL(dataReceived(QByteArray)), sitoipCheck, SIGNAL(checkData(QByteArray)));
            QObject::connect(sitoipCheck, SIGNAL(signalSitoipConnectionStatistics(uint,uint,uint,uint,uint,uint)), SLOT(slotGetSitoipConnectionStatistics(uint,uint,uint,uint,uint,uint)));
        }
        udpConnection->setSocket(getDeviceIpAddress(), getDeviceIpPort());
        udpConnection->connectDevice();
        connect(udpConnection, SIGNAL(dataReceived(QByteArray)), SLOT(receiveDataSlot(QByteArray)));
        //connect(udpConnection, SIGNAL(dataReceived(QByteArray)), SLOT(receiveSitoIpCommand(QByteArray)));
        if(currentSettings.getDeviceSettingsOnConnect)
        {
            getAllDeviseSettings();
        }
        bool arpSuccessFlag;
        QFuture<QByteArray> future = QtConcurrent::run(getMacAddrFromIP, getDeviceIpAddress(), &arpSuccessFlag);
        future.waitForFinished();
        //QByteArray gatewayMac = getMacAddrFromIP(getDeviceIpAddress(), &arpSuccessFlag);
        MacAddress gatewayMac(future.result());
        if(arpSuccessFlag)
        {
            ui->deviceMacAddress->setText(gatewayMac.toString());
        }
        else
        {
            ui->deviceMacAddress->setText(trUtf8("Не удалось получить"));
        }
    }
    else
    {
        udpConnection->disconnectDevice();
        if(currentSettings.checkConnection)
        {
            if(currentSettings.checkConnection)
            {
                QObject::disconnect(udpConnection, SIGNAL(connectNotify()), sitoipCheck, SLOT(sendSitoipEmptyRequestSequence()));
                QObject::disconnect(udpConnection, SIGNAL(disconnectNotify()), sitoipCheck, SLOT(stopSitoipEmptyRequestSequence()));
                QObject::disconnect(sitoipCheck, SIGNAL(sendData(QByteArray)), udpConnection, SLOT(sendData(QByteArray)));
                QObject::disconnect(udpConnection, SIGNAL(dataReceived(QByteArray)), sitoipCheck, SIGNAL(checkData(QByteArray)));
                QObject::disconnect(sitoipCheck, SIGNAL(signalSitoipConnectionStatistics(uint,uint,uint,uint,uint,uint)), this, SLOT(slotGetSitoipConnectionStatistics(uint,uint,uint,uint,uint,uint)));
            }
        }
        disconnect(udpConnection, SIGNAL(dataReceived(QByteArray)), this, SLOT(receiveDataSlot(QByteArray)));
        //disconnect(udpConnection, SIGNAL(dataReceived(QByteArray)), this, SLOT(receiveSitoIpCommand(QByteArray)));
        ui->deviceMacAddress->setText(trUtf8("Не подключено"));
    }
}

void MainWindow::on_connectionButton_clicked()
{
    int pos = 0;
    QString checkString = ui->deviceIpAddressLineEdit->text();
    if(ui->deviceIpAddressLineEdit->validator()->validate(checkString, pos) == QValidator::Acceptable)
    {
        emit changeDeviceConnectionState(!isDeviceConnected);
    }
    else
    {
        QMessageBox::information(NULL,QObject::trUtf8("Неверные данные"),trUtf8("Неверный формат ip-адреса"));
    }
}

void MainWindow::on_setDeviceSettingsButton_clicked()
{
    ui->settingsDeviceIpAddressLineEdit->setText("192.168.23.120");
    ui->settingsIpPortSpinBox->setValue(58500);
    ui->settingsMacAddressLineEdit->setText("00:60:8C:61:EA:2A");
    ui->settingsGatewayLineEdit->setText("192.168.23.1");
    ui->settingsNetMaskLineEdit->setText("255.255.255.0");
}

void MainWindow::on_setDeviceSettingsPermanentButton_clicked()
{
    CodecError codecError;
    if(settingsGathewayChanged)
    {
        SettingsCommandFamilyParams * settingsParamsSetGatheway = new SettingsCommandFamilyParams;
        settingsParamsSetGatheway->commandId = COMMAND_ID_SETTINGS_SET_GATEWAY_IP;
        settingsParamsSetGatheway->commandType = CT_REQUEST;
        settingsParamsSetGatheway->gatewayIpAddress.setAddress(ui->settingsGatewayLineEdit->text());
        QByteArray settingsDataSetGatheway = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetGatheway), QDataStream::LittleEndian, &codecError);
        if(codecError.errType != CE_NO_ERROR)
        {
            return;
        }
        emit sendData(settingsDataSetGatheway);
    }
    if(settingsNetmaskChanged)
    {
        SettingsCommandFamilyParams * settingsParamsSetNetmask = new SettingsCommandFamilyParams;
        settingsParamsSetNetmask->commandId = COMMAND_ID_SETTINGS_SET_NETMASK;
        settingsParamsSetNetmask->commandType = CT_REQUEST;
        settingsParamsSetNetmask->netmask.setAddress(ui->settingsNetMaskLineEdit->text());
        QByteArray settingsDataSetNetmask = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetNetmask), QDataStream::LittleEndian, &codecError);
        if(codecError.errType != CE_NO_ERROR)
        {
            return;
        }
        emit sendData(settingsDataSetNetmask);
    }
    if(settingsSocketChanged)
    {
        SettingsCommandFamilyParams * settingsParamsSetSocket = new SettingsCommandFamilyParams;
        settingsParamsSetSocket->commandId = COMMAND_ID_SETTINGS_SET_SOCKET;
        settingsParamsSetSocket->commandType = CT_REQUEST;
        settingsParamsSetSocket->deviceIpAddress.setAddress(ui->settingsDeviceIpAddressLineEdit->text());
        settingsParamsSetSocket->deviceIpPort = ui->settingsIpPortSpinBox->value();
        QByteArray settingsDataSetSocket = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetSocket), QDataStream::LittleEndian, &codecError);
        if(codecError.errType != CE_NO_ERROR)
        {
            return;
        }
        emit sendData(settingsDataSetSocket);
    }

}

void MainWindow::on_setSocketPushButton_clicked()
{
    QHostAddress newIpAddress;
    newIpAddress.setAddress(ui->settingsDeviceIpAddressLineEdit->text());
    quint16 newIpPort = ui->settingsIpPortSpinBox->value();
    SettingsCommandFamilyParams * settingsParamsSetSocket = new SettingsCommandFamilyParams;
    settingsParamsSetSocket->commandId = COMMAND_ID_SETTINGS_SET_SOCKET;
    settingsParamsSetSocket->commandType = CT_REQUEST;
    settingsParamsSetSocket->deviceIpAddress = newIpAddress;
    settingsParamsSetSocket->deviceIpPort = newIpPort;
    CodecError codecError;
    QByteArray settingsDataSetSocket = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetSocket), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        addLogEvent("Ошибка кодирования пакета");
        return;
    }
    addLogEvent(trUtf8("Изменение сокета: ") + newIpAddress.toString() + ":" + QString::number(newIpPort));
    ui->deviceSettingsTab->setEnabled(false);
    //setSocketAnswerFilter.startWaitDataOnce(10000);
    setSocketAnswerFilter.startWait();
    emit sendData(settingsDataSetSocket);
}

void MainWindow::slotSetSocketAnswer(QSharedPointer <CommandParams> params)
{
    SettingsCommandFamilyParams * settingParams = (SettingsCommandFamilyParams *) params.data();
    if(settingParams->commandStatus == STATUS_SUCCESS)
    {
        addLogEvent(trUtf8("Сокет успешно изменен"));
        udpConnection->disconnectDevice();
        setDeviceSocket(QHostAddress(ui->settingsDeviceIpAddressLineEdit->text()), (quint16)ui->settingsIpPortSpinBox->value());
        udpConnection->connectDevice();
    }
    else if(settingParams->commandStatus == STATUS_ERROR)
    {
        addLogEvent(trUtf8("Ошибка изменения сокета"));
        QMessageBox::information(NULL,QObject::trUtf8("Изменение сокета"),trUtf8("Ошибка изменения сокета"));
    }
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotSetSocketTimeout()
{
    addLogEvent(trUtf8("Не удалось изменить сокет: таймаут ответа"));
    QMessageBox::information(NULL,QObject::trUtf8("Изменение сокета"),trUtf8("Не удалось изменить сокет: таймаут ответа"));
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::on_setMacPuchButton_clicked()
{
    SettingsCommandFamilyParams * settingsParamsSetMac = new SettingsCommandFamilyParams;
    settingsParamsSetMac->commandId = COMMAND_ID_SETTINGS_SET_MAC;
    settingsParamsSetMac->commandType = CT_REQUEST;
    MacAddress deviceMac(ui->settingsMacAddressLineEdit->text());
    settingsParamsSetMac->deviceMacAddress = deviceMac.getMacByteArray();
    CodecError codecError;
    QByteArray settingsDataSetMac = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetMac), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        addLogEvent("Ошибка кодирования пакета");
        return;
    }
    addLogEvent(trUtf8("Изменение MAC адреса: ")  + ui->settingsMacAddressLineEdit->text());
    ui->deviceSettingsTab->setEnabled(false);
    setMacAnswerFilter.startWait();
    emit sendData(settingsDataSetMac);
}

void MainWindow::slotSetMacAnswer(QSharedPointer <CommandParams> params)
{
    SettingsCommandFamilyParams * settingParams = (SettingsCommandFamilyParams *) params.data();
    if(settingParams->commandStatus == STATUS_SUCCESS)
    {
        addLogEvent(trUtf8("MAC-адрес успешно изменен"));
    }
    else if(settingParams->commandStatus == STATUS_ERROR)
    {
        addLogEvent(trUtf8("Ошибка изменения MAC-адреса"));
        QMessageBox::information(NULL,QObject::trUtf8("Изменение MAC-адреса"),trUtf8("Ошибка изменения MAC-адреса"));
    }
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotSetMacTimeout()
{
    addLogEvent(trUtf8("Не удалось изменить MAC-адрес: таймаут ответа"));
    QMessageBox::information(NULL,QObject::trUtf8("Изменение MAC-адреса"),trUtf8("Не удалось изменить MAC-адрес: таймаут ответа"));
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::on_setMaskPushButton_clicked()
{
    QHostAddress newMask;
    newMask.setAddress(ui->settingsNetMaskLineEdit->text());
    SettingsCommandFamilyParams * settingsParamsSetMask = new SettingsCommandFamilyParams;
    settingsParamsSetMask->commandId = COMMAND_ID_SETTINGS_SET_NETMASK;
    settingsParamsSetMask->commandType = CT_REQUEST;
    settingsParamsSetMask->netmask = newMask;
    CodecError codecError;
    QByteArray settingsDataSetMask = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetMask), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        addLogEvent("Ошибка кодирования пакета");
        return;
    }
    addLogEvent(trUtf8("Изменение маски подсети: ") + newMask.toString());
    ui->deviceSettingsTab->setEnabled(false);
    setNetmaskAnswerFilter.startWait();
    emit sendData(settingsDataSetMask);
}

void MainWindow::slotSetNetmaskAnswer(QSharedPointer <CommandParams> params)
{
    SettingsCommandFamilyParams * settingParams = (SettingsCommandFamilyParams *) params.data();
    if(settingParams->commandStatus == STATUS_SUCCESS)
    {
        addLogEvent(trUtf8("Маска подсети успешно изменена"));
    }
    else if(settingParams->commandStatus == STATUS_ERROR)
    {
        addLogEvent(trUtf8("Ошибка изменения маски подсети"));
        QMessageBox::information(NULL,QObject::trUtf8("Изменение маски подсети"),trUtf8("Ошибка изменения маски подсети"));
    }
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotSetNetmaskTimeout()
{
    addLogEvent(trUtf8("Не удалось изменить маску подсети: таймаут ответа"));
    QMessageBox::information(NULL,QObject::trUtf8("Изменение маски подсети"),trUtf8("Не удалось изменить маску подсети: таймаут ответа"));
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::on_setGathewayPushButton_clicked()
{
    QHostAddress newGateway;
    newGateway.setAddress(ui->settingsGatewayLineEdit->text());
    SettingsCommandFamilyParams * settingsParamsSetGateway = new SettingsCommandFamilyParams;
    settingsParamsSetGateway->commandId = COMMAND_ID_SETTINGS_SET_GATEWAY_IP;
    settingsParamsSetGateway->commandType = CT_REQUEST;
    settingsParamsSetGateway->gatewayIpAddress = newGateway;
    CodecError codecError;
    QByteArray settingsDataSetGateway = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsSetGateway), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        addLogEvent("Ошибка кодирования пакета");
        return;
    }
    addLogEvent(trUtf8("Изменение шлюза: ") + newGateway.toString());
    ui->deviceSettingsTab->setEnabled(false);
    setGatewayAnswerFilter.startWait();
    emit sendData(settingsDataSetGateway);
}

void MainWindow::slotSetGatewayAnswer(QSharedPointer <CommandParams> params)
{
    SettingsCommandFamilyParams * settingParams = (SettingsCommandFamilyParams *) params.data();
    if(settingParams->commandStatus == STATUS_SUCCESS)
    {
        addLogEvent(trUtf8("Шлюз успешно изменен"));
    }
    else if(settingParams->commandStatus == STATUS_ERROR)
    {
        addLogEvent(trUtf8("Ошибка изменения шлюза"));
        QMessageBox::information(NULL,QObject::trUtf8("Изменение шлюза"),trUtf8("Ошибка изменения шлюза"));
    }
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotSetGatewayTimeout()
{
    addLogEvent(trUtf8("Не удалось изменить шлюз: таймаут ответа"));
    QMessageBox::information(NULL,QObject::trUtf8("Изменение шлюза"),trUtf8("Не удалось изменить шлюз: таймаут ответа"));
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::on_getDeviceSettingsButton_clicked()
{
    getAllDeviseSettings();
}

void MainWindow::slotGetAllAnswer(QSharedPointer <CommandParams> params)
{
    SettingsCommandFamilyParams * settingParams = (SettingsCommandFamilyParams *) params.data();
    if(settingParams->commandStatus == STATUS_SUCCESS)
    {
        addLogEvent(trUtf8("Настройки устройства получены"));
        ui->settingsDeviceIpAddressLineEdit->setText(settingParams->deviceIpAddress.toString());
        ui->settingsIpPortSpinBox->setValue(settingParams->deviceIpPort);
        MacAddress newMacAddress(settingParams->deviceMacAddress);
        ui->settingsMacAddressLineEdit->setText(newMacAddress.toString());
        ui->settingsNetMaskLineEdit->setText(settingParams->netmask.toString());
        ui->settingsGatewayLineEdit->setText(settingParams->gatewayIpAddress.toString());
        MacAddress gatewayMacAddress(settingParams->gatewayMacAddress);
        ui->settingsGatewayMacValueLabel->setText(gatewayMacAddress.toString());
    }
    else if(settingParams->commandStatus == STATUS_ERROR)
    {
        addLogEvent(trUtf8("Ошибка получения настроек устройства"));
        QMessageBox::information(NULL,QObject::trUtf8("Получение настроек устройства"),trUtf8("Ошибка получуния настроек устройства"));
    }
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotGetAllTimeout()
{
    addLogEvent(trUtf8("Не удалось получить настройки устройства: таймаут ответа"));
    QMessageBox::information(NULL,QObject::trUtf8("Получение настроек устройства"),trUtf8("Не удалось получить настройки устройства: таймаут ответа"));
    ui->deviceSettingsTab->setEnabled(true);
}

void MainWindow::slotGetIcmpAnswer(PingStatus pingStatus, uint realTimeout, uint numberInSequence)
{
    QString logString = trUtf8("Пинг запрос #") + QString("%1").arg(numberInSequence, 2, 10, QChar('0')) + " ";
    switch(pingStatus)
    {
        case PING_SUCCESS:
            logString += trUtf8("успешно за ") + QString::number(realTimeout) + trUtf8("мс");
            break;
        case PING_TIMEOUT:
            logString += trUtf8("превышен интервал ожидания");
            break;
        case PING_ERROR:
            logString += trUtf8("сбой передачи");
            break;
    }
    addLogEvent(logString);
}

void MainWindow::slotGetIcmpStatistics(uint pingTotalRequests, uint successRequests, uint failRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec)
{
    QString logString = trUtf8("Статистика обмена пакетами: \n");
    logString += trUtf8("Пакетов: отправлено = ")
            + QString::number(pingTotalRequests)
            + trUtf8(", получено = ")
            + QString::number(successRequests)
            + trUtf8((", потеряно = "))
            + QString::number(failRequests);
    if(successRequests > 0)
    {
        logString += "\n" + trUtf8("Время приема-передачи: минимальное = ")
                + QString::number(minTimeMsec) + trUtf8("мсек")
                + trUtf8(", максимальное = ") + QString::number(maxTimeMsec) + trUtf8("мсек")
                + trUtf8(", среднее = ") + QString::number(avgTimeMsec) + trUtf8("мсек");
    }
    addLogEvent(logString);
    setPingButtonToStartState();
}

void MainWindow::slotOpenSitFile()
{
    addLogEvent(trUtf8("Пакетная команда"));

    QString sitFilename = QFileDialog::getOpenFileName(0, trUtf8("Выбирите файл sit"), "", "*.sit;; *.txt;; *.*");

    if(sitFilename == "")
    {
        return;
    }

    QFile sitFile(sitFilename);
    if(sitFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {

        QQueue <SitlCommandFamilyParams *> sitlCommandsList;
        while(!sitFile.atEnd())
        {
            QString sitlCommand = sitFile.readLine().trimmed();
            sitoipConsoleProc->setProcessMode(PM_POST_PROCESSING);
            console->addExternalCommandAndNotify(sitlCommand);
//            console->addExternalCommand(sitlCommand);
//            commandConsole->addExternalCommand(sitlCommand);
//            SitlCommandFamilyParams * params = new SitlCommandFamilyParams();
//            QStringList sitlArguments = sitlCommand.toUpper().split(" ", QString::SkipEmptyParts);
//            *params = sitlAssembler->assemble(sitlArguments);
//            sitlCommandsList.enqueue(params);
        }
        sitoipConsoleProc->process();
        sitoipConsoleProc->setProcessMode(PM_IMMEDIATE_PROCESSING);
//        while(!sitlCommandsList.isEmpty())
//        {
//            SitoipCommandFamilyParams sitoipParams;
//            sitoipParams.commandType = CT_REQUEST;
//            sitoipParams.commandId = COMMAND_ID_SITOIP_REQUEST;
//            CodecError codecError;
//            sitoipParams.sitlCommandsList.clear();
//            while(!sitlCommandsList.isEmpty() && sitoipParams.sitlCommandsList.size() != currentSettings.maxSitlWordsInSitoipPacket)
//            {
//                sitoipParams.sitlCommandsList.append(sitlCommandsList.dequeue());
//            }
//            QByteArray sitoipRawData = sitoIpCommandBuilder->encode((CommandParams *)&sitoipParams, QDataStream::LittleEndian, codecError);
//            if(codecError != CE_NO_ERROR)
//            {
//                console->output(trUtf8("SITOIP SYNER"));
//                return;
//            }
//            emit sendData(sitoipRawData);
//            connect(&sitlTimeoutTimer, SIGNAL(timeout()), this, SLOT(sitlAnswerTimeout()));
//            sitlTimeoutTimer.setSingleShot(true);
//            sitlTimeoutTimer.start(4000);
//        }
    }
    else
    {
        addLogEvent(trUtf8("Невозможно открыть файл: ") + sitFilename);
    }
}

void MainWindow::slotSaveSitFile()
{
    QString sitFilename = QFileDialog::getSaveFileName(0, trUtf8("Выберете файл для сохранения"), "", "*.sit;; *.txt;; *.*");
    if(sitFilename == "")
    {
        return;
    }
    QFile sitFile(sitFilename);
    if(sitFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream fileStream(&sitFile);
        QStringList commands = console->getCommands();
        for(int i = 0; i < commands.size(); ++i)
        {
            fileStream << commands.at(i) << "\n";
        }
        sitFile.close();
    }
    else
    {
        addLogEvent(trUtf8("Невозможно открыть файл: ") + sitFilename);
    }
}

void MainWindow::slotSaveSeparateSitFile()
{
    QString sitFilename = QFileDialog::getSaveFileName(0, trUtf8("Выберете файл для сохранения"), "", "*.sit;; *.txt;; *.*");
    if(sitFilename == "")
    {
        return;
    }
    QFile sitFile(sitFilename);
    if(sitFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream fileStream(&sitFile);
        QStringList commands = commandConsole->getCommands();
        for(int i = 0; i < commands.size(); ++i)
        {
            fileStream << commands.at(i) << "\n";
        }
        sitFile.close();
    }
    else
    {
        addLogEvent(trUtf8("Невозможно открыть файл: ") + sitFilename);
    }
}

void MainWindow::slotSaveSrlFile()
{
    QString srlFilename = QFileDialog::getSaveFileName(0, trUtf8("Выберете файл для сохранения"), "", "*.srl;; *.txt;; *.*");
    if(srlFilename == "")
    {
        return;
    }
    QFile srlFile(srlFilename);
    if(srlFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream fileStream(&srlFile);
        QStringList results = resultConsole->getResults();
        for(int i = 0; i < results.size(); ++i)
        {
            fileStream << results.at(i) << "\n";
        }
        srlFile.close();
    }
    else
    {
        addLogEvent(trUtf8("Невозможно открыть файл: ") + srlFilename);
    }
}

void MainWindow::slotSaveSeparateSrlFile()
{
    QString srlFilename = QFileDialog::getSaveFileName(0, trUtf8("Выберете файл для сохранения"), "", "*.srl;; *.txt;; *.*");
    if(srlFilename == "")
    {
        return;
    }
    QFile srlFile(srlFilename);
    if(srlFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream fileStream(&srlFile);
        QStringList results = resultConsole->getResults();
        for(int i = 0; i < results.size(); ++i)
        {
            fileStream << results.at(i) << "\n";
        }
        srlFile.close();
    }
    else
    {
        addLogEvent(trUtf8("Невозможно открыть файл: ") + srlFilename);
    }
}

void MainWindow::slotSaveSitlLog()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    trUtf8("Сохранение файла sitl журнала"),
                                                    "",
                                                    trUtf8("Файл журнала (*.log);;Все файлы (*)"));
    if(fileName.isEmpty())
    {
        return;
    }
    else
    {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            QMessageBox::information(this, trUtf8("Невозможно открыть файл"), file.errorString());
            return;
        }
        else
        {
            QTextStream logStream(&file);
            logStream << console->toPlainText();
            file.flush();
            file.close();
        }
    }
}

void MainWindow::slotSettingsUpdated(SitlClientSettingsClass newSettings)
{
    settings = newSettings;
    settings.saveSettingsToFile(SETTINGS_FILE);
}

void MainWindow::on_action_triggered()
{
    About * aboutWindow = new About(this);
    aboutWindow->show();
}

void MainWindow::setPingButtonToStartState()
{
    ui->pingButton->setText(trUtf8("Пинг-запустить"));
    pingButtonStarted = false;
}

void MainWindow::setPingButtonToStopState()
{
    ui->pingButton->setText(trUtf8("Пинг-остановить"));
    pingButtonStarted = true;
}

void MainWindow::on_pingButton_clicked()
{
    if(pingButtonStarted)
    {
        icmpRequestSequence.stopPingSequence();
    }
    else
    {
        setPingButtonToStopState();
        addLogEvent(trUtf8("Обмен пакетами с ") + getDeviceIpAddress().toString());
        icmpRequestSequence.sendPingSequence(getDeviceIpAddress(), settings.pingRequestTimeout, settings.pingRequestCount);
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->logTextEdit->clear();
}

void MainWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    trUtf8("Сохранение файла журнала"),
                                                    "",
                                                    trUtf8("Файл журнала (*.log);;Все файлы (*)"));
    if(fileName.isEmpty())
    {
        return;
    }
    else
    {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly))
        {
            QMessageBox::information(this, trUtf8("Невозможно открыть файл"), file.errorString());
            return;
        }
        else
        {
            QTextStream logStream(&file);
            logStream << ui->logTextEdit->toPlainText();
            file.flush();
            file.close();
        }
    }
}

void MainWindow::slotGetSitoipConnectionStatistics(uint sitoipTotalRequests, uint sitoipSuccessRequests, uint sitoipFailRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec)
{
    Q_UNUSED(minTimeMsec);
    Q_UNUSED(maxTimeMsec);
    Q_UNUSED(avgTimeMsec);
    if(sitoipSuccessRequests == 0)
    {
        updateConnectionStateStatSlot(CSS_NO_CONNECTION);
    }
    else if(sitoipSuccessRequests + currentSettings.checkMaxFailRequestsToBeStable  < sitoipTotalRequests)
    {
        updateConnectionStateStatSlot(CSS_UNSTABLE);
    }
    else if(sitoipSuccessRequests + currentSettings.checkMaxFailRequestsToBeStable >= sitoipTotalRequests)
    {
        updateConnectionStateStatSlot(CSS_STABLE);
    }
    if(currentSettings.disconnectFromDeviceIfCheckFaild)
    {
        if(currentSettings.checkRequests == sitoipFailRequests)
        {
            emit changeDeviceConnectionState(false);
        }
    }
}

void MainWindow::updateConnectionStateStatSlot(ConnectionStateStat stat)
{
    QPalette connectionStateLabelPalette;
    switch (stat)
    {
    case CSS_EMPTY:
        ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
        connectionStateLabelPalette.setColor(ui->connectionStateLabel->backgroundRole(),
                                             palette().color(QPalette::Background));
        ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
        ui->connectionStateLabel->setText("");
        break;
    case CSS_STABLE:
        connectionStateLabelPalette.setColor(ui->connectionStateLabel->backgroundRole(), Qt::green);
        ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
        ui->connectionStateLabel->setText(trUtf8(" Стабильно "));
        break;
    case CSS_UNSTABLE:
        connectionStateLabelPalette.setColor(ui->connectionStateLabel->backgroundRole(),  QColor(0xff, 0x99, 0x00));
        ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
        ui->connectionStateLabel->setText(trUtf8(" Нестабильно "));
        break;
    case CSS_NO_CONNECTION:
        connectionStateLabelPalette.setColor(ui->connectionStateLabel->backgroundRole(),  Qt::red);
        ui->connectionStateLabel->setPalette(connectionStateLabelPalette);
        ui->connectionStateLabel->setText(trUtf8(" Нет связи "));
        break;
    default:
        break;
    }
}

void MainWindow::setVisibleInfoAboutConnection(bool visible)
{
    ui->deviceMacAddress->setVisible(visible);
    ui->deviceMacAddressLabel->setVisible(visible);
    ui->connectionInfoLabel->setVisible(visible);
    ui->connectionStateLabel->setVisible(visible);
}

void MainWindow::clearDeviceSettingsTab()
{
    ui->settingsDeviceIpAddressLineEdit->setText("");
    ui->settingsIpPortSpinBox->setValue(0);
    ui->settingsMacAddressLineEdit->setText("");
    ui->settingsGatewayLineEdit->setText("");
    ui->settingsGatewayMacValueLabel->setText("");
    ui->settingsNetMaskLineEdit->setText("");
}

void MainWindow::getAllDeviseSettings()
{
    SettingsCommandFamilyParams * settingsParamsGetAll = new SettingsCommandFamilyParams;
    settingsParamsGetAll->commandId = COMMAND_ID_SETTINGS_GET_ALL;
    settingsParamsGetAll->commandType = CT_REQUEST;
    CodecError codecError;
    QByteArray settingsDataGetAll = settingsCommandBuilder->encode(QSharedPointer <CommandParams>(settingsParamsGetAll), QDataStream::LittleEndian, &codecError);
    if(codecError.errType != CE_NO_ERROR)
    {
        addLogEvent("Ошибка кодирования пакета");
        return;
    }
    addLogEvent(trUtf8("Получение настроек устройства"));
    ui->deviceSettingsTab->setEnabled(false);
    getAllAnswerFilter.startWait();
    emit sendData(settingsDataGetAll);
}

void MainWindow::on_action_3_triggered()
{
    settingsForm->setSettings(settings);
    settingsForm->showSettings();
    settingsForm->show();
}

void MainWindow::on_action_7_triggered()
{
    slotSaveSeparateSitFile();
}

void MainWindow::on_action_8_triggered()
{
    slotSaveSeparateSrlFile();
}

void MainWindow::on_action_9_triggered()
{
    slotOpenSitFile();
}

void MainWindow::on_action_SITL_2_triggered()
{
    console->clear();
    resultConsole->clear();
    commandConsole->clear();
}

void MainWindow::on_action_SITL_3_triggered()
{
    slotSaveSitlLog();
}

void MainWindow::on_updateArpPushButton_clicked()
{
    ui->updateArpPushButton->setEnabled(false);
    quint64 arpTableAddress = currentSettings.arpTableAddress;
    uint arpTableRecords = currentSettings.arpTableMaxRecords;
    uint arpTableRecordSize = 15;
    uint arpTableTotalSize = arpTableRecords * arpTableRecordSize;
    QByteArray arpTable = sitlRamAccessor->readMemory(arpTableAddress, arpTableTotalSize);
    QByteArray ipAddressArray;
    QByteArray macAddressArray;
    QByteArray ttlArray;
    QByteArray flagArray;
    if(arpTable.size() == arpTableTotalSize)
    {
        qDebug() << "ARP_TABLE_HEX:";
        qDebug() << arpTable.toHex();
        int offset = 0;
        int size = arpTableRecords * 4;
        ipAddressArray = arpTable.mid(offset, size);
        offset += size;
        size = arpTableRecords * 6;
        macAddressArray = arpTable.mid(offset, size);
        offset += size;
        size = arpTableRecords * 4;
        ttlArray = arpTable.mid(offset, size);
        offset += size;
        size = arpTableRecords * 1;
        flagArray = arpTable.mid(offset, size);
    }
    else
    {
        QMessageBox::information(NULL,QObject::trUtf8("Получение ARP-таблицы"),trUtf8("Не удалось получить ARP-таблицу: таймаут ответа"));
        ui->deviceSettingsTab->setEnabled(true);
        ui->updateArpPushButton->setEnabled(true);
        return;
    }
    QList <ArpRecord> arpRecordList;
    QDataStream ipAddressStream(ipAddressArray);
    ipAddressStream.setByteOrder(QDataStream::BigEndian);
    QDataStream macAddressStream(macAddressArray);
    macAddressStream.setByteOrder(QDataStream::BigEndian);
    QDataStream ttlStream(ttlArray);
    ttlStream.setByteOrder(QDataStream::LittleEndian);
    QDataStream flagStream(flagArray);
    flagStream.setByteOrder(QDataStream::BigEndian);
    for(uint i = 0; i < arpTableRecords; ++i)
    {
        ArpRecord arpRecord;
        quint32 ipAddressBin;
        ipAddressStream >> ipAddressBin;
        arpRecord.ipAddress.setAddress(ipAddressBin);
        QByteArray macAddressBin;
        macAddressBin.resize(6);
        macAddressStream.readRawData(macAddressBin.data(), macAddressBin.size());
        arpRecord.macAddress.setMacFromByteArray(macAddressBin);
        quint32 ttlBin;
        ttlStream >> ttlBin;
        arpRecord.ttl = ttlBin;
        quint8 flagBin;
        flagStream >> flagBin;
        arpRecord.flag = flagBin;
        arpRecordList.append(arpRecord);
    }
    updateArpTable(arpRecordList);
    ui->updateArpPushButton->setEnabled(true);
}

void MainWindow::updateArpTable(QList <ArpRecord> arpTable)
{
    clearArpTable();
    for(int i = 0; i < arpTable.size(); ++i)
    {
        ArpRecord arpRecord = arpTable.at(i);
        if(arpRecord.flag == 0)
        {
            QStandardItem * ipAddrSI = new QStandardItem(arpRecord.ipAddress.toString());
            ipAddrSI->setEditable(false);
            QStandardItem * macAddrSI = new QStandardItem(arpRecord.macAddress.toString());
            macAddrSI->setEditable(false);
            QStandardItem * ttlSI = new QStandardItem(QString::number(arpRecord.ttl));
            ttlSI->setEditable(false);
            QList <QStandardItem *> arpRow;
            arpRow.append(ipAddrSI);
            arpRow.append(macAddrSI);
            arpRow.append(ttlSI);
            arpModel->appendRow(arpRow);
        }
   }
    ui->arpTableView->resizeColumnsToContents();
}

void MainWindow::clearArpTable()
{
    arpModel->removeRows(0, arpModel->rowCount());
    ui->arpTableView->resizeColumnsToContents();
}
