#ifndef SITL_COMMAND_GLOBAL_CODEC_H
#define SITL_COMMAND_GLOBAL_CODEC_H

#include "command_builder.h"
#include "sitl_command_family_codec.h"
#include "sitl_command_family_params.h"
#include <QObject>

class SitlCommandGlobalCodec : public CommandCodec
{
public:
    SitlCommandGlobalCodec();
    ~SitlCommandGlobalCodec();
    void addCodec(CommandCodec * codec);
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian);
    bool isParamsMyCommand(QSharedPointer <CommandParams> params);
private:
    void postDecodeModify(QSharedPointer <CommandParams> params);
    CommandBuilder sitlCommandBuilder;
    SitlCommandSeqAnswerCodec * sitlCommandSeqAnswerCodec;
};

#endif // SITL_COMMAND_GLOBAL_CODEC_H
