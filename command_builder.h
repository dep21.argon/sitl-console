#ifndef COMMAND_BUILDER_H
#define COMMAND_BUILDER_H

#include <QDataStream>
#include <QByteArray>
#include <QMap>
#include "command_params.h"
#include "command_codec.h"

class CommandBuilder : public QObject
{
Q_OBJECT
public:
    CommandBuilder(QObject * parent = 0);
    void setEndian(QDataStream::ByteOrder endian);
    QDataStream::ByteOrder getEndian();
    virtual void registerCodec(CommandCodec * codec);
    virtual void unregisterCodec(quint64 codecId);
    virtual QByteArray encode(QSharedPointer <CommandParams> params, CodecError * codecError);
    virtual QSharedPointer <CommandParams> decode(QByteArray rawData, CodecError * codecError);
    virtual QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual CommandCodec * getCodecFromData(QByteArray data);
    virtual CommandCodec * getCodecFromData(QByteArray data, QDataStream::ByteOrder endian);
    virtual CommandCodec * getCodecFromParams(QSharedPointer <CommandParams> param);
    //quint32 getCodecIdFromData(QByteArray data);
    //quint32 getCodecIdFromParams(CommandParams * params);
    //CommandCodec * getCodecFromId(quint32 codecId);
public slots:
    virtual void encodeSlot(QSharedPointer <CommandParams> params);
    virtual void decodeSlot(QByteArray rawData);
signals:
    void encodeDoneSignal(QByteArray rawData);
    void encodeErrorSignal();
    void decodeDoneSignal(QSharedPointer <CommandParams> params);
    void decodeErrorSignal();
private:
    QMap <quint64, CommandCodec*> m_codecs;
    QDataStream::ByteOrder endian;
};

#endif // COMMAND_BUILDER_H

