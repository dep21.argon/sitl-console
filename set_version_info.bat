@echo off
setlocal enabledelayedexpansion

FOR /F "usebackq" %%a IN (`git log -1 --pretty^=%%h`) DO (
	set build_id=^"%%a^"
)

FOR /F "usebackq" %%a IN (`git describe --tags`) DO (
	set build_version=^"%%a^"
	set build_version=^"!build_version:~2,-1!^"
)

echo #ifndef ABOUT_INFO_H > about_info.h
echo #define ABOUT_INFO_H >> about_info.h
echo. >> about_info.h
echo #define BUILD_VERSION !build_version! >> about_info.h
echo #define BUILD_ID !build_id! >> about_info.h
echo #define BUILD_DATE ^"%date:~-4,4%-%date:~-7,2%-%date:~-10,2%^" >> about_info.h
echo #define BUILD_TIME ^"%time:~0,8%^" >> about_info.h
echo. >> about_info.h
echo #endif // ABOUT_INFO_H >> about_info.h
