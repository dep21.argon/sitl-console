#include "command_codec.h"

CodecError::CodecError()
{
    errType = CE_NO_ERROR;
    errCode = 0x00;
}

CommandCodec::CommandCodec(quint64 codecId)
{
    m_codecId = codecId;
}

CommandCodec::~CommandCodec()
{

}

quint64 CommandCodec::getCodecId()
{
    return m_codecId;
}
