#ifndef SITOIP_CONSOLE_PROC_H
#define SITOIP_CONSOLE_PROC_H

#include <QtCore>
#include <QObject>

#include "console_proc.h"
#include "sitl_assembler.h"
#include "sitoip_command_family_codec.h"
#include "codec_data_param_filter.h"

enum ProcessMode{PM_IMMEDIATE_PROCESSING, PM_POST_PROCESSING};

class SitoipConsoleProc: public ConsoleProc
{
Q_OBJECT
public:
    SitoipConsoleProc();
    void startProc(QStringList commandArguments);
    void setProcessMode(ProcessMode mode);
    ProcessMode getProcessMode();
    void setEndian(QDataStream::ByteOrder endian);
    void setMaxSitlCommandsInSitoipPacket(uint sitlCommands);
    uint getMaxSitlCommandsInSitoipPacket();
    void process();
    QList <QSharedPointer <CommandParams> > createSitoipPackets(QList <QSharedPointer <CommandParams> > sitlCommandList);
    void sendSitoipPackets(QList <QSharedPointer <CommandParams> > sitoipParams);
    void addAssembler(SitlAssembler * assembler);
    void addCodec(SitoipCommandFamilyCodec * sitoipCodec);
signals:
    void sitoipRawCommandOutput(QByteArray sitoipRawCommand);
    void receivedSitlAnswerList(QList <QSharedPointer <CommandParams> > sitlAnswerList);
public slots:
    void sitoipRawCommandInpup(QByteArray sitoipRawCommand);
    void showSitoipPacket(QSharedPointer <CommandParams> sitoipParams);
    void handleInputPacket(QSharedPointer <CommandParams> sitoipParams);
    void sendSitlRequestList(QList <QSharedPointer <CommandParams> > sitlRequestList);
private:
    CodecDataParamFilter sitoipFilter;
    QList <QSharedPointer <CommandParams> > sitlCommands;
    SitlAssembler * assembler;
    ProcessMode processMode;
    QDataStream::ByteOrder endian;
    uint maxSitlCommandsInSitoipPacket;
};

#endif // SITOIP_CONSOLE_PROC_H

