#include "command_params.h"

bool CommandParams::operator ==(const CommandParams & params) const
{
    return ((commandId == params.commandId) && (commandType == params.commandType));
}

CommandParams::~CommandParams()
{
}
