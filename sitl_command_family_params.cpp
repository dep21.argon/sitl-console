#include "sitl_command_family_params.h"

SitlCommandFamilyParams::SitlCommandFamilyParams()
{
}

SitlCommandFamilyParams::SitlCommandFamilyParams(quint64 commandId, CommandType commandType)
{
    this->commandId = commandId;
    this->commandType = commandType;
    this->isMultipleTransaction = false;
}

SitlCommandFamilyParams::~SitlCommandFamilyParams()
{
}

bool SitlCommandFamilyParams::operator ==(const SitlCommandFamilyParams & params) const
{
    if(commandType == CT_REQUEST)
    {
        if((commandId == COMMAND_ID_SITL_MWR)
                || (commandId == COMMAND_ID_SITL_MRD))
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (wordSize == params.wordSize)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord));
        }
        if((commandId == COMMAND_ID_SITL_LIST)
                || (commandId == COMMAND_ID_SITL_IDEN)
                || (commandId == COMMAND_ID_SITL_SYNER))
        {
            return (commandId == params.commandId);
        }
        if((commandId == COMMAND_ID_SITL_MWX)
                || (commandId == COMMAND_ID_SITL_MWI))
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (wordSize == params.wordSize)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (haveWordInMessage == params.haveWordInMessage)
                    && (!haveWordInMessage || (dataWord == params.dataWord))
                    && (isMultipleTransaction == params.isMultipleTransaction)
                    && (placeInTransaction == params.placeInTransaction)
                    && (useAddressAutoincrement == params.useAddressAutoincrement));
        }
        if(commandId == COMMAND_ID_SITL_MRX)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (wordSize == params.wordSize)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (useAddressAutoincrement == params.useAddressAutoincrement)
                    && (wordsCount == params.wordsCount));
        }
        if(commandId == COMMAND_ID_SITL_SEQ)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (haveAddressInMessage == params.haveAddressInMessage)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (haveWordInMessage == params.haveWordInMessage)
                    && (!haveWordInMessage || (wordSize == params.wordSize))
                    && (!haveWordInMessage || (dataWord == params.dataWord))
                    && (isMultipleTransaction == params.isMultipleTransaction)
                    && (placeInTransaction == params.placeInTransaction)
                    && (useAddressAutoincrement == params.useAddressAutoincrement));
        }
        if((commandId == COMMAND_ID_SITL_MRL)
                || commandId == COMMAND_ID_SITL_MRM)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (wordSize == params.wordSize)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (useAddressAutoincrement == params.useAddressAutoincrement));
        }
        if(commandId == COMMAND_ID_SITL_CLINE)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType));
        }
        if(commandId == COMMAND_ID_SITL_CLSET)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (cacheSize == params.cacheSize));
        }
    }
    else if(commandType == CT_ANSWER)
    {
        if((commandId == COMMAND_ID_SITL_MWR)
                || (commandId == COMMAND_ID_SITL_MRD))
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (answerCode == params.answerCode));
        }
        if((commandId == COMMAND_ID_SITL_LIST)
                || (commandId == COMMAND_ID_SITL_IDEN)
                || (commandId == COMMAND_ID_SITL_SYNER))
        {
            return (commandId == params.commandId);
        }
        if((commandId == COMMAND_ID_SITL_MWX)
                || (commandId == COMMAND_ID_SITL_MWI)
                || (commandId == COMMAND_ID_SITL_MRX)
                || (commandId == COMMAND_ID_SITL_MRL)
                || (commandId == COMMAND_ID_SITL_MRM)
                || (commandId == COMMAND_ID_SITL_SEQ))
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (answerCode == params.answerCode)
                    && (haveAddressInMessage == params.haveAddressInMessage)
                    && (memoryAddressSize == params.memoryAddressSize)
                    && (addressWord == params.addressWord)
                    && (haveWordInMessage == params.haveWordInMessage)
                    && (!haveWordInMessage || (wordSize == params.wordSize))
                    && (!haveWordInMessage || (dataWord == params.dataWord))
                    && (isMultipleTransaction == params.isMultipleTransaction)
                    && (placeInTransaction == params.placeInTransaction)
                    && (useAddressAutoincrement == params.useAddressAutoincrement));
        }
        if(commandId == COMMAND_ID_SITL_INT)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (answerCode == params.answerCode)
                    && (irqNumber == params.irqNumber)
                    && (haveWordInMessage == params.haveWordInMessage)
                    && (!haveWordInMessage || (dataWord == params.dataWord))
                    && (!haveWordInMessage || (wordSize == params.wordSize)));
        }
        if(commandId == COMMAND_ID_SITL_CLINE)
        {
            return ((commandId == params.commandId)
                    && (commandType == params.commandType)
                    && (answerCode == params.answerCode)
                    && (cacheSize == params.cacheSize));
        }
    }
    //return CommandParams::operator ==(params);
    return false;
}

unsigned int SitlCommandFamilyParams::GetBytesMemoryAddressSize()
{
    switch(memoryAddressSize)
    {
        case MAS_16BIT:
            return 2;
            break;
        case MAS_24BIT:
            return 3;
            break;
        case MAS_32BIT:
            return 4;
            break;
        case MAS_64BIT:
            return 8;
            break;
    }
    return 0;
}

unsigned int SitlCommandFamilyParams::GetBytesWordSize()
{
    switch(wordSize)
    {
        case WS_8BIT:
            return 1;
            break;
        case WS_16BIT:
            return 2;
            break;
        case WS_32BIT:
            return 4;
            break;
        case WS_64BIT:
            return 8;
            break;
    }
    return 0;
}

SitlCommandFamilyParamsRequest::SitlCommandFamilyParamsRequest(quint64 commandId)
    :SitlCommandFamilyParams(commandId, CT_REQUEST)
{

}

SitlCommandFamilyParamsAnswer::SitlCommandFamilyParamsAnswer(quint64 commandId, AnswerCode answerCode)
    :SitlCommandFamilyParams(commandId, CT_ANSWER)
{
    this->answerCode = answerCode;
}

SitlCommandEmptyParamsRequest::SitlCommandEmptyParamsRequest()
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_EMPTY)
{

}

SitlCommandEmptyParamsAnswer::SitlCommandEmptyParamsAnswer()
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_EMPTY, AC_DONE)
{

}

SitlCommandSynerParamsRequest::SitlCommandSynerParamsRequest()
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_SYNER)
{

}

SitlCommandSynerParamsAnswer::SitlCommandSynerParamsAnswer()
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_SYNER, AC_ICTRL)
{

}

SitlCommandListParamsRequest::SitlCommandListParamsRequest()
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_LIST)
{

}

SitlCommandListParamsAnswer::SitlCommandListParamsAnswer()
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_LIST, AC_DONE)
{

}

SitlCommandIdenParamsRequest::SitlCommandIdenParamsRequest()
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_IDEN)
{

}

SitlCommandIdenParamsAnswer::SitlCommandIdenParamsAnswer()
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_IDEN, AC_DONE)
{

}

SitlCommandWriteParamsRequest::SitlCommandWriteParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize)
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_MWR)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
}

SitlCommandWriteParamsAnswer::SitlCommandWriteParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, AnswerCode answerCode)
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_MWR, answerCode)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
}

SitlCommandReadParamsRequest::SitlCommandReadParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize)
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_MRD)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
}

SitlCommandReadParamsAnswer::SitlCommandReadParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, AnswerCode answerCode)
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_MRD, answerCode)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
}

SitlCommandIntParamsAnswer::SitlCommandIntParamsAnswer(quint64 dataWord, quint8 irqNumber, AnswerCode answerCode)
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_INT, answerCode)
{
    this->dataWord = dataWord;
    this->irqNumber = irqNumber;
}

SitlCommandMWXParamsRequest::SitlCommandMWXParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, PlaceInTransaction placeInTransaction, bool useAddressAutoincrement)
    :SitlCommandFamilyParamsRequest(COMMAND_ID_SITL_MWX)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
    this->placeInTransaction = placeInTransaction;
    this->useAddressAutoincrement = useAddressAutoincrement;
}

SitlCommandMWXParamsAnswer::SitlCommandMWXParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, PlaceInTransaction placeInTransaction, bool useAddressAutoincrement, AnswerCode answerCode)
    :SitlCommandFamilyParamsAnswer(COMMAND_ID_SITL_MWX, answerCode)
{
    this->dataWord = dataWord;
    this->addressWord = addressWord;
    this->wordSize = wordSize;
    this->memoryAddressSize = memoryAddressSize;
    this->placeInTransaction = placeInTransaction;
    this->useAddressAutoincrement = useAddressAutoincrement;
}
