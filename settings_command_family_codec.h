#ifndef SETTINGS_COMMAND_FAMILY_CODEC_H
#define SETTINGS_COMMAND_FAMILY_CODEC_H

#include "settings_command_family_params.h"
#include "command_codec.h"

#define COMMAND_ID_SETTINGS_ANSWER 0x01

#define CODEC_ID_SETTINGS_SET_SOCKET_REQUEST        0x0000000300000200
#define CODEC_ID_SETTINGS_SET_SOCKET_ANSWER         0x0000000300000201
#define CODEC_ID_SETTINGS_SET_MAC_REQUEST           0x0000000300000300
#define CODEC_ID_SETTINGS_SET_MAC_ANSWER            0x0000000300000301
#define CODEC_ID_SETTINGS_SET_GATEWAY_IP_REQUEST    0x0000000300000700
#define CODEC_ID_SETTINGS_SET_GATEWAY_IP_ANSWER     0x0000000300000701
#define CODEC_ID_SETTINGS_SET_NETMASK_REQUEST       0x0000000300000800
#define CODEC_ID_SETTINGS_SET_NETMASK_ANSWER        0x0000000300000801
#define CODEC_ID_SETTINGS_GET_ALL_REQUEST           0x0000000300000900
#define CODEC_ID_SETTINGS_GET_ALL_ANSWER            0x0000000300000901

class SettingsCommandFamilyCodec: public CommandCodec
{
public:
    SettingsCommandFamilyCodec(quint64 codecId, quint64 commandId, CommandType commandType);
    virtual QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian);
    virtual bool isParamsMyCommand(QSharedPointer <CommandParams> params);
    quint64 m_commandId;
    CommandType m_commandType;
};

class SettingsCommandSetSocketRequestCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetSocketRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SettingsCommandSetSocketAnswerCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetSocketAnswerCodec();
};

class SettingsCommandSetMacRequestCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetMacRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SettingsCommandSetMacAnswerCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetMacAnswerCodec();
};

class SettingsCommandSetGatewayIpRequestCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetGatewayIpRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SettingsCommandSetGatewayIpAnswerCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetGatewayIpAnswerCodec();
};

class SettingsCommandSetNetmaskRequestCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetNetmaskRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SettingsCommandSetNetmaskAnswerCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandSetNetmaskAnswerCodec();
};

class SettingsCommandGetAllRequestCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandGetAllRequestCodec();
};

class SettingsCommandGetAllAnswerCodec: public SettingsCommandFamilyCodec
{
public:
    SettingsCommandGetAllAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

#endif // SETTINGS_COMMAND_FAMILY_CODEC_H

