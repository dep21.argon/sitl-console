#ifndef SITL_COMMAND_FAMILY_PARAMS_H
#define SITL_COMMAND_FAMILY_PARAMS_H

#include "command_params.h"
#include "command_codec.h"

#define COMMAND_ID_SITL_EMPTY       0x00000001000000FF
#define COMMAND_ID_SITL_LIST        0x0000000100000010
#define COMMAND_ID_SITL_IDEN        0x0000000100000011
#define COMMAND_ID_SITL_CLINE       0x0000000100000012
#define COMMAND_ID_SITL_CLSET       0x0000000100000150
#define COMMAND_ID_SITL_SYNER       0x0000000100000013
#define COMMAND_ID_SITL_MWR         0x0000000100000000
#define COMMAND_ID_SITL_MRD         0x0000000100000040
#define COMMAND_ID_SITL_INT         0x0000000100000050
#define COMMAND_ID_SITL_MWX         0x0000000100000080
#define COMMAND_ID_SITL_MRX         0x00000001000000C0
#define COMMAND_ID_SITL_MWI         0x0000000100000090
#define COMMAND_ID_SITL_MRL         0x00000001000000D0
#define COMMAND_ID_SITL_MRM         0x00000001000000D4
#define COMMAND_ID_SITL_SEQ         0x0000000100000053

/*
#define COMMAND_ID_SITL_CLSET       0x50

#define SITL_ANSWER_CODE_ICTRL      0x00
#define SITL_ANSWER_CODE_DONE       0x01
#define SITL_ANSWER_CODE_ERDON      0x02
#define SITL_ANSWER_CODE_ABORT      0x03
#define SITL_ANSWER_CODE_ERABR      0x04
#define SITL_ANSWER_CODE_RETRY      0x05
#define SITL_ANSWER_CODE_ERRET      0x06
*/

enum AnswerCode{AC_ICTRL, AC_DONE, AC_ERDON, AC_ABORT, AC_ERABR, AC_RETRY, AC_ERRET, AC_SETUP, AC_ACTIV};

enum WordSize{WS_8BIT, WS_16BIT, WS_32BIT, WS_64BIT};

enum MemoryAddressSize{MAS_16BIT, MAS_24BIT, MAS_32BIT, MAS_64BIT};

enum PlaceInTransaction{PIT_FIRST, PIT_INTERMEDIATE, PIT_LAST};


class SitlCommandFamilyParams: public CommandParams
{
public:
    SitlCommandFamilyParams();
    SitlCommandFamilyParams(quint64 commandId, CommandType commandType);
    ~SitlCommandFamilyParams();
    bool operator==(const SitlCommandFamilyParams & params) const;
    WordSize wordSize;
    unsigned int GetBytesWordSize();
    MemoryAddressSize memoryAddressSize;
    unsigned int GetBytesMemoryAddressSize();
    quint64 dataWord;
    quint64 addressWord;
    quint8 irqNumber;
    AnswerCode answerCode;
    bool haveWordInMessage;
    bool haveAddressInMessage;
    PlaceInTransaction placeInTransaction;
    bool useAddressAutoincrement;
    quint64 wordsCount;
    bool isMultipleTransaction;
    quint16 cacheSize;
};

class SitlCommandFamilyParamsRequest: public SitlCommandFamilyParams
{
public:
    SitlCommandFamilyParamsRequest(quint64 commandId);
};

class SitlCommandFamilyParamsAnswer: public SitlCommandFamilyParams
{
public:
    SitlCommandFamilyParamsAnswer(quint64 commandId, AnswerCode answerCode);
};

class SitlCommandEmptyParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandEmptyParamsRequest();
};

class SitlCommandEmptyParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandEmptyParamsAnswer();
};

class SitlCommandListParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandListParamsRequest();
};
class SitlCommandListParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandListParamsAnswer();
};

class SitlCommandIdenParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandIdenParamsRequest();
};

class SitlCommandIdenParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandIdenParamsAnswer();
};

class SitlCommandWriteParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandWriteParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize);
};

class SitlCommandWriteParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandWriteParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, AnswerCode answerCode);
};

class SitlCommandReadParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandReadParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize);
};

class SitlCommandReadParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandReadParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, AnswerCode answerCode);
};

class SitlCommandSynerParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandSynerParamsRequest();
};

class SitlCommandSynerParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandSynerParamsAnswer();
};

class SitlCommandIntParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandIntParamsAnswer(quint64 dataWord, quint8 irqNumber, AnswerCode answerCode);
};

class SitlCommandMWXParamsRequest: public SitlCommandFamilyParamsRequest
{
public:
    SitlCommandMWXParamsRequest(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, PlaceInTransaction placeInTransaction, bool useAddressAutoincrement);
};

class SitlCommandMWXParamsAnswer: public SitlCommandFamilyParamsAnswer
{
public:
    SitlCommandMWXParamsAnswer(quint64 dataWord, quint64 addressWord, WordSize wordSize, MemoryAddressSize memoryAddressSize, PlaceInTransaction placeInTransaction, bool useAddressAutoincrement, AnswerCode answerCode);
};

Q_DECLARE_METATYPE(SitlCommandFamilyParams)
Q_DECLARE_METATYPE(SitlCommandFamilyParams *)

#endif // SITL_COMMAND_FAMILY_PARAMS_H

