#ifndef UDP_DEVICE_CONNECTION_H
#define UDP_DEVICE_CONNECTION_H

#include "device_connection.h"
#include <QUdpSocket>
#include <QHostAddress>
#include <QtCore>

class UdpDeviceConnection : public DeviceConnection
{
Q_OBJECT
public:
    UdpDeviceConnection(QHostAddress ipAddress, quint16 ipPort, int checkConnectionInterval);
    void setSocket(QHostAddress ipAddress, quint16 ipPort);
    void setCheckConnectionInterval(int interval);
    ConnectionState getConnectionState();
    quint16 getMyPort();
//signals:
//    void disconnectNotify();
//    void connectNotify();
//    void dataSended(QByteArray rawData);
//    void dataReceived(QByteArray rawData);
public slots:
    void connectDevice();
    void disconnectDevice();
    int sendData(QByteArray rawData);
    void slotDataReceived();
    void checkConnection();
private:
    QUdpSocket * udpConnection;
    QHostAddress deviceIpAddress;
    quint16 deviceIpPort;
    int checkConnectionInterval;
    ConnectionState connectionState;
    QTimer checkTimer;
};

#endif // UDP_DEVICE_CONNECTION_H

