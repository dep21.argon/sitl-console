#include "sitoip_command_family_params.h"

SitoipCommandFamilyParams::SitoipCommandFamilyParams()
{
    commandId = COMMAND_ID_SITOIP_EMPTY;
    commandType = CT_REQUEST;
}

SitoipCommandFamilyParams::SitoipCommandFamilyParams(quint64 commandId, CommandType commandType)
{
    this->commandId = commandId;
    this->commandType = commandType;
}

SitoipCommandFamilyParams::~SitoipCommandFamilyParams()
{
}

bool SitoipCommandFamilyParams::operator==(const SitoipCommandFamilyParams &params) const
{
    if((commandId != params.commandId)
            || (commandType != params.commandType)
            || sitlCommandsList.size() != params.sitlCommandsList.size())
    {
        return false;
    }
    for(int i = 0; i < sitlCommandsList.size(); ++i)
    {
        if(!(*sitlCommandsList.at(i) == *params.sitlCommandsList.at(i)))
        {
            return false;
        }
    }
    return true;
}

void SitoipCommandFamilyParams::clear()
{
//    for(int i = 0; i < sitlCommandsList.size(); ++i)
//    {
//        CommandParams * sitlCommand = sitlCommandsList.at(i);
//        delete sitlCommand;
//    }
    sitlCommandsList.clear();
}
