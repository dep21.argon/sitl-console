#include "settings_command_family_codec.h"

SettingsCommandFamilyCodec::SettingsCommandFamilyCodec(quint64 codecId, quint64 commandId, CommandType commandType)
    :CommandCodec(codecId)
{
    m_commandId = commandId;
    m_commandType = commandType;
}

bool SettingsCommandFamilyCodec::isParamsMyCommand(QSharedPointer <CommandParams> params)
{
    return ((m_commandId == params.data()->commandId)
            && (m_commandType == params.data()->commandType));
}

bool SettingsCommandFamilyCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    if(m_commandType == CT_REQUEST)
    {
        quint8 header;
        dataStream >> header;
        if(header == (quint8)m_commandId)
        {
            return true;
        }
    }
    else if(m_commandType == CT_ANSWER)
    {
        quint8 header;
        dataStream >> header;
        if(header == (quint8)COMMAND_ID_SETTINGS_ANSWER)
        {
            quint8 paramsCount;
            dataStream >> paramsCount;
            quint16 command;
            dataStream >> command;
            if(((quint8)command == (quint8)m_commandId) || (quint8)command == (quint8)~m_commandId)
            {
                return true;
            }

        }
    }
    return false;
}

QByteArray SettingsCommandFamilyCodec::encode(QSharedPointer <CommandParams> params, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = (SettingsCommandFamilyParams *)params.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandId != settingsParams->commandId)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(m_commandType != settingsParams->commandType)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(params->commandType == CT_REQUEST)
    {
        dataStream << (quint8)m_commandId;
        dataStream << (quint8)0x00;
    }
    else if(params->commandType == CT_ANSWER)
    {
        dataStream << (quint8)COMMAND_ID_SETTINGS_ANSWER;
        dataStream << (quint8)0x01;
        quint16 commandCode = (quint16)(0xFF & m_commandId);
        if(params->commandStatus == STATUS_SUCCESS)
        {
            commandCode = (quint16)(0xFF & m_commandId);
        }
        else if(params->commandStatus == STATUS_ERROR)
        {
            commandCode = ~(quint16)(0xFF & m_commandId);
        }
        dataStream << commandCode;
    }
    return data;
}

QSharedPointer <CommandParams> SettingsCommandFamilyCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = new SettingsCommandFamilyParams();
    settingsParams->commandType = m_commandType;
    settingsParams->commandId = 0xFF;
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandType == CT_REQUEST)
    {
        quint8 header;
        dataStream >> header;
        if(header != (quint8)m_commandId)
        {
            codecError->errType = CE_CANT_DECODE;
            return QSharedPointer <CommandParams>(settingsParams);
        }
        settingsParams->commandId = m_commandId;
        settingsParams->commandType = CT_REQUEST;
    }
    else if(m_commandType == CT_ANSWER)
    {
        quint8 header;
        dataStream >> header;
        if(header != COMMAND_ID_SETTINGS_ANSWER)
        {
            codecError->errType = CE_CANT_DECODE;
            return QSharedPointer <CommandParams>(settingsParams);
        }
        quint8 paramsCount;
        dataStream >> paramsCount;
        if(paramsCount < 1)
        {
            codecError->errType = CE_CANT_DECODE;
            return QSharedPointer <CommandParams>(settingsParams);
        }
        quint16 command;
        dataStream >> command;
        if(command == (quint16)(0xFF & m_commandId))
        {
            settingsParams->commandId = m_commandId;
            settingsParams->commandStatus = STATUS_SUCCESS;
        }
        else if((quint8)command == (quint8)~m_commandId)
        {
            settingsParams->commandId = m_commandId;
            settingsParams->commandStatus = STATUS_ERROR;
            settingsParams->errorCode = EC_DEVICE_ERROR;
        }
        else
        {
            codecError->errType = CE_CANT_DECODE;
            return QSharedPointer <CommandParams>(settingsParams);
        }
    }
    return QSharedPointer <CommandParams>(settingsParams);
}

SettingsCommandSetSocketRequestCodec::SettingsCommandSetSocketRequestCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_SOCKET_REQUEST, COMMAND_ID_SETTINGS_SET_SOCKET, CT_REQUEST)
{

}

QByteArray SettingsCommandSetSocketRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = (SettingsCommandFamilyParams *)param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandId != settingsParams->commandId)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(m_commandType != settingsParams->commandType)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    dataStream << (quint8)m_commandId;
    dataStream << (quint8)0x03;
    quint16 hIpAddr = (quint16)(settingsParams->deviceIpAddress.toIPv4Address() >> 16);
    quint16 lIpAddr = (quint16)settingsParams->deviceIpAddress.toIPv4Address();
    dataStream << hIpAddr << lIpAddr << settingsParams->deviceIpPort;
    return data;
}

SettingsCommandSetSocketAnswerCodec::SettingsCommandSetSocketAnswerCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_SOCKET_ANSWER, COMMAND_ID_SETTINGS_SET_SOCKET, CT_ANSWER)
{

}

SettingsCommandSetMacRequestCodec::SettingsCommandSetMacRequestCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_MAC_REQUEST, COMMAND_ID_SETTINGS_SET_MAC, CT_REQUEST)
{

}

QByteArray SettingsCommandSetMacRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = (SettingsCommandFamilyParams *)param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandId != settingsParams->commandId)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(m_commandType != settingsParams->commandType)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    dataStream << (quint8)m_commandId;
    dataStream << (quint8)0x03;
    quint16 mac1 = ((((quint16)settingsParams->deviceMacAddress.at(0) << 8) & 0xff00) | ((quint16)settingsParams->deviceMacAddress.at(1) & 0x00ff));
    quint16 mac2 = ((((quint16)settingsParams->deviceMacAddress.at(2) << 8) & 0xff00) | ((quint16)settingsParams->deviceMacAddress.at(3) & 0x00ff));
    quint16 mac3 = ((((quint16)settingsParams->deviceMacAddress.at(4) << 8) & 0xff00) | ((quint16)settingsParams->deviceMacAddress.at(5) & 0x00ff));
    dataStream << mac1 << mac2 << mac3;
    return data;
}

SettingsCommandSetMacAnswerCodec::SettingsCommandSetMacAnswerCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_MAC_ANSWER, COMMAND_ID_SETTINGS_SET_MAC, CT_ANSWER)
{

}

SettingsCommandSetGatewayIpRequestCodec::SettingsCommandSetGatewayIpRequestCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_GATEWAY_IP_REQUEST, COMMAND_ID_SETTINGS_SET_GATEWAY_IP, CT_REQUEST)
{

}

QByteArray SettingsCommandSetGatewayIpRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = (SettingsCommandFamilyParams *)param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandId != settingsParams->commandId)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(m_commandType != settingsParams->commandType)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    dataStream << (quint8)m_commandId;
    dataStream << (quint8)0x02;
    quint16 hGateway = (quint16)(settingsParams->gatewayIpAddress.toIPv4Address() >> 16);
    quint16 lGateway = (quint16)settingsParams->gatewayIpAddress.toIPv4Address();
    dataStream << hGateway << lGateway;
    return data;
}

SettingsCommandSetGatewayIpAnswerCodec::SettingsCommandSetGatewayIpAnswerCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_GATEWAY_IP_ANSWER, COMMAND_ID_SETTINGS_SET_GATEWAY_IP, CT_ANSWER)
{

}

SettingsCommandSetNetmaskRequestCodec::SettingsCommandSetNetmaskRequestCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_NETMASK_REQUEST, COMMAND_ID_SETTINGS_SET_NETMASK, CT_REQUEST)
{

}

QByteArray SettingsCommandSetNetmaskRequestCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = (SettingsCommandFamilyParams *)param.data();
    QByteArray data;
    QDataStream dataStream(&data, QIODevice::WriteOnly);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    if(m_commandId != settingsParams->commandId)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    if(m_commandType != settingsParams->commandType)
    {
        codecError->errType = CE_CANT_ENCODE;
        return data;
    }
    dataStream << (quint8)m_commandId;
    dataStream << (quint8)0x02;
    quint16 hNetmask = (quint16)(settingsParams->netmask.toIPv4Address() >> 16);
    quint16 lNetmask = (quint16)settingsParams->netmask.toIPv4Address();
    dataStream << hNetmask << lNetmask;
    return data;
}

SettingsCommandSetNetmaskAnswerCodec::SettingsCommandSetNetmaskAnswerCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_SET_NETMASK_ANSWER, COMMAND_ID_SETTINGS_SET_NETMASK, CT_ANSWER)
{

}

SettingsCommandGetAllRequestCodec::SettingsCommandGetAllRequestCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_GET_ALL_REQUEST, COMMAND_ID_SETTINGS_GET_ALL, CT_REQUEST)
{

}

SettingsCommandGetAllAnswerCodec::SettingsCommandGetAllAnswerCodec()
    :SettingsCommandFamilyCodec(CODEC_ID_SETTINGS_GET_ALL_ANSWER, COMMAND_ID_SETTINGS_GET_ALL, CT_ANSWER)
{

}

QSharedPointer <CommandParams> SettingsCommandGetAllAnswerCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    SettingsCommandFamilyParams * settingsParams = new SettingsCommandFamilyParams();
    settingsParams->commandId = m_commandId;
    settingsParams->commandType = m_commandType;
    QDataStream dataStream(data);
    dataStream.setByteOrder(endian);
    codecError->errType = CE_NO_ERROR;
    quint8 header;
    dataStream >> header;
    if(header != COMMAND_ID_SETTINGS_ANSWER)
    {
        codecError->errType = CE_CANT_DECODE;
        return QSharedPointer <CommandParams>(settingsParams);
    }
    quint8 paramsCount;
    dataStream >> paramsCount;
    if(paramsCount != 11)
    {
        codecError->errType = CE_CANT_DECODE;
        return QSharedPointer <CommandParams>(settingsParams);
    }
    quint16 command;
    dataStream >> command;
    if(command == (quint16)(0xFF & m_commandId))
    {
        settingsParams->commandId = m_commandId;
        settingsParams->commandStatus = STATUS_SUCCESS;
        quint16 hIpAddr;
        quint16 lIpAddr;
        dataStream >> hIpAddr >> lIpAddr;
        settingsParams->deviceIpAddress.setAddress((quint32)(((quint32)hIpAddr << 16) | (quint32)lIpAddr));
        dataStream >> settingsParams->deviceIpPort;
        quint16 mac1;
        quint16 mac2;
        quint16 mac3;
        dataStream >> mac1 >> mac2 >> mac3;
        settingsParams->deviceMacAddress.resize(6);
        settingsParams->deviceMacAddress[0] = (quint8)(mac1 >> 8);
        settingsParams->deviceMacAddress[1] = (quint8)(mac1);
        settingsParams->deviceMacAddress[2] = (quint8)(mac2 >> 8);
        settingsParams->deviceMacAddress[3] = (quint8)(mac2);
        settingsParams->deviceMacAddress[4] = (quint8)(mac3 >> 8);
        settingsParams->deviceMacAddress[5] = (quint8)(mac3);
        //settingsParams->deviceMacAddress = (quint64)(((quint64)mac1 << 32) | ((quint64)mac2 << 16) | (quint64)mac3);
        dataStream >> hIpAddr >> lIpAddr;
        settingsParams->gatewayIpAddress.setAddress((quint32)(((quint32)hIpAddr << 16) | (quint32)lIpAddr));
        dataStream >> hIpAddr >> lIpAddr;
        settingsParams->netmask.setAddress((quint32)(((quint32)hIpAddr << 16) | (quint32)lIpAddr));
        dataStream >> mac1 >> mac2 >> mac3;
        settingsParams->gatewayMacAddress.resize(6);
        settingsParams->gatewayMacAddress[0] = (quint8)(mac1 >> 8);
        settingsParams->gatewayMacAddress[1] = (quint8)(mac1);
        settingsParams->gatewayMacAddress[2] = (quint8)(mac2 >> 8);
        settingsParams->gatewayMacAddress[3] = (quint8)(mac2);
        settingsParams->gatewayMacAddress[4] = (quint8)(mac3 >> 8);
        settingsParams->gatewayMacAddress[5] = (quint8)(mac3);
    }
    else if((quint8)command == (quint8)~m_commandId)
    {
        settingsParams->commandId = m_commandId;
        settingsParams->commandStatus = STATUS_ERROR;
        settingsParams->errorCode = EC_DEVICE_ERROR;
    }
    else
    {
        codecError->errType = CE_CANT_DECODE;
        return QSharedPointer <CommandParams>(settingsParams);
    }
    return QSharedPointer <CommandParams>(settingsParams);
}
