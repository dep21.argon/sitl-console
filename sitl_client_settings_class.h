#ifndef SITL_CLIENT_SETTINGS_CLASS_H
#define SITL_CLIENT_SETTINGS_CLASS_H

#include <QDataStream>
#include "sitl_command_family_params.h"

class SitlClientSettingsClass
{
public:
    SitlClientSettingsClass();
    void loadSettingsFromFile(QString fileName);
    void saveSettingsToFile(QString fileName);
    bool needShowNetworkDataInLog;
    uint pingRequestCount;
    uint pingRequestTimeout;
    bool checkConnection;
    uint checkInterval;
    uint checkTimeout;
    uint checkRequests;
    uint checkMaxFailRequestsToBeStable;
    bool disconnectFromDeviceIfCheckFaild;
    bool getDeviceSettingsOnConnect;
    WordSize sitlIntStatusSize;
    uint maxSitlWordsInSitoipPacket;
    quint64 arpTableAddress;
    uint arpTableMaxRecords;
};

#endif // SITL_CLIENT_SETTINGS_CLASS_H

