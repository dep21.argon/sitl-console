#ifndef CONSOLE_PROC_H
#define CONSOLE_PROC_H

#include <QObject>

class ConsoleProc: public QObject
{
Q_OBJECT
public:
    virtual void startProc(QStringList commandArguments) = 0;
signals:
    void output(QString outputString);
    void returnControl();
public slots:
    virtual void input(QString inputString);
};

#endif // CONSOLE_PROC_H

