#ifndef SITOIP_COMMAND_FAMILY_CODEC_H
#define SITOIP_COMMAND_FAMILY_CODEC_H

#define CODEC_ID_SITOIP         0x00000002000081FF
#define CODEC_ID_SITOIP_EMPTY   0x0000000200008000

#define SITOIP_COMMAND_REQUEST          0x80
#define SITOIP_COMMAND_ANSWER           0x81
#define SITOIP_COMMAND_EMPTY            0x00
#define SITOIP_COMMAND_REPEAT_REQUEST   0x82
#define SITOIP_COMMAND_REPEAT_ANSWER    0x83

#include "sitoip_command_family_params.h"
#include "sitl_command_family_codec.h"
#include "command_builder.h"

class SitoipCommandEmptyCodec: public CommandCodec
{
public:
    SitoipCommandEmptyCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian);
    bool isParamsMyCommand(QSharedPointer <CommandParams> params);
};

class SitoipCommandFamilyCodec: public CommandCodec
{
public:
    SitoipCommandFamilyCodec();
    void addCodec(CommandCodec * codec);
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian);
    bool isParamsMyCommand(QSharedPointer <CommandParams> params);
private:
    CommandBuilder sitoipCommandBuilder;
};

#endif // SITOIP_COMMAND_FAMILY_CODEC_H

