#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "udp_device_connection.h"
#include "command_builder.h"
#include "sitoip_command_family_codec.h"
#include "sitoip_command_family_params.h"
#include "settings_command_family_params.h"
#include "settings_command_family_codec.h"
#include "sitl_command_family_codec.h"
#include "sitl_command_global_codec.h"
#include "sitl_assembler.h"
#include "console.h"
#include "codec_data_param_timeout_filter.h"
#include "icmp_funcs.h"
#include "check_sitoip_connection.h"
#include "sitl_client_settings_class.h"
#include "settings_form.h"
#include "console_engine.h"
#include "sitoip_console_proc.h"
#include "help_console_proc.h"
#include "sitl_ram_accessor.h"
#include "arp_record.h"

#define MAX_SITL_PACKETS_IN_UDP_SITOIP_PACKET 76

namespace Ui {
class MainWindow;
}

enum ConnectionStateStat {CSS_EMPTY, CSS_STABLE, CSS_UNSTABLE, CSS_NO_CONNECTION};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QHostAddress getDeviceIpAddress();
    quint16 getDeviceIpPort();
    void setDeviceSocket(QHostAddress ipAddress, quint16 ipPort);
    void connectToDevice();
    void disconnectFromDevice();
    void addLogEvent(QString event);
    void setPingButtonToStartState();
    void setPingButtonToStopState();
    void setVisibleInfoAboutConnection(bool visible);
    void clearDeviceSettingsTab();
    void getAllDeviseSettings();

    bool settingsSocketChanged;
    bool settingsMacChanged;
    bool settingsGathewayChanged;
    bool settingsNetmaskChanged;

    //void sendSitlCommand(QString sitlCommand);
    //void sendData(QByteArray rawData);

signals:
    void changeDeviceConnectionState(bool);
    void sendData(QByteArray rawData);
    void updateConnectionStateStatSignal(ConnectionStateStat stat);
    //void sitlCommand(QString command);
    //void sitlResult(QString result);

private slots:
    void changeDeviceConnectionStateSlot(bool isConnected);
    void sendDataSlot(QByteArray rawData);
    void sendedDataSlot(QByteArray rawData);
    void receiveDataSlot(QByteArray rawData);
    void receiveSitoIpCommand(QByteArray rawData);
    void sendSitlCommandSlot(QString sitlCommand);
    void sitlAnswerTimeout();

    void on_connectionButton_clicked();
    void on_setDeviceSettingsButton_clicked();
    void on_setDeviceSettingsPermanentButton_clicked();
    void on_setSocketPushButton_clicked();

    void slotSetSocketAnswer(QSharedPointer <CommandParams> params);
    void slotSetSocketTimeout();

    void slotSetMacAnswer(QSharedPointer <CommandParams> params);
    void slotSetMacTimeout();

    void slotSetNetmaskAnswer(QSharedPointer <CommandParams> params);
    void slotSetNetmaskTimeout();

    void slotSetGatewayAnswer(QSharedPointer <CommandParams> params);
    void slotSetGatewayTimeout();

    void slotGetAllAnswer(QSharedPointer <CommandParams> params);
    void slotGetAllTimeout();

    void slotGetIcmpAnswer(PingStatus pingStatus, uint realTimeout, uint numberInSequence);
    void slotGetIcmpStatistics(uint pingTotalRequests, uint successRequests, uint failRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec);

    void slotGetSitoipConnectionStatistics(uint sitoipTotalRequests, uint sitoipSuccessRequests, uint sitoipFailRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec);
    void updateConnectionStateStatSlot(ConnectionStateStat stat);

    void slotOpenSitFile();
    void slotSaveSitFile();
    void slotSaveSrlFile();
    void slotSaveSitlLog();

    void slotSaveSeparateSitFile();
    void slotSaveSeparateSrlFile();

    void slotSettingsUpdated(SitlClientSettingsClass newSettings);

    void on_setMacPuchButton_clicked();
    void on_setMaskPushButton_clicked();
    void on_setGathewayPushButton_clicked();
    void on_getDeviceSettingsButton_clicked();
    void on_action_triggered();

    void on_pingButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();

    void on_action_3_triggered();

    void on_action_7_triggered();

    void on_action_8_triggered();

    void on_action_9_triggered();

    void on_action_SITL_2_triggered();

    void on_action_SITL_3_triggered();

    void on_updateArpPushButton_clicked();

    void updateArpTable(QList <ArpRecord> arpTable);
    void clearArpTable();

private:
    void setupSitlTab();
    void setupSeparateSitlTab();
    void setupArpTableTab();
    SettingsForm * settingsForm;
    SitlClientSettingsClass settings;
    SitlClientSettingsClass currentSettings;
    Ui::MainWindow *ui;
    SitlRamAccessor * sitlRamAccessor;
    Console * console;
    Console * resultConsole;
    Console * commandConsole;
    ConsoleEngine * consoleEngine;
    SitlAssembler * sitlAssembler;
    SitoipConsoleProc * sitoipConsoleProc;
    HelpConsoleProc * helpConsoleProc;
    UdpDeviceConnection * udpConnection;
    CommandBuilder * commandBuilder;
    //SITL Codecs
    SitlCommandGlobalCodec * sitlCommandGlobalCodec;
    SitlCommandMwrRequestCodec * sitlCommandMwrRequestCodec;
    SitlCommandMwrAnswerCodec * sitlCommandMwrAnswerCodec;
    SitlCommandMrdRequestCodec * sitlCommandMrdRequestCodec;
    SitlCommandMrdAnswerCodec * sitlCommandMrdAnswerCodec;
    SitlCommandListRequestCodec * sitlCommandListRequestCodec;
    SitlCommandListAnswerCodec * sitlCommandListAnswerCodec;
    SitlCommandIdenRequestCodec * sitlCommandIdenRequestCodec;
    SitlCommandIdenAnswerCodec * sitlCommandIdenAnswerCodec;
    SitlCommandSynerRequestCodec * sitlCommandSynerRequestCodec;
    SitlCommandSynerAnswerCodec * sitlCommandSynerAnswerCodec;
    SitlCommandIntAnswerCodec * sitlCommandIntAnswerCodec;
    SitlCommandMwxAnswerCodec * sitlCommandMwxAnswerCodec;
    SitlCommandMwxRequestCodec * sitlCommandMwxRequestCodec;
    SitlCommandMrxAnswerCodec * sitlCommandMrxAnswerCodec;
    SitlCommandMrxRequestCodec * sitlCommandMrxRequestCodec;
    SitlCommandMwiAnswerCodec * sitlCommandMwiAnswerCodec;
    SitlCommandMwiRequestCodec * sitlCommandMwiRequestCodec;
    SitlCommandMrlAnswerCodec * sitlCommandMrlAnswerCodec;
    SitlCommandMrlRequestCodec * sitlCommandMrlRequestCodec;
    SitlCommandMrmAnswerCodec * sitlCommandMrmAnswerCodec;
    SitlCommandMrmRequestCodec * sitlCommandMrmRequestCodec;
    SitlCommandClineAnswerCodec * sitlCommandClineAnswerCodec;
    SitlCommandClineRequestCodec * sitlCommandClineRequestCodec;
    SitlCommandClsetRequestCodec * sitlCommandClsetRequestCodec;
    SitlCommandSeqAnswerCodec * sitlCommandSeqAnswerCodec;
    SitlCommandSeqRequestCodec * sitlCommandSeqRequestCodec;

    CommandBuilder * sitoIpCommandBuilder;
    CommandBuilder * settingsCommandBuilder;
    IcmpRequestSequence icmpRequestSequence;
    SitoipCheckConnection * sitoipCheck;
    QStandardItemModel * arpModel;
    bool isDeviceConnected;
    QTimer sitlTimeoutTimer;
    CodecDataParamTimeoutFilter setSocketAnswerFilter;
    CodecDataParamTimeoutFilter setMacAnswerFilter;
    CodecDataParamTimeoutFilter setNetmaskAnswerFilter;
    CodecDataParamTimeoutFilter setGatewayAnswerFilter;
    CodecDataParamTimeoutFilter getAllAnswerFilter;
    bool pingButtonStarted;
};

#endif // MAINWINDOW_H
