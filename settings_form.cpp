#include "settings_form.h"
#include "ui_settings_form.h"
#include "uint_spin_box.h"

SettingsForm::SettingsForm(SitlClientSettingsClass currentSettings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsForm)
{
    ui->setupUi(this);
    ui->arpTableAddressUIntSpinBox->setBase(16);
    ui->arpTableAddressUIntSpinBox->setMaximum(0xFFFFFFFF);
    setSettings(currentSettings);
    showSettings();
}

void SettingsForm::setSettings(SitlClientSettingsClass currentSettigns)
{
    settings = currentSettigns;
}

void SettingsForm::showSettings()
{
    ui->showNetworkDataCheckBox->setChecked(settings.needShowNetworkDataInLog);
    ui->pingRequestCountSpinBox->setValue(settings.pingRequestCount);
    ui->pingRequestTimeoutSpinBox->setValue(settings.pingRequestTimeout);
    ui->checkConnectionGroupBox->setChecked(settings.checkConnection);
    ui->checkIntervalTimeSpinBox->setValue(settings.checkInterval);
    ui->checkTimeoutSpinBox->setValue(settings.checkTimeout);
    ui->checkRequestsCountSpinBox->setValue(settings.checkRequests);
    ui->checkMaxFailRequestsToBeStableSpinBox->setValue(settings.checkMaxFailRequestsToBeStable);
    ui->disconnectIfCheckFailCheckBox->setChecked(settings.disconnectFromDeviceIfCheckFaild);
    ui->getDeviceSettingsOnConnectCheckBox->setChecked(settings.getDeviceSettingsOnConnect);

    switch(settings.sitlIntStatusSize)
    {
        case WS_8BIT:
            ui->sitlIntStatSizeComboBox->setCurrentIndex(0);
            break;
        case WS_16BIT:
            ui->sitlIntStatSizeComboBox->setCurrentIndex(1);
            break;
        case WS_32BIT:
            ui->sitlIntStatSizeComboBox->setCurrentIndex(2);
            break;
        case WS_64BIT:
            ui->sitlIntStatSizeComboBox->setCurrentIndex(3);
            break;
    }
    ui->maxSitlWordsInSitoipPacketSpinBox->setValue(settings.maxSitlWordsInSitoipPacket);
    ui->arpTableAddressUIntSpinBox->setValue(settings.arpTableAddress);
    ui->arpTableRowsSpinBox->setValue(settings.arpTableMaxRecords);
}

void SettingsForm::saveSettings()
{
    settings.needShowNetworkDataInLog = ui->showNetworkDataCheckBox->isChecked();
    settings.pingRequestCount = ui->pingRequestCountSpinBox->value();
    settings.pingRequestTimeout = ui->pingRequestTimeoutSpinBox->value();
    settings.checkConnection = ui->checkConnectionGroupBox->isChecked();
    settings.checkInterval = ui->checkIntervalTimeSpinBox->value();
    settings.checkTimeout = ui->checkTimeoutSpinBox->value();
    settings.checkRequests = ui->checkRequestsCountSpinBox->value();
    settings.checkMaxFailRequestsToBeStable = ui->checkMaxFailRequestsToBeStableSpinBox->value();
    settings.disconnectFromDeviceIfCheckFaild = ui->disconnectIfCheckFailCheckBox->isChecked();
    settings.getDeviceSettingsOnConnect = ui->getDeviceSettingsOnConnectCheckBox->isChecked();

    switch(ui->sitlIntStatSizeComboBox->currentIndex())
    {
        case 0:
            settings.sitlIntStatusSize = WS_8BIT;
            break;
        case 1:
            settings.sitlIntStatusSize = WS_16BIT;
            break;
        case 2:
            settings.sitlIntStatusSize = WS_32BIT;
            break;
        case 3:
            settings.sitlIntStatusSize = WS_64BIT;
            break;
    }
    settings.maxSitlWordsInSitoipPacket = ui->maxSitlWordsInSitoipPacketSpinBox->value();
    settings.arpTableAddress = ui->arpTableAddressUIntSpinBox->value();
    settings.arpTableMaxRecords = ui->arpTableRowsSpinBox->value();
}

SettingsForm::~SettingsForm()
{
    delete ui;
}

void SettingsForm::on_buttonBox_accepted()
{
    //qDebug() << "accepted";
    saveSettings();
    emit settingsUpdated(settings);
    this->close();
}

void SettingsForm::on_buttonBox_rejected()
{
    //qDebug() << "rejected";
    this->close();
}

void SettingsForm::on_checkRequestsCountSpinBox_valueChanged(int arg1)
{
    ui->checkMaxFailRequestsToBeStableSpinBox->setMaximum(arg1 - 1);
}
