#ifndef ARP_FUNCS_H
#define ARP_FUNCS_H

#include <QtCore>
#include <QHostAddress>

QByteArray getMacAddrFromIP(QHostAddress address, bool * successFlag);

#endif // ARP_FUNCS_H

