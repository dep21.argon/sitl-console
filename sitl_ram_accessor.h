#ifndef SITL_RAM_ACCESSOR_H
#define SITL_RAM_ACCESSOR_H

#include <QObject>
#include <QTimer>
#include "sitl_command_family_params.h"

class SitlRamAccessor : public QObject
{
    Q_OBJECT
public:
    explicit SitlRamAccessor(MemoryAddressSize addressSize = MAS_32BIT, WordSize wordSize = WS_8BIT, QObject *parent = 0);
    void setMinimunChunkSize(WordSize wordSize);
    void setAddressSize(MemoryAddressSize memoryAddressSize);
    QByteArray readMemory(quint64 address, quint64 sizeBytes, int timeout = 3000);
    //TODO
    //quint64 writeMemory(quint64 address, QByteArray data, int timeout = 3000);
signals:
    void sendSitlRequestList(QList <QSharedPointer <CommandParams> > sitlRequestList);
    void receivedSitlPacket();

public slots:
    void receiveSitlAnswerList(QList <QSharedPointer <CommandParams> > sitlAnswerList);
private:
    uint wordSizeToBytes(WordSize wordSize);
    uint memoryAddressSizeToBytes(MemoryAddressSize addressSize);
    WordSize BytesToWordSize(uint bytes);
    MemoryAddressSize BytesToMemoryAddressSize(uint bytes);
    QList <QSharedPointer <CommandParams> > m_receivedSitlAnswerList;
    WordSize m_minimunChunkSize;
    MemoryAddressSize m_memoryAddressSize;
};

#endif // SITL_RAM_ACCESSOR_H
