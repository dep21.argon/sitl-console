#ifndef SETTINGS_COMMAND_FAMILY_PARAMS_H
#define SETTINGS_COMMAND_FAMILY_PARAMS_H

#include <QHostAddress>
#include "command_params.h"

#define COMMAND_ID_SETTINGS_SET_SOCKET      0x0000000300000002
#define COMMAND_ID_SETTINGS_SET_MAC         0x0000000300000003
#define COMMAND_ID_SETTINGS_SET_GATEWAY_IP  0x0000000300000007
#define COMMAND_ID_SETTINGS_SET_NETMASK     0x0000000300000008
#define COMMAND_ID_SETTINGS_GET_ALL         0x0000000300000009


class SettingsCommandFamilyParams: public CommandParams
{
public:
    SettingsCommandFamilyParams();
    SettingsCommandFamilyParams(quint64 commandId, CommandType commandType);
    ~SettingsCommandFamilyParams();
    bool operator==(const SettingsCommandFamilyParams &params) const;
    QHostAddress deviceIpAddress;
    quint16 deviceIpPort;
    QByteArray deviceMacAddress;
    QHostAddress gatewayIpAddress;
    QHostAddress netmask;
    QByteArray gatewayMacAddress;
};

#endif // SETTINGS_COMMAND_FAMILY_PARAMS_H

