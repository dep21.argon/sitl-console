#ifndef HELP_CONSOLE_PROC_H
#define HELP_CONSOLE_PROC_H

#include <QtCore>
#include "console_proc.h"

class HelpConsoleProc: public ConsoleProc
{
Q_OBJECT
public:
    HelpConsoleProc();
    void startProc(QStringList commandArguments);
    void addHelpTo(QString command, QString help);
    QString getHelpTo(QString command);
private:
    QMap <QString, QString> manuals;
};

#endif // HELP_CONSOLE_PROC_H

