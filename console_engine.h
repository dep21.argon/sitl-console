#ifndef CONSOLE_ENGINE_H
#define CONSOLE_ENGINE_H

#include <QtCore>
#include <QObject>
#include "console_proc.h"

enum ConsoleMode{CM_IO, CM_COMMAND};

class ConsoleEngine: public QObject
{
Q_OBJECT
public:
    explicit ConsoleEngine();
    void addProc(QString commandName, ConsoleProc * proc);
    void addAliasToCommand(QString commandName, QString alias);
    ConsoleProc * getProc(QString commandName);
    void setMode(ConsoleMode mode);
    ConsoleMode getMode();
public slots:
    void inputData(QString commandString);
    void startProc(QStringList commandArguments);
    void returnControl();
    //in future: add input and output buffer QTextStream
signals:
    void output(QString outputString);
    void input(QString inputString);
private:
    QMap <QString, ConsoleProc *> allProcs;
    ConsoleMode m_currentMode;
    ConsoleProc * m_currentProc;
};

#endif // CONSOLE_ENGINE_H

