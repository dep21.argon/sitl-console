#ifndef CODEC_DATA_PARAM_FILTER_H
#define CODEC_DATA_PARAM_FILTER_H

#include <QtCore>
#include "command_params.h"
#include "command_builder.h"

class CodecDataParamFilter: public QObject
{
Q_OBJECT
public:
    CodecDataParamFilter(QObject * parent = 0);
    void addCodec(CommandCodec * codec);
    void setEndian(QDataStream::ByteOrder endian);
    QDataStream::ByteOrder getEndian();
    void setNeedEncode(bool encode = true);
    bool getNeedEncode();
    void setNeedDecode(bool decode = true);
    bool getNeedDecode();
public slots:
    virtual void filterData(QByteArray rawData);
    virtual void filterParams(QSharedPointer <CommandParams> params);
signals:
    void validData(QByteArray rawData);
    void validParams(QSharedPointer <CommandParams>);
    void encodedData(QByteArray rawData);
    void decodedParams(QSharedPointer <CommandParams>);
protected:
    CommandBuilder commandBuilder;
    QDataStream::ByteOrder endian;
    bool encode;
    bool decode;
};

#endif // CODEC_DATA_PARAM_FILTER_H

