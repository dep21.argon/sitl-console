#ifndef SITL_COMMAND_FAMILY_CODEC_H
#define SITL_COMMAND_FAMILY_CODEC_H

#include "sitl_command_family_params.h"

#define COMMAND_ID_SITL_REQUEST     0x80
#define COMMAND_ID_SITL_ANSWER      0x81

//#define SITL_COMMAND_MWR            0x00
#define SITL_COMMAND_MASK_MWR           0xF0

//#define SITL_COMMAND_MRD            0x40
#define SITL_COMMAND_MASK_MRD           0xF0
#define SITL_COMMAND_MASK_MWX           0xD0
#define SITL_COMMAND_MASK_MRX           0xD0
#define SITL_COMMAND_MASK_MWI           0xD0
#define SITL_COMMAND_MASK_MRL           0xDC
#define SITL_COMMAND_MASK_MRM           0xDC
#define SITL_COMMAND_MASK_SEQ           0xFC
#define SITL_COMMAND_MASK_ALL_HEADER    0xFF

#define SITL_COMMAND_ID_SEQ             0x50

#define SITL_SEQ_HAVE_DATA_MASK    0x01
#define SITL_SEQ_HAVE_DATA_TRUE    0x01
#define SITL_SEQ_HAVE_DATA_FALSE   0x00

#define SITL_SEQ_PIT_MASK         0x02
#define SITL_SEQ_PIT_INTERMEDIATE 0x00
#define SITL_SEQ_PIT_LAST         0x02

#define SITL_WORD_SIZE_MASK         0x0C
#define SITL_WORD_SIZE_8BIT         0x00
#define SITL_WORD_SIZE_16BIT        0x04
#define SITL_WORD_SIZE_32BIT        0x08
#define SITL_WORD_SIZE_64BIT        0x0C

#define SITL_ANSWER_CODE_ICTRL              0x00
#define SITL_ANSWER_CODE_DONE_WITH_DATA     0x01
#define SITL_ANSWER_CODE_ERDONE_WITH_DATA   0x02
#define SITL_ANSWER_CODE_ABORT_NO_DATA      0x04
#define SITL_ANSWER_CODE_ABORT_WITH_DATA    0x08
#define SITL_ANSWER_CODE_ERABR_WITH_DATA    0x10
#define SITL_ANSWER_CODE_RETRY_NO_DATA      0x20
#define SITL_ANSWER_CODE_RETRY_WITH_DATA    0x40
#define SITL_ANSWER_CODE_ERRET_WITH_DATA    0x80
#define SITL_ANSWER_CODE_SETUP_NO_STATUS    0x0F
#define SITL_ANSWER_CODE_SETUP_WITH_STATUS  0x1F
#define SITL_ANSWER_CODE_ACTIV              0xFF

#define SITL_ADDRESS_SIZE_MASK      0x03
#define SITL_ADDRESS_SIZE_16BIT     0x00
#define SITL_ADDRESS_SIZE_24BIT     0x01
#define SITL_ADDRESS_SIZE_32BIT     0x02
#define SITL_ADDRESS_SIZE_64BIT     0x03

#define CODEC_ID_SITL                   0x0000000100000000
#define CODEC_ID_SITL_MWR_REQUEST       0x00000001000000FF
#define CODEC_ID_SITL_MWR_ANSWER        0x00000001000001FF
#define CODEC_ID_SITL_MRD_REQUEST       0x00000001000040FF
#define CODEC_ID_SITL_MRD_ANSWER        0x00000001000041FF
#define CODEC_ID_SITL_LIST_REQUEST      0x0000000100001000
#define CODEC_ID_SITL_LIST_ANSWER       0x0000000100001001
#define CODEC_ID_SITL_IDEN_REQUEST      0x0000000100001100
#define CODEC_ID_SITL_IDEN_ANSWER       0x0000000100001101
#define CODEC_ID_SITL_SYNER_REQUEST     0x0000000100001300
#define CODEC_ID_SITL_SYNER_ANSWER      0x0000000100001301
#define CODEC_ID_SITL_INT_ANSWER        0x0000000100005001
#define CODEC_ID_SITL_MWX_REQUEST       0x0000000100008000
#define CODEC_ID_SITL_MWX_ANSWER        0x0000000100008001
#define CODEC_ID_SITL_MRX_REQUEST       0x000000010000C000
#define CODEC_ID_SITL_MRX_ANSWER        0x000000010000C001
#define CODEC_ID_SITL_MWI_REQUEST       0x0000000100009000
#define CODEC_ID_SITL_MWI_ANSWER        0x0000000100009001
#define CODEC_ID_SITL_MRL_REQUEST       0x000000010000D000
#define CODEC_ID_SITL_MRL_ANSWER        0x000000010000D001
#define CODEC_ID_SITL_MRM_REQUEST       0x000000010000D400
#define CODEC_ID_SITL_MRM_ANSWER        0x000000010000D401
#define CODEC_ID_SITL_CLINE_REQUEST     0x0000000100001200
#define CODEC_ID_SITL_CLINE_ANSWER      0x0000000100001201
#define CODEC_ID_SITL_CLSET_REQUEST     0x0000000100005000
#define CODEC_ID_SITL_SEQ_REQUEST       0x0000000100005300
#define CODEC_ID_SITL_SEQ_ANSWER        0x0000000100005301


class SitlCommandFamilyCodec: public CommandCodec
{
public:
    SitlCommandFamilyCodec(quint64 codecId, quint64 commandId, quint8 commandMask, CommandType commandTyep);
    virtual QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    virtual bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder);
    virtual bool isParamsMyCommand(QSharedPointer <CommandParams> params);
    AnswerCode answerCodeFromSitlAnswerCode(quint8 sitlAnswerCode, bool &success);
    quint8 sitlAnswerCodeFromAnswerCodeAndData(AnswerCode answerCode, bool dataInMessage, bool &success);
    bool haveWordInMessageFromSitlAnswerCode(quint8 sitlAnswerCode, bool &success);
    bool haveWordInMessageFromSitlHeader(quint8 header, bool &success);
    PlaceInTransaction getHeaderPIT(quint8 header);
    void setHeaderHaveWord(quint8 &header, bool haveWord);
    void setHeaderPIT(quint8 &header, PlaceInTransaction pit);
    void setHeaderWordSize(quint8 &header, WordSize wordSize);
    void setHeaderAddressSize(quint8 &header, MemoryAddressSize memoryAddressSize);
    WordSize getFromHeaderWordSize(quint8 header);
    MemoryAddressSize getFromHeaderAddressSize(quint8 header);
    bool getFromHeaderUseAutoincrementAddress(quint8 header);
    void setHeaderUseAutoincrementAddress(quint8 &header, bool useAutoincrementAddress);
    quint8 calculateOffset(quint8 maxWordSize, quint8 wordSize, quint64 address);
    quint64 m_commandId;
    quint64 m_commandMask;
    CommandType m_commandType;
    WordSize statusIdSize;
};

class SitlCommandMwrRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwrRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMwrAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwrAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrdRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrdRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrdAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrdAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandListRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandListRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandListAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandListAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandIdenRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandIdenRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandIdenAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandIdenAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandSynerRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandSynerRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandSynerAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandSynerAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandIntAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandIntAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    void setStatusIdSize(WordSize size);
};

class SitlCommandMwxRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwxRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMwxAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwxAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMwiRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwiRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMwiAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMwiAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrxRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrxRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrxAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrxAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrlRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrlRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrlAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrlAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrmRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrmRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandMrmAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandMrmAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandClineRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandClineRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandClineAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandClineAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandClsetRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandClsetRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandSeqRequestCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandSeqRequestCodec();
    QByteArray encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError);
};

class SitlCommandSeqAnswerCodec: public SitlCommandFamilyCodec
{
public:
    SitlCommandSeqAnswerCodec();
    QSharedPointer <CommandParams> decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError);
    bool isDataMyCommand(QByteArray data, QDataStream::ByteOrder);
    void toDefault();
    void setAddressAutoincrement(bool aai);
    void setCommandId(quint64 commandId);
    void setMemoryAddressSize(MemoryAddressSize size);
    void setWordSize(WordSize wordSize);
    void setAddressWord(quint64 addressWord);
private:
    quint64 m_ws_commandId;
    MemoryAddressSize m_ws_mas;
    quint64 m_ws_addressWord;
    WordSize m_ws_wordSize;
    bool m_ws_useAddressAutoincrement;
};

#endif // SITL_COMMAND_FAMILY_CODEC_H

