#include "console_engine.h"

ConsoleEngine::ConsoleEngine()
{
    setMode(CM_COMMAND);
    m_currentProc = NULL;
}

void ConsoleEngine::addProc(QString commandName, ConsoleProc *proc)
{
    allProcs.insert(commandName.toUpper(), proc);
    QObject::connect(proc, SIGNAL(output(QString)), SIGNAL(output(QString)));
    QObject::connect(proc, SIGNAL(returnControl()), this, SLOT(returnControl()));
}

void ConsoleEngine::addAliasToCommand(QString commandName, QString alias)
{
    ConsoleProc * proc = getProc(commandName);
    if(proc == NULL)
    {
        return;
    }
    allProcs.insert(alias.toUpper(), proc);
}

ConsoleProc * ConsoleEngine::getProc(QString commandName)
{
    QMap <QString, ConsoleProc *>::const_iterator i = allProcs.find(commandName);
    if((i == allProcs.end() || i.key() != commandName.toUpper()))
    {
        return NULL;
    }
    return i.value();
}

void ConsoleEngine::setMode(ConsoleMode mode)
{
    m_currentMode = mode;
}

ConsoleMode ConsoleEngine::getMode()
{
    return m_currentMode;
}

void ConsoleEngine::inputData(QString commandString)
{
    if(m_currentMode == CM_COMMAND)
    {
        QStringList commandArguments = commandString.toUpper().split(" ", QString::SkipEmptyParts);
        startProc(commandArguments);
    }
    else if(m_currentMode == CM_IO)
    {
        emit input(commandString);
    }
}

void ConsoleEngine::startProc(QStringList commandArguments)
{
    QString commandName = commandArguments.at(0);
    ConsoleProc * proc = getProc(commandName);
    if(proc == NULL)
    {
        emit output(trUtf8("Команда ") + commandName + trUtf8(" не найдена"));
        return;
    }
    else
    {
        setMode(CM_IO);
        m_currentProc = proc;
        QObject::connect(this, SIGNAL(input(QString)), m_currentProc, SLOT(input(QString)));
        m_currentProc->startProc(commandArguments);
    }
}

void ConsoleEngine::returnControl()
{
    setMode(CM_COMMAND);
    QObject::disconnect(this, SIGNAL(input(QString)), m_currentProc, SLOT(input(QString)));
    m_currentProc = NULL;
}
