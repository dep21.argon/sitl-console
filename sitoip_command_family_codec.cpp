#include "sitoip_command_family_codec.h"

SitoipCommandEmptyCodec::SitoipCommandEmptyCodec()
    :CommandCodec(CODEC_ID_SITOIP_EMPTY)
{
}

bool SitoipCommandEmptyCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    Q_UNUSED(endian);
    if((((quint8)data.at(0) == (quint8)SITOIP_COMMAND_REQUEST)
            || ((quint8)data.at(0) == (quint8)SITOIP_COMMAND_ANSWER))
            && (quint8)data.at(1) == (quint8)SITOIP_COMMAND_EMPTY)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool SitoipCommandEmptyCodec::isParamsMyCommand(QSharedPointer <CommandParams> params)
{
    if(params->commandId == COMMAND_ID_SITOIP_EMPTY)
    {
        return true;
    }
    else
    {
        return false;
    }
}

QByteArray SitoipCommandEmptyCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    Q_UNUSED(endian);
    QByteArray sitoipOut;
    codecError->errType = CE_NO_ERROR;
    if(param->commandId == COMMAND_ID_SITOIP_EMPTY)
    {
        sitoipOut.resize(2);
        if(param->commandType == CT_REQUEST)
        {
            sitoipOut[0] = SITOIP_COMMAND_REQUEST;
        }
        else if(param->commandType == CT_ANSWER)
        {
            sitoipOut[0] = SITOIP_COMMAND_ANSWER;
        }
        sitoipOut[1] = SITOIP_COMMAND_EMPTY;
    }
    else
    {
        codecError->errType = CE_CANT_ENCODE;
    }
    return sitoipOut;
}

QSharedPointer <CommandParams> SitoipCommandEmptyCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    Q_UNUSED(endian);
    codecError->errType = CE_NO_ERROR;
    if((quint8)data.at(1) != (quint8)SITOIP_COMMAND_EMPTY)
    {
        codecError->errType = CE_CANT_DECODE;
        return QSharedPointer <CommandParams>(NULL);
    }
    else
    {
        CommandParams * outSitoipParams = new SitoipCommandFamilyParams;
        outSitoipParams->commandId = COMMAND_ID_SITOIP_EMPTY;
        if((quint8)data.at(0) == (quint8)SITOIP_COMMAND_REQUEST)
        {
            outSitoipParams->commandType = CT_REQUEST;
        }
        else if((quint8)data.at(0) == (quint8)SITOIP_COMMAND_ANSWER)
        {
            outSitoipParams->commandType = CT_ANSWER;
        }
        return QSharedPointer <CommandParams>(outSitoipParams);
    }
}

SitoipCommandFamilyCodec::SitoipCommandFamilyCodec()
    :CommandCodec(CODEC_ID_SITOIP)
{
}

void SitoipCommandFamilyCodec::addCodec(CommandCodec *codec)
{
    sitoipCommandBuilder.registerCodec(codec);
}

bool SitoipCommandFamilyCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    Q_UNUSED(endian);
    if(((quint8)data.at(0) == (quint8)SITOIP_COMMAND_REQUEST)
            || ((quint8)data.at(0) == (quint8)SITOIP_COMMAND_ANSWER)
            || ((quint8)data.at(0) == (quint8)SITOIP_COMMAND_REPEAT_REQUEST)
            || ((quint8)data.at(0) == (quint8)SITOIP_COMMAND_REPEAT_ANSWER))
    {
        return true;
    }
    return false;
}

bool SitoipCommandFamilyCodec::isParamsMyCommand(QSharedPointer <CommandParams> params)
{
    if((params->commandId == COMMAND_ID_SITOIP_REQUEST)
            || (params->commandId == COMMAND_ID_SITOIP_ANSWER)
            || (params->commandId == COMMAND_ID_SITOIP_REPEAT_REQUEST)
            || (params->commandId == COMMAND_ID_SITOIP_REPEAT_ANSWER)
            || (params->commandId == COMMAND_ID_SITOIP_EMPTY))
    {
        return true;
    }
    return false;
}

QByteArray SitoipCommandFamilyCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    QByteArray sitoipRawData;
    if((param->commandId != COMMAND_ID_SITOIP_REQUEST)
            && (param->commandId != COMMAND_ID_SITOIP_ANSWER)
            && (param->commandId != COMMAND_ID_SITOIP_REPEAT_REQUEST)
            && (param->commandId != COMMAND_ID_SITOIP_REPEAT_ANSWER)
            && (param->commandId != COMMAND_ID_SITOIP_EMPTY))
    {
        codecError->errType = CE_CANT_ENCODE;
        return sitoipRawData;
    }
    QDataStream sitoipDataStream(&sitoipRawData, QIODevice::WriteOnly);
    SitoipCommandFamilyParams * sitoipParam = (SitoipCommandFamilyParams *) param.data();
    if(sitoipParam->commandType == CT_REQUEST)
    {
        if(sitoipParam->commandId == COMMAND_ID_SITOIP_REQUEST)
        {
            sitoipDataStream << (quint8)SITOIP_COMMAND_REQUEST;
        }
        else if(sitoipParam->commandId == COMMAND_ID_SITOIP_REPEAT_REQUEST)
        {
            sitoipDataStream << (quint8)SITOIP_COMMAND_REPEAT_REQUEST;
            sitoipDataStream << (quint8)SITOIP_COMMAND_EMPTY;
            return sitoipRawData;
        }
    }
    else if(sitoipParam->commandType == CT_ANSWER)
    {
        if(sitoipParam->commandId == COMMAND_ID_SITOIP_ANSWER)
        {
            sitoipDataStream << (quint8)SITOIP_COMMAND_ANSWER;
        }
        else if(sitoipParam->commandId == COMMAND_ID_SITOIP_REPEAT_ANSWER)
        {
            sitoipDataStream << (quint8)SITOIP_COMMAND_REPEAT_ANSWER;
        }
    }
    sitoipDataStream << (quint8)sitoipParam->sitlCommandsList.size();
    for(int i = 0; i < sitoipParam->sitlCommandsList.size(); ++i)
    {
        QByteArray sitlMCW = sitoipCommandBuilder.encode(sitoipParam->sitlCommandsList.at(i), endian, codecError);
        sitoipRawData.append(sitlMCW);
    }
    return sitoipRawData;
}

QSharedPointer <CommandParams> SitoipCommandFamilyCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError * codecError)
{
    codecError->errType = CE_NO_ERROR;
    SitoipCommandFamilyParams * sitoipParams = new SitoipCommandFamilyParams();
    quint8 header = data.at(0);
    if(header == SITOIP_COMMAND_REQUEST)
    {
        sitoipParams->commandType = CT_REQUEST;
        sitoipParams->commandId = COMMAND_ID_SITOIP_REQUEST;
    }
    else if(header == SITOIP_COMMAND_ANSWER)
    {
        sitoipParams->commandType = CT_ANSWER;
        sitoipParams->commandId = COMMAND_ID_SITOIP_ANSWER;
    }
    else if(header == SITOIP_COMMAND_REPEAT_REQUEST)
    {
        sitoipParams->commandType = CT_REQUEST;
        sitoipParams->commandId = COMMAND_ID_SITOIP_REPEAT_REQUEST;
    }
    else if(header == SITOIP_COMMAND_REPEAT_ANSWER)
    {
        sitoipParams->commandType = CT_ANSWER;
        sitoipParams->commandId = COMMAND_ID_SITOIP_REPEAT_ANSWER;
    }
    else
    {
        codecError->errType = CE_CANT_DECODE;
        return QSharedPointer <CommandParams>(NULL);
    }
    quint8 sitlCommandsTotal = data.at(1);
    if(sitlCommandsTotal == SITOIP_COMMAND_EMPTY)
    {
        sitoipParams->commandId = COMMAND_ID_SITOIP_EMPTY;
        return QSharedPointer <CommandParams>(sitoipParams);
    }
    for(int i = 0; i < sitlCommandsTotal; ++i)
    {
        CodecError sitlCodecError;
        QSharedPointer <CommandParams> sitlCommand = sitoipCommandBuilder.decode(data.mid(i * 18 + 2), endian, &sitlCodecError);
        sitoipParams->sitlCommandsList.append(sitlCommand);
    }
    return QSharedPointer <CommandParams>(sitoipParams);
}
