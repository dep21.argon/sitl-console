#ifndef CHECK_SITOIP_CONNECTION_H
#define CHECK_SITOIP_CONNECTION_H

#include <QtCore>
#include <QHostAddress>
#include "udp_device_connection.h"
#include "sitoip_command_family_params.h"
#include "sitoip_command_family_codec.h"
#include "codec_data_param_timeout_filter.h"

enum SitoipAnswer{SITOIP_SUCCESS, SITOIP_ERROR, SITOIP_TIMEOUT};

class SitoipEmptyRequest: public QObject
{
Q_OBJECT
public:
    SitoipEmptyRequest();
    void sendRequest(uint timeoutMsec, QDataStream::ByteOrder endian);
private:
    QTime sendRequestTime;
    QTime receiveAnswerTime;
    SitoipCommandEmptyCodec sitoipEmptyCodec;
    CodecDataParamTimeoutFilter sitoipEmptyAnswerFilter;

signals:
    void signalAnswer(SitoipAnswer answer, uint timeToAnswerMsec);
    void checkData(QByteArray rawData);
    void sendData(QByteArray rawData);
public slots:
    void stop();
    void slotSitoipAnswerReady(QByteArray rawData);
    void slotSitoipAnswerTimeout();
};

enum SitoipConnectionStatus{SITOIP_CONNECTION_STABLE, SITOIP_CONNECTION_UNSTABLE, SITOIP_CONNECTION_ERROR};

class SitoipEmptyAnswerInfo
{
public:
    SitoipAnswer answer;
    uint timeToAnswer;
};

class SitoipCheckConnection: public QObject
{
Q_OBJECT
public:
    SitoipCheckConnection();
    void setCheckConnectionParams(uint timeoutMsec, uint timeBetweenRequestsMsec, uint requestsForStatistics);
signals:
    void sendData(QByteArray rawData);
    void checkData(QByteArray rawData);
    void signalSitoipAnswer(SitoipAnswer answer, uint timeToAnswer, uint numberInSequence);
    void signalSitoipConnectionStatistics(uint sitoipTotalRequests, uint sitoipSuccessRequests, uint sitoipFailRequests, uint minTimeMsec, uint maxTimeMsec, uint avgTimeMsec);
public slots:
    void sendSitoipEmptyRequestSequence();
    void stopSitoipEmptyRequestSequence();
    void slotSitoipAnswer(SitoipAnswer answer, uint timeToAnswerMsec);
    void slotSendSitoipEmpytRequest();
private:
    uint successSend();
    uint errorSend();
    uint minSendTime();
    uint maxSendTime();
    uint avgSendTime();
    QQueue <SitoipEmptyAnswerInfo> answersInfo;
    QTimer waitBetwenSendRequests;
    uint timeoutMsec;
    uint timeBetweenRequestsMsec;
    uint requestsForStatistics;
    uint sendTotal;
    SitoipEmptyRequest * sitoipEmptyRequest;
    bool stopRequestNow;
};

#endif // CHECK_SITOIP_CONNECTION_H

