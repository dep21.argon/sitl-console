#ifndef SITOIP_COMMAND_FAMILY_PARAMS_H
#define SITOIP_COMMAND_FAMILY_PARAMS_H

#include "command_params.h"
#include "sitl_command_family_params.h"

#define COMMAND_ID_SITOIP_REQUEST           0x00000002000080FF
#define COMMAND_ID_SITOIP_ANSWER            0x00000002000081FF
#define COMMAND_ID_SITOIP_REPEAT_REQUEST    0x0000000200008200
#define COMMAND_ID_SITOIP_REPEAT_ANSWER     0x00000002000083FF
#define COMMAND_ID_SITOIP_EMPTY             0x0000000200008000

class SitoipCommandFamilyParams: public CommandParams
{
public:
    SitoipCommandFamilyParams();
    SitoipCommandFamilyParams(quint64 commandId, CommandType commandType);
    ~SitoipCommandFamilyParams();
    bool operator==(const SitoipCommandFamilyParams &params) const;
    QList <QSharedPointer <CommandParams> > sitlCommandsList;
    QList <QByteArray> sitlRawDataList;
    void clear();
};

#endif // SITOIP_COMMAND_FAMILY_PARAMS_H

