#ifndef CODEC_DATA_PARAM_TIMEOUT_FILTER_H
#define CODEC_DATA_PARAM_TIMEOUT_FILTER_H

#include "codec_data_param_filter.h"

#define CDPTF_TIMEOUT_INFINITE  -1

class CodecDataParamTimeoutFilter: public CodecDataParamFilter
{
Q_OBJECT
public:
    CodecDataParamTimeoutFilter(QObject * parent = 0);
    void setTimeout(int timeoutMsec);
    void filterData(QByteArray rawData);
    void filterParams(QSharedPointer <CommandParams> params);
    bool isActive();
public slots:
    void startWait();
    void stopWait();
    void timerTimeout();
signals:
    void timeout();
    void stoped();
private:
    int timeoutMsec;
    bool active;
    QTimer timer;
};

#endif // CODEC_DATA_PARAM_TIMEOUT_FILTER_H

