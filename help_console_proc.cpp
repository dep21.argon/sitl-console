#include "help_console_proc.h"

HelpConsoleProc::HelpConsoleProc()
{
    QString helpManual;
    helpManual = trUtf8("Название:\n");
    helpManual += trUtf8("HELP - справка о команде\n\n");
    helpManual += trUtf8("Синтаксис:\n");
    helpManual += trUtf8("HELP КОМАНДА\n\n");
    helpManual += trUtf8("Описане:\n");
    helpManual += trUtf8("Команда HELP выводит справку о указанной команде.");
    addHelpTo("HELP", helpManual);
    return;
}

void HelpConsoleProc::startProc(QStringList commandArguments)
{
    if(commandArguments.size() == 1)
    {
        QString allCommands = getHelpTo("HELP");
        allCommands += trUtf8("\n\nДоступные команды:\n");
        QMap <QString, QString>::const_iterator i;
        for(i = manuals.begin(); i != manuals.end(); ++i)
        {
            allCommands += i.key() + "\n";
        }
        emit output(allCommands);
        emit returnControl();
        return;
    }
    QString commandName = commandArguments.at(1).toUpper();
    QString manual = getHelpTo(commandName);
    if(manual == "")
    {
        emit output(trUtf8("Справка для комманды ") + commandName + trUtf8(" не найдена"));
        emit returnControl();
        return;
    }
    else
    {
        emit output(manual);
        emit returnControl();
    }
}

void HelpConsoleProc::addHelpTo(QString command, QString help)
{
    manuals.insert(command.toUpper(), help);
}

QString HelpConsoleProc::getHelpTo(QString commandName)
{
    QMap <QString, QString>::const_iterator i = manuals.find(commandName);
    if(i == manuals.end() || i.key() != commandName)
    {
        return "";
    }
    else
    {
        return i.value();
    }
}
