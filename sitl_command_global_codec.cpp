#include "sitl_command_global_codec.h"

SitlCommandGlobalCodec::SitlCommandGlobalCodec()
    :CommandCodec(CODEC_ID_SITL)
{
    sitlCommandSeqAnswerCodec = NULL;
}

SitlCommandGlobalCodec::~SitlCommandGlobalCodec()
{

}

void SitlCommandGlobalCodec::addCodec(CommandCodec *codec)
{
    sitlCommandBuilder.registerCodec(codec);
    if(codec->getCodecId() == CODEC_ID_SITL_SEQ_ANSWER)
    {
        sitlCommandSeqAnswerCodec = (SitlCommandSeqAnswerCodec *)codec;
    }
}

bool SitlCommandGlobalCodec::isDataMyCommand(QByteArray data, QDataStream::ByteOrder endian)
{
    return (sitlCommandBuilder.getCodecFromData(data, endian) != NULL);
}

bool SitlCommandGlobalCodec::isParamsMyCommand(QSharedPointer <CommandParams> params)
{
    return (sitlCommandBuilder.getCodecFromParams(params) != NULL);
}

QByteArray SitlCommandGlobalCodec::encode(QSharedPointer <CommandParams> param, QDataStream::ByteOrder endian, CodecError *codecError)
{
    return sitlCommandBuilder.encode(param, endian, codecError);
}

QSharedPointer <CommandParams> SitlCommandGlobalCodec::decode(QByteArray data, QDataStream::ByteOrder endian, CodecError *codecError)
{
    QSharedPointer <CommandParams> params = sitlCommandBuilder.decode(data, endian, codecError);
    if(codecError->errType == CE_NO_ERROR)
    {
        postDecodeModify(params);
    }
    return params;
}

void SitlCommandGlobalCodec::postDecodeModify(QSharedPointer <CommandParams> params)
{
    SitlCommandFamilyParams * sitlParams = (SitlCommandFamilyParams *) params.data();
    if((sitlParams->isMultipleTransaction) && (sitlParams->placeInTransaction == PIT_FIRST))
    {
        sitlCommandSeqAnswerCodec->setCommandId(sitlParams->commandId);
        sitlCommandSeqAnswerCodec->setAddressWord(sitlParams->addressWord);
        sitlCommandSeqAnswerCodec->setMemoryAddressSize(sitlParams->memoryAddressSize);
        sitlCommandSeqAnswerCodec->setAddressAutoincrement(sitlParams->useAddressAutoincrement);
        sitlCommandSeqAnswerCodec->setWordSize(sitlParams->wordSize);
    }
    else if((sitlParams->isMultipleTransaction) && (sitlParams->placeInTransaction == PIT_LAST))
    {
        sitlCommandSeqAnswerCodec->toDefault();
    }
}
