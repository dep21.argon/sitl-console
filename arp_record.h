#ifndef ARP_RECORD_H
#define ARP_RECORD_H

#include <QHostAddress>
#include "mac_address.h"

class ArpRecord
{
public:
    QHostAddress ipAddress;
    MacAddress macAddress;
    quint32 ttl;
    quint8 flag;
};

#endif // ARP_RECORD_H
