#include "udp_device_connection.h"

#include <QTimer>

UdpDeviceConnection::UdpDeviceConnection(QHostAddress ipAddress, quint16 ipPort, int interval)
{
    udpConnection = new QUdpSocket;
    //udpConnection->bind();
    //udpConnection->close();
    setSocket(ipAddress, ipPort);
    connectionState = CS_DISCONNECTED;
    setCheckConnectionInterval(interval);
    QObject::connect(&checkTimer, SIGNAL(timeout()), SLOT(checkConnection()));
    QObject::connect(udpConnection, SIGNAL(readyRead()), SLOT(slotDataReceived()));
}

void UdpDeviceConnection::setSocket(QHostAddress ipAddress, quint16 ipPort)
{
    deviceIpAddress = ipAddress;
    deviceIpPort = ipPort;
}

void UdpDeviceConnection::setCheckConnectionInterval(int interval)
{
    checkConnectionInterval = interval;
}

void UdpDeviceConnection::connectDevice()
{
    connectionState = CS_CONNECTED;
    checkTimer.setInterval(checkConnectionInterval);
    checkTimer.start();
    udpConnection->bind();
    emit connectNotify();
}

void UdpDeviceConnection::disconnectDevice()
{
    connectionState = CS_DISCONNECTED;
    checkTimer.stop();
    udpConnection->close();
    emit disconnectNotify();
}

int UdpDeviceConnection::sendData(QByteArray rawData)
{
    if(udpConnection->state() == QUdpSocket::UnconnectedState)
    {
        return false;
    }
    udpConnection->writeDatagram(rawData, deviceIpAddress, deviceIpPort);
    emit dataSended(rawData);
    return true;
}

ConnectionState UdpDeviceConnection::getConnectionState()
{
    return connectionState;
}

quint16 UdpDeviceConnection::getMyPort()
{
    quint16 port = udpConnection->localPort();
    return port;
}

void UdpDeviceConnection::checkConnection()
{
    //todo
    //if...
    //  disconnectDevice();
    //  emit disconnectNotify();
}

void UdpDeviceConnection::slotDataReceived()
{
    QByteArray rawData;
    if(udpConnection->hasPendingDatagrams())
    {
        rawData.resize(udpConnection->pendingDatagramSize());
        udpConnection->readDatagram(rawData.data(), rawData.size());
        emit dataReceived(rawData);
    }
}
