#include "codec_data_param_timeout_filter.h"

CodecDataParamTimeoutFilter::CodecDataParamTimeoutFilter(QObject * parent)
        : CodecDataParamFilter(parent)
{
    setTimeout(CDPTF_TIMEOUT_INFINITE);
    timer.setSingleShot(true);
    active = false;
    QObject::connect(&timer, SIGNAL(timeout()), SLOT(timerTimeout()));
}

void CodecDataParamTimeoutFilter::setTimeout(int timeoutMsec)
{
    this->timeoutMsec = timeoutMsec;
}

bool CodecDataParamTimeoutFilter::isActive()
{
    return active;
}

void CodecDataParamTimeoutFilter::filterData(QByteArray rawData)
{
    if(isActive())
    {
        if(commandBuilder.getCodecFromData(rawData) != NULL)
        {
            timer.stop();
            active = false;
            emit validData(rawData);
            if(decode)
            {
                CodecError codecError;
                QSharedPointer <CommandParams> params = commandBuilder.decode(rawData, &codecError);
                if(codecError.errType == CE_NO_ERROR)
                {
                    emit decodedParams(params);
                }
            }
        }
    }
}

void CodecDataParamTimeoutFilter::filterParams(QSharedPointer <CommandParams> params)
{
    if(isActive())
    {
        if(commandBuilder.getCodecFromParams(params) != NULL)
        {
            timer.stop();
            emit validParams(params);
            if(encode)
            {
                CodecError codecError;
                QByteArray rawData = commandBuilder.encode(params, &codecError);
                if(codecError.errType == CE_NO_ERROR)
                {
                    emit encodedData(rawData);
                }
            }
        }
    }
}

void CodecDataParamTimeoutFilter::startWait()
{
    active = true;
    if(timeoutMsec != CDPTF_TIMEOUT_INFINITE)
    {
        timer.start(timeoutMsec);
    }
}

void CodecDataParamTimeoutFilter::stopWait()
{
    active = false;
    timer.stop();
    emit stoped();
}

void CodecDataParamTimeoutFilter::timerTimeout()
{
    active = false;
    emit timeout();
}
