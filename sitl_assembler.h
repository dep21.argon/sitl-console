#ifndef SITL_ASSEMBLER_H
#define SITL_ASSEMBLER_H

#include "sitl_command_family_params.h"
#include <QStringList>
#include <QMap>

class SitlCommandAssembler
{
public:
    SitlCommandAssembler(quint64 commandId, QString commandStr);
    quint64 getCommandId();
    bool isMyCommand(QString sitlCommandStr);
    bool isMyCommand(quint8 commandId);
    QList <QString> getAliases();
    virtual QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString) = 0;
    virtual QString disassemble(SitlCommandFamilyParams sitlParams) = 0;
    virtual QString manual();
protected:
    void addCommandAlias(QString alias);
    QString answerCodeToString(AnswerCode answerCode);
    WordSize stringToWordSize(QString wordSizeStr, bool &successFlag);
    MemoryAddressSize memoryAddressSizeFromBits(int bits, bool &successFlag);
    WordSize wordSizeFromBits(int bits, bool &successFlag);
    bool stringToUseAutoincrementAddress(QString useAutoincrementAddressString, bool &successFlag);
    quint64 stringToReadCount(QString readCountStr, bool &successFlag);
    quint64 commandId;
    QString commandStr;
    QList <QString> aliases;
};

class SitlAssembler
{
public:
    void addCommandAssembler(SitlCommandAssembler * commandAssembler);
    bool isSitlCommand(QString sitlCommandStr);
    bool isSitlCommand(quint64 sitlCommandId);
    SitlCommandAssembler * getSitlCommandAssembler(QString commandStr);
    SitlCommandAssembler * getSitlCommandAssembler(quint64 sitlCommandId);
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandArguments);
    QString disassemble(SitlCommandFamilyParams sitlParams);
private:
    QMap <quint64, SitlCommandAssembler *> sitlCommands;
};

class SitlCommandAssemblerMWR: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMWR();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMRD: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMRD();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerIDEN: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerIDEN();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerLIST: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerLIST();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerINT: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerINT();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerSYNER: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerSYNER();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMWX: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMWX();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMRX: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMRX();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMWI: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMWI();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMRL: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMRL();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerMRM: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerMRM();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerCLINE: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerCLINE();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};

class SitlCommandAssemblerCLSET: public SitlCommandAssembler
{
public:
    SitlCommandAssemblerCLSET();
    QString manual();
protected:
    QList <SitlCommandFamilyParams> assemble(QStringList sitlCommandString);
    QString disassemble(SitlCommandFamilyParams sitlParams);
};


#endif // SITL_ASSEMBLER_H

